﻿#include "LogProcessor.h"
#include "../Output/Object/ConsoleOutput.h"
#include "../Output/Object/FileOutput.h"
#include "../Output/OutputArgument.h"
#include "../../../Utility/src/Object/ArgumentFactory.h"
#include <string>

namespace wlog {
	namespace processor {
		/// 내부에서 사용할 변수를 정의합니다.
		/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
		struct LogProcessor::Impl
		{
			Output_Container			output_container_;
		};

		LogProcessor::LogProcessor(ProducerObj* pproducer)
			: Consumer(pproducer, 1)
		{
			impl_ = std::make_unique<Impl>();

			ThreadRun();
		}
		LogProcessor::~LogProcessor()
		{}

		void
			LogProcessor::Handle(const object& obj)
		{
			typedef utility::object::ArgumentFactory<output::OutputArgument>
				ArgumentFactory;

			Output_Container::iterator itr = impl_->output_container_.find(obj.Handle_);

			if (itr == impl_->output_container_.end())
			{
				return;
			}

			output::OutputArgument* out_arg = static_cast<output::OutputArgument*>(obj.pValue_);
			if (nullptr == out_arg)
			{
				std::stringstream str_error;
				str_error << "[LogProcessor::Handle] if(nullptr != out_arg ) obj.Handle_(" << obj.Handle_ << ") ";
				itr->second->Write(str_error.str().c_str());
				return;
			}

			itr->second->Write(out_arg->msg_.c_str());

			ArgumentFactory::Destroy(out_arg);
		}

		LogProcessor::Output_Container*
			LogProcessor::GetOutputContainer()
		{
			return &impl_->output_container_;
		}
	}
} /// log::processor