﻿#pragma once
#include <tbb/concurrent_unordered_map.h>
#include "../../../Utility/src/Engine/Processor/consumer.hpp"
#include <memory>

namespace wlog {
	namespace output {
		class IOutput;
	}
}

namespace wlog {
	namespace processor {
		/**
			@class : LogProcessor
			@date : 2015/05/15
			@brief : 전달 받은 로그 데이터를 Handle함수를 통해서 처리합니다.
			@author : jinny20123(안 정웅)
		*/
		class LogProcessor
			: public CONSUMER(utility::object::message)
		{
		public:

			struct OutputType
			{
				enum OutType
				{
				};
			};

			typedef output::IOutput
				IOutput;
			typedef tbb::concurrent_unordered_map<unsigned int, std::shared_ptr<IOutput>>
				Output_Container;

			LogProcessor(ProducerObj * pproducer);
			virtual ~LogProcessor();

			Output_Container* GetOutputContainer();

		protected:
			virtual void Handle(const object & obj);
		private:
			struct Impl;
			std::unique_ptr<Impl> impl_;
		};
	}
} /// log::processor
