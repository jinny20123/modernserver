﻿#include <boost/filesystem.hpp>
#include <cstdio>
#include <cstdarg>
#include <Windows.h>
#include <memory>
#include "CommonLogger.h"
#include "../LoggerDefine.h"
#include "../Procecessor/LogProcessor.h"
#include "../Output/OutputArgument.h"
#include "../Output/Object/FileOutput.h"
#include "../Output/Object/ConsoleOutput.h"
#include "../../../Utility/src/Object/ArgumentFactory.h"
#include "../../../Utility/src/Time/TimeToString.h"

namespace wlog {
	namespace common {
		/// 내부에서 사용할 변수를 정의합니다.
		/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
		struct CommonLogger::Impl
		{
			std::unique_ptr<LogProcessor::ProducerObj>		log_producer_;
			std::unique_ptr<LogProcessor>					log_process_;
			bool											preset_;
		};

		CommonLogger::CommonLogger()
		{
			impl_ = std::make_unique<Impl>();
			impl_->log_producer_ = std::make_unique<LogProcessor::ProducerObj>();
			impl_->log_process_ = std::make_unique<LogProcessor>(impl_->log_producer_.get());

			impl_->preset_ = false;
		}
		CommonLogger::~CommonLogger()
		{}

		void
		CommonLogger::Prepare()
		{
			if (false == impl_->preset_)
			{
				/// 해당 경로에 폴더 만들기
				boost::filesystem::path log_path = boost::filesystem::current_path() / "/log/";

				if (!boost::filesystem::exists(boost::filesystem::path(log_path)))
				{
					bool bcreate = boost::filesystem::create_directory(log_path);
					BOOST_ASSERT(bcreate);
				}

				LogProcessor::Output_Container* pout_con = impl_->log_process_->GetOutputContainer();
				BOOST_ASSERT(pout_con);

				typedef output::object::ConsoleOutput
					ConsoleOutput;
				typedef output::object::FileOutput
					FileOutput;

				/// 추가할 로그 데이터를 정의합니다.
				pout_con->insert(LogProcessor::Output_Container::value_type(CONSOLE, std::make_shared<ConsoleOutput>()));
				INSERT_FILEOUTPUT(NETWORK, FileOutput, "cnetwork");
				INSERT_FILEOUTPUT(SYSTEM, FileOutput, "csystem");
				INSERT_FILEOUTPUT(LOGIC, FileOutput, "clogic");

				impl_->preset_ = true;
			}
			else
			{
				BOOST_ASSERT(false);
			}
		}

		void
		CommonLogger::ToInfo(OutType output_type, const char* msg, ...)
		{
			char szTemp[log_max_size];
			memset(szTemp, 0x00, sizeof(szTemp));
			VAR_MAKE(szTemp, log_max_size);
			ToWrite(0, output_type, szTemp);
		}

		void
		CommonLogger::ToDebug(OutType output_type, const char* msg, ...)
		{
#ifdef _DEBUG
			char szTemp[log_max_size];
			memset(szTemp, 0x00, sizeof(szTemp));
			VAR_MAKE(szTemp, log_max_size);
			ToWrite(1, output_type, szTemp);
#endif
		}

		void
		CommonLogger::ToWrite(unsigned int grade, OutType& output_type, const char* msg)
		{
			typedef utility::object::ArgumentFactory<output::OutputArgument>
				ArgumentFactory;
			typedef ::utility::time::TimeToString
				TimeToString;

			output::OutputArgument* output_arg = static_cast<output::OutputArgument*>(ArgumentFactory::Create());

			output_arg->grade_ = grade;
			output_arg->msg_.clear();

			TimeToString::GetCurrentTimeToString(output_arg->msg_);
			output_arg->msg_.append(msg);

			impl_->log_producer_->push(static_cast<unsigned int>(output_type), static_cast<void*>(output_arg));
		}

		CommonLogger&
		CommonLogger::GetInstace()
		{
			return CommonLogger::get_mutable_instance();
		}
	}
} /// wlog::common