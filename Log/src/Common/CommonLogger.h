﻿#pragma once
#include <boost/serialization/singleton.hpp>
#include "../Procecessor/LogProcessor.h"

namespace wlog {
	namespace common {
		class CommonLogger
			: public boost::serialization::singleton<CommonLogger>
		{
		public:
			typedef processor::LogProcessor
				LogProcessor;

			const static size_t log_max_size = 1024;

			enum OutType
			{
				OUTPUTTYPE_START = 0,
				CONSOLE = OUTPUTTYPE_START,
				NETWORK,
				SYSTEM,
				LOGIC,
				OUTPUTTYPE_END,
			};

			CommonLogger();
			virtual ~CommonLogger();

			static CommonLogger& GetInstace();

			void Prepare();
			void ToInfo(OutType output_type, const char* msg, ...);
			void ToDebug(OutType output_type, const char* msg, ...);

		private:
			void ToWrite(unsigned int grade, OutType& output_type, const char* msg);
			struct Impl;
			std::unique_ptr<Impl> impl_;
		};
	}
} /// log::common

#define NS_COMMONLOG ::wlog::common::CommonLogger
#define COMMONLOG() ::wlog::common::CommonLogger::GetInstace()
