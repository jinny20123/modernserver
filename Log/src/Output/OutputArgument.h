﻿#pragma once

#include "../../../Utility/src/Object/argument.h"

namespace wlog { namespace output { 

typedef ::utility::object::Argument
		Argument;

/** 
	@class : OutputArgument
	@date : 2015/05/15
	@brief : Log 메시지를 전달하기 위한 Argument를 전달합니다. 
	@author : jinny20123(안 정웅) 
*/
struct OutputArgument 
	: public Argument
{
	unsigned int		grade_;				///> 로그등급 debug, error, trace, info 
	std::string		msg_;				///> 실제메시지
};
	
}} /// wlog::output