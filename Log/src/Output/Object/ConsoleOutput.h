﻿#pragma once
#include "../IOutput.h"

namespace wlog {
	namespace output {
		namespace object {
			/**
				@class : ConsoleOutput
				@date : 2015/04/30
				@brief :
				@author : jinny20123(안 정웅)
			*/
			class ConsoleOutput
				: public IOutput
			{
			public:
				ConsoleOutput();
				virtual ~ConsoleOutput();
				virtual void Write(const char* msg);
			private:
				struct Impl;
				std::unique_ptr<Impl> impl_;
			};
		}
	}
} /// wlog::output::object
