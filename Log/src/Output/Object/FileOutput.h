﻿#pragma once
#include "../IOutput.h"

namespace wlog {
	namespace output {
		namespace object {
			/**
				@class : FileOutput
				@date : 2015/04/30
				@brief :
				@author :
			*/
			class FileOutput
				: public IOutput
			{
			public:
				FileOutput(const char* filename);
				virtual ~FileOutput();
				virtual void Write(const char* msg);
			private:
				void	RunChangeFile();
				void	OpenFile();

				struct Impl;
				std::unique_ptr<Impl> impl_;
			};
		}
	}
} /// log::output::object
