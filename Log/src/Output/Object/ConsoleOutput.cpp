﻿#include <iostream>
#include "ConsoleOutput.h"
namespace wlog {
	namespace output {
		namespace object {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct ConsoleOutput::Impl
			{
				Impl() {}
			};

			ConsoleOutput::ConsoleOutput()
			{
				impl_ = std::make_unique<Impl>();
			}
			ConsoleOutput::~ConsoleOutput()
			{}

			void
				ConsoleOutput::Write(const char* msg)
			{
				std::cout << msg << std::endl;
			}
		}
	}
} /// wlog::output::object