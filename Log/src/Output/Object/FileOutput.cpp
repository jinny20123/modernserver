﻿#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <cstdio>
#include <string.h>
#include "FileOutput.h"

namespace wlog {
	namespace output {
		namespace object {
			//#define _FILE_OC
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct FileOutput::Impl
			{
				FILE* pfile_;
				std::string fiie_path_;
				std::string file_name_;
				unsigned int date_;
			};

			FileOutput::FileOutput(const char* filename)
			{
				impl_ = std::make_unique<Impl>();
				impl_->pfile_ = nullptr;

				impl_->fiie_path_.clear();
				impl_->file_name_.clear();
				impl_->date_ = 0;

				impl_->file_name_.append(filename);

				RunChangeFile();
				/// 버퍼를 거치지 않고 바로 파일에 쓴다.
				setvbuf(impl_->pfile_, NULL, _IONBF, 0);

#ifdef _FILE_OC
				fclose(impl_->pfile_);
				impl_->pfile_ = nullptr;
#endif
			}
			FileOutput::~FileOutput()
			{
				if (nullptr != impl_->pfile_)
				{
					fclose(impl_->pfile_); impl_->pfile_ = nullptr;
				}
			}

			void
				FileOutput::Write(const char* msg)
			{
				char strbuffer[512];
				memset(strbuffer, 0, sizeof(strbuffer));
				size_t idx = 0;
#ifdef _FILE_OC
				OpenFile();
				if (nullptr != impl_->pfile_)
				{
					strcpy_s(strbuffer, sizeof(strbuffer), msg);
					idx += strlen(msg);
					strcpy_s(strbuffer + idx, sizeof(strbuffer) - idx, "\n");
					idx += strlen("\n");
					fwrite(strbuffer, strlen(strbuffer), 1, impl_->pfile_);
					fclose(impl_->pfile_);
					impl_->pfile_ = nullptr;
				}
#else
				if (nullptr != impl_->pfile_)
				{
					strcpy_s(strbuffer, sizeof(strbuffer), msg);
					idx += strlen(msg);
					strcpy_s(strbuffer + idx, sizeof(strbuffer) - idx, "\n");
					idx += strlen("\n");
					fwrite(strbuffer, strlen(strbuffer), 1, impl_->pfile_);
				}
				else
				{
					OpenFile();
				}
#endif
			}

			void
				FileOutput::RunChangeFile()
			{
				time_t curtime;
				struct tm ptm;
				time(&curtime);
				localtime_s(&ptm, &curtime);

				unsigned int date = static_cast<unsigned int>((ptm.tm_year + 1900) * 10000) + static_cast<unsigned int>((ptm.tm_mon + 1) * 100) + static_cast<unsigned int>(ptm.tm_mday);

				if (impl_->date_ != date)
				{
					impl_->date_ = date;
					OpenFile();
				}
			}

			void
				FileOutput::OpenFile()
			{
				std::string open_path;
				open_path.append(impl_->fiie_path_.c_str());
				open_path.append(impl_->file_name_.c_str());
				open_path.append("_");
				open_path.append(boost::lexical_cast<std::string>(impl_->date_).c_str());
				open_path.append(".log");

				if (nullptr != impl_->pfile_)
				{
					fclose(impl_->pfile_);
				}

				fopen_s(&impl_->pfile_, open_path.c_str(), "a+");
			}
		}
	}
} /// wlog::output::object