﻿#pragma once

namespace utility { namespace object{ 
	struct Argument;
}}

namespace wlog { namespace output { 

class IOutput 
{
public: 
	virtual ~IOutput(){};

	/// IOutput 인터페이스를 정의합니다. 
	virtual void Write(const char * msg) = 0;
};

}} /// wlog::output
