﻿#pragma once

#define VAR_MAKE(szTemp, szTempSize )		\
	va_list args;																				\
	va_start( args, msg );																		\
	\
	const int32_t nRetVal = vsnprintf_s( szTemp, sizeof(szTemp), szTempSize, msg, args );		\
	\
	if ( nRetVal < 0 || static_cast<const size_t>(nRetVal) == szTempSize )						\
	{ szTemp[ szTempSize - 1 ] = '\0'; }														\
	\
	va_end(args)

#define INSERT_FILEOUTPUT(type, outputObject, filename) \
pout_con->insert( LogProcessor::Output_Container::value_type(type,		std::make_shared<outputObject>( ( log_path.string() + filename).c_str() ) ) );
