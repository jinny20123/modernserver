﻿
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <memory>
#include <map>
#include <vector>
#include <functional>



// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
#include "../Utility/src/macro.h"
#ifndef _WIN32_WINNT         
#define _WIN32_WINNT 0x0600    // 윈도우 2000 이상 지원
#endif  
#include <boost/asio.hpp>
 