﻿#pragma once
namespace utility { namespace object{ 
	struct Argument;
}}

namespace network { namespace parser { 

/** 
	@class : IParser
	@date : 2016/02/24
	@brief : 세션과 로직과의 연결을 위한 중간 대리인 역활를 하는 객체 입니다.  
	@author : jinny20123(안 정웅) 
*/
class ISessionLogic
{
public: 
	typedef ::utility::object::Argument
			Argument;

	virtual ~IChannel(){};

	/// IChannel 인터페이스를 정의합니다. 
	virtual size_t Recv(Argument * arg)			= 0;
	virtual size_t Send(Argument * arg)			= 0;
	virtual void Close(Argument * arg)			= 0;()

};

}} /// network::parser
