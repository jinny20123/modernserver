﻿#pragma once
#include "../../../Utility/src/Object/argument.h"

namespace network { namespace session { 
	class ISession;
}}

namespace network { namespace parser { 

typedef ::utility::object::Argument
	Argument;
typedef ::network::session::ISession
	ISession;

struct ParserArgument
	: public Argument
{
	ParserArgument()
	{}
	ParserArgument(char * buffer, size_t buffer_size, ISession * psession) :
	buffer_(buffer), buffer_size_(buffer_size),  psession_(psession)
	{}

	char		* buffer_;
	size_t		buffer_size_;
	ISession	* psession_;	 
};


}} /// network::parser
