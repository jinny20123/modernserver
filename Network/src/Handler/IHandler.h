﻿#pragma once
namespace utility {
	namespace object {
		struct Argument;
	}
}

namespace network {
	namespace handler {
		/**
			@class : IHandler
			@date : 2015/05/07
			@brief : 네트워크로 전달받은 패킷 데이터를 목적에 따라서 분기해주는 역활을 담당합니다.
			@author : jinny20123(안 정웅)
		*/
		class IHandler
		{
		public:
			using Argument = ::utility::object::Argument;

			virtual ~IHandler() {};

			/// IHandler 인터페이스를 정의합니다.
			virtual void Prepare() = 0;
			virtual void OnRecv(Argument* arg) = 0;
			virtual void OnSend(Argument* arg) = 0;
			virtual void OnReservation(Argument* arg) = 0;
			virtual void OnClose(Argument* arg) = 0;

			enum class HANDLE_TYPE : int
			{
				HANDLE_START = 0,
				HANDLE_ONRECV = HANDLE_START,
				HANDLE_ONSEND,
				HANDLE_ONRESERVATION,
				HANDLE_CLOSE,
			};
		};
	}
} /// network::handler
