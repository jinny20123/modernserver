﻿#pragma once
#include <zookeeper.h>

namespace network { namespace util { namespace watcher { 

void watcher(zhandle_t * zh, int type, int state, const char * path, void * watcherCtx);
	
}}} /// network::util::watcher