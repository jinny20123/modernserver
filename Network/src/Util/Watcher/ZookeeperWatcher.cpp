﻿#include "stdafx.h"
#include "ZookeeperWatcher.h"
#include "../../Session/Object/ZookeeperSession.h"
#include "../../Channel/ChannelArgument.h"

namespace network {
	namespace util {
		namespace watcher {
			typedef ::network::session::object::ZookeeperSession
				ZookeeperSession;

			void ConnectingStateWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
			{
				ZookeeperSession* zoo_session = static_cast<ZookeeperSession*>(watcherCtx);
				if (type == ZOO_CREATED_EVENT)
				{
				}
				else if (type == ZOO_DELETED_EVENT)
				{
				}
				else if (type == ZOO_CHANGED_EVENT)
				{
				}
				else if (type == ZOO_CHILD_EVENT)
				{
				}
				else if (type == ZOO_SESSION_EVENT)
				{
				}
				else if (type == ZOO_NOTWATCHING_EVENT)
				{
				}
			}

			void ConnectedStateWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
			{
				ZookeeperSession* zoo_session = static_cast<ZookeeperSession*>(watcherCtx);

				if (type == ZOO_CREATED_EVENT)
				{
					int a = 0;
				}
				else if (type == ZOO_DELETED_EVENT)
				{
				}
				else if (type == ZOO_CHANGED_EVENT)
				{
				}
				else if (type == ZOO_CHILD_EVENT)
				{
					int a = 0;
				}
				else if (type == ZOO_SESSION_EVENT)
				{
					typedef ::network::channel::ZookeeperChannelArgumentRegist
						ZookeeperChannelArgumentRegist;
					zoo_session->Send(&ZookeeperChannelArgumentRegist(zoo_session, zoo_session->GetPath(), zoo_session->GetValue()));
				}
				else if (type == ZOO_NOTWATCHING_EVENT)
				{
				}
			}

			void watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
			{
				ZookeeperSession* zoo_session = static_cast<ZookeeperSession*>(watcherCtx);

				if (state == ZOO_CONNECTING_STATE)
				{
					ConnectingStateWatcher(zh, type, state, path, watcherCtx);
					//LOG().ToInfo(NS_LOG::KeyType::ZOOKEEPER, "[%s][%d] [watcher] ZOO_CONNECTING_STATE type(%d)", __FILE__, __LINE__, type );
				}
				else if (state == ZOO_CONNECTED_STATE)
				{
					ConnectedStateWatcher(zh, type, state, path, watcherCtx);
					//LOG().ToInfo(NS_LOG::KeyType::ZOOKEEPER, "[%s][%d] [watcher] ZOO_CONNECTED_STATE type(%d)", __FILE__, __LINE__, type );
				}
				else
				{
					//LOG().ToInfo(NS_LOG::KeyType::ZOOKEEPER, "[%s][%d] [watcher] state(%d) type(%d)", __FILE__, __LINE__, state, type );
				}
			}
		}
	}
} /// network::util::watcher