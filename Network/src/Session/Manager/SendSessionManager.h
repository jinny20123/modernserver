﻿#pragma once
#include "../Session_fwd.h"
#include "../Type.h"
#include <vector>
#include <algorithm>

namespace network {
	namespace session {
		namespace manager {
			/**
				@class : SendSessionManager
				@date : 2015/04/30
				@brief :
				@author :
			*/
			class SendSessionManager
			{
			public:
				typedef ::network::session::SessionType
					SessionType;

				SendSessionManager();
				virtual ~SendSessionManager();

				SessionBase* Aquire(SessionType type);
				void Release(SessionType type, SessionBase* psession);

				class ScopeSessionManager
				{
				public:
					typedef std::pair<SessionType, SessionBase*>
						Value;
					typedef std::vector<Value>
						Container;
					ScopeSessionManager(SendSessionManager* sessionmanager) : sessionmanager_(sessionmanager)
					{
					}
					~ScopeSessionManager()
					{
						std::for_each(container_.begin(), container_.end(),
							[this](Value value)
							{
								this->sessionmanager_->Release(value.first, value.second);
							}
						);
					}

					SessionBase* Get(SessionType type)
					{
						SessionBase* psession = sessionmanager_->Aquire(type);
						if (nullptr != psession)
						{
							container_.push_back(Value(type, psession));
						}
						return psession;
					}

				private:
					SendSessionManager* sessionmanager_;
					Container				container_;
				};
			private:
				struct Impl;
				std::unique_ptr<Impl> impl_;
			};
		}
	}
} /// network::session::manager
