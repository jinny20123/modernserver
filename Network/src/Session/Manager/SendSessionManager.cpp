﻿#include "stdafx.h"
#include <unordered_map>
#include "SendSessionManager.h"
#include "../../../../Utility/src/Object/MultiThread.h"
#include "../../../../Utility/src/Pool/object_pool.h"
namespace network {
	namespace session {
		namespace manager {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct SendSessionManager::Impl
			{
				using Pool = utility::pool::ObjectPool<SessionBase, ::utility::object::MultiThread>;
				using Container = std::unordered_map<SessionType, Pool*>;

				std::shared_ptr<Container> container_;

				inline Pool* con_find(SessionType type)
				{
					Container::iterator iter = container_->find(type);
					if (iter == container_->end())
					{
						return nullptr;
					}

					return iter->second;
				}
			};

			SendSessionManager::SendSessionManager()
			{
				impl_ = std::make_unique<Impl>();
				impl_->container_ = std::make_shared<Impl::Container>();

				impl_->container_->insert(Impl::Container::value_type(SessionType::REDIS, new Impl::Pool));
				impl_->container_->insert(Impl::Container::value_type(SessionType::ZOOKEEPER, new Impl::Pool));
			}
			SendSessionManager::~SendSessionManager()
			{}

			SessionBase*
				SendSessionManager::Aquire(SessionType type)
			{
				Impl::Pool* ppool = impl_->con_find(type);
				if (nullptr == ppool)
				{
					return nullptr;
				}

				SessionBase* psession;
				ppool->Acquire(psession);

				return psession;
			}
			void
				SendSessionManager::Release(SessionType type, SessionBase* psession)
			{
				Impl::Pool* ppool = impl_->con_find(type);
				if (nullptr == ppool)
				{
					return;
				}

				ppool->Restore(psession);
			}
		}
	}
} /// network::session::manager