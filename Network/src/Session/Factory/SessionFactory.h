﻿#pragma once

#include "../../../../Utility/src/macro.h"
#include "../../../../Utility/src/Factory/Container/factory_container.h"
#include <boost/shared_ptr.hpp>
#include <boost/serialization/singleton.hpp>

namespace utility { namespace object {
	struct Argument;
}}

namespace network { namespace session {
	class SessionBase;
}}
namespace network { namespace session { namespace factory { 


/** 
	@class : SessionFactory
	@date : 2015/04/30
	@brief : 세션 오브젝트를 관리하는 팩토리 객체입니다. 
	@author : jinny20123(안 정웅)
*/
template<typename Type>
class SessionFactory 
	: private boost::serialization::singleton< SessionFactory<Type> >
{
public:
	typedef ::utility::object::Argument
			Argument; 
	typedef ::network::session::SessionBase
			Interface;

	SessionFactory() {};
	virtual ~SessionFactory() {};

	Interface * Create(Argument * arg = nullptr)
	{
		auto pobj = factory_.Create();
		pobj->Capture(arg);
		return pobj;
	}

	void Destroy( Interface * pobj)
	{
		pobj->Release();
		factory_.Destroy(pobj);
	}

	static SessionFactory<Type> & GetInstance()
	{
		return get_mutable_instance();
	}

private :
	::utility::factory::Factory<Interface, Type > factory_;
};

TEMPLATE_SINGLETON_CALLFUNC(SessionFactory, SESSION_FACTORY)
	
}}} /// network::session::factory