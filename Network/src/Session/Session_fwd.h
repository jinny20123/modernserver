﻿#pragma once
#include <functional>

namespace network { namespace session { 

using CloseCallBack = std::function<void()>;
class SessionBase;


}} /// network::session
