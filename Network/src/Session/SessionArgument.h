﻿#pragma once

#include "../../../Utility/src/Object/argument.h"
#include "../../../Utility/src/RedisClient/redisclient.h"
#include <functional>
#include <zookeeper.h>
#include <boost/asio.hpp>


namespace packet {
	struct Packet;
}

namespace network { namespace channel { 
	class IChannel;
}}

namespace network { namespace util { namespace endpoint {
	class IEndPoint;
}}}

namespace network { namespace session { namespace logic { 
	class ISessionLogic;
}}}

namespace network { namespace session { 

class SessionBase;

using Argument = ::utility::object::Argument;	
using IChannel = ::network::channel::IChannel;
using ISessionLogic =::network::session::logic::ISessionLogic;
/** 
	@class : UdpSessionCreateArgument
	@date : 2015/05/07
	@brief : Udp 세션 생성시 전달할 매개변수를 정의합니다. 
	@author : jinny20123(안 정웅) 
*/

struct SessionCreateArgument
	: public Argument
{
	SessionCreateArgument() {}
	SessionCreateArgument(IChannel * pchannel) : pchannel_(pchannel)
	{}

	virtual bool IsEnable()
	{
		if ( nullptr == this->pchannel_ ) 
		{ return false; }  
		return true;
	}

	IChannel	*				pchannel_;
};

struct AsioTcpSessionCreateArgument
	: public Argument
{
	typedef ::boost::asio::ip::tcp::socket
			AsioTcpSocket;
	
	AsioTcpSessionCreateArgument(AsioTcpSocket * socket, ISessionLogic * psessionlogic) : 
	psocket_(socket), psessionlogic_(psessionlogic)
	{	
	}

	AsioTcpSocket	*			psocket_;
	ISessionLogic	*			psessionlogic_;
};

struct AsioUdpSessionCreateArgument
	: public SessionCreateArgument
{
	AsioUdpSessionCreateArgument() {}
	AsioUdpSessionCreateArgument(boost::asio::io_service * pio_service, unsigned short port, IChannel * pchannel) :
	pio_service_(pio_service), bindport_(port) , SessionCreateArgument(pchannel)
	{	
	}

	boost::asio::io_service *	pio_service_;
	unsigned short					bindport_;
};


/** 
	@class : UdpSessionPacketArgument
	@date : 2015/05/07
	@brief : Udp 세션으로 네트워크 통신작업시 전달할 매개변수를 정의합니다. 
	@author : jinny20123(안 정웅) 
*/
struct UdpSessionPacketArgument
	: public Argument
{
	typedef ::network::util::endpoint::IEndPoint
			IEndPoint;
	typedef ::packet::Packet
			Packet;

	UdpSessionPacketArgument()
	{		
	}

	virtual bool IsEnable()
	{
		if ( nullptr == this->ppacket_ ||
			nullptr == this->psession_ ) 
		{ return false; }  

		return true;
	}

	Packet	*							ppacket_;
	IEndPoint *							pendpoint_;
	SessionBase *							psession_;
};
/** 
	@class : ENetSessionCreateArgument
	@date : 2015/07/02
	@brief : EnetSession을 생성시 필요한 매개변수를 정의합니다.  
	@author : jinny20123(안 정웅) 
*/
struct ENetSessionCreateArgument
	: public SessionCreateArgument
{
	ENetSessionCreateArgument() {}
	ENetSessionCreateArgument( unsigned short max_connection, unsigned short port, IChannel * pchannel) :
	max_connection_(max_connection), bindport_(port), SessionCreateArgument(pchannel)
	{}

	virtual bool IsEnable()
	{
		if ( nullptr == this->pchannel_ ) 
		{ return false; }  
		return true;
	}

	unsigned short					max_connection_;
	unsigned short					bindport_;
};

struct RedisSessionCreateArgument
	: public SessionCreateArgument
{
	RedisSessionCreateArgument() {}
	RedisSessionCreateArgument(boost::asio::io_service * pio_service, IChannel * pchannel) :
	pio_service_(pio_service), SessionCreateArgument(pchannel)
	{}

	virtual bool IsEnable()
	{
		return SessionCreateArgument::IsEnable();
	}

	boost::asio::io_service *	pio_service_;
};

struct SessionConnectArgument
	: public Argument
{
	SessionConnectArgument() {}	
	SessionConnectArgument(std::string ip, unsigned short port)  : ipaddress_(ip), port_(port)
	{}

	std::string					ipaddress_;
	unsigned short					port_;
};

struct RedisSessionPacketArgument
	: public Argument
{
	typedef std::function<void(const RedisValue &)>
			RedisSendCallBack;

	virtual bool IsEnable()
	{
		if( nullptr == callback_)
		{ return false; }

		return true;
	}

	RedisSessionPacketArgument() {}
	RedisSessionPacketArgument(std::string & command, SessionBase * psession, RedisSendCallBack callback) : command_(command), psession_(psession), callback_(callback)
	{}

	std::string			command_;
	SessionBase	*		psession_;
	RedisSendCallBack	callback_;
};

struct ZookeeperSessionConnectArgument
	: public SessionConnectArgument
{

	ZookeeperSessionConnectArgument(){}
	ZookeeperSessionConnectArgument( std::string ip, unsigned short port, int32_t time_out, std::string path, std::string value, watcher_fn watcher ) : SessionConnectArgument(ip, port), time_out_(time_out), path_(path), value_(value), watcher_(watcher)
	{}
	
	int32_t time_out_;
	std::string path_;
	std::string value_;
	watcher_fn watcher_;
};

}} /// network::session
