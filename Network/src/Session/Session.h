﻿#pragma once
#include <memory>
#include <functional>
namespace utility { namespace object{ 
	struct Argument;
}}

namespace network { namespace channel { 
	class IChannel;
}}

namespace network { 
namespace session { 

enum class SessionCallBackType : int {
	ACCEPTED,
	CONNECTED,
	SENDED,
	RECVED,
	CLOSED,
	MAX,
};

/** 
	@class : SessionBase
	@date : 2015/05/11
	@brief : 네트워크 통신을 할수 있는 객체 단위를 인터페이스를 제공합니다. 
	@author : jinny20123(안 정웅) 
*/
class SessionBase 
{
public: 
	using IChannel = channel::IChannel;
	using Argument = ::utility::object::Argument;
	using CallBackFunc = std::function<void(SessionBase* psession)>;

	SessionBase();
	virtual ~SessionBase(){};

	/// SessionBase 인터페이스를 정의합니다. 
	virtual bool Capture(Argument * arg)		= 0;
	virtual void Release()						= 0;
	virtual void Connect(Argument * arg)		= 0;
	virtual void Send(Argument * arg)			= 0;
	virtual void Recv()							= 0;
	virtual void Close()						= 0;

	void Accepted();

	bool RegistCallBackFunc(SessionCallBackType type, CallBackFunc f);
protected:
	
	const CallBackFunc FindCallBackFunc(SessionCallBackType type) const;
private:
	struct Impl;
	std::shared_ptr<Impl> impl_;
};

}} /// network::session
