﻿#include "stdafx.h"
#include <boost/asio.hpp>
#include "ZookeeperSession.h"
#include "../SessionArgument.h"
#include "../../Channel/Object/ZookeeperChannel.h"

namespace network {
	namespace session {
		namespace object {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct ZookeeperSession::Impl
			{
				Impl() {}
				zhandle_t* zh;
				std::shared_ptr<IChannel>		pchannel_;
				int								watcher_state_;
				std::string						zookeeper_server_ip_;
				std::string						path_;
				std::string						value_;
				int32_t							timeout_;
				watcher_fn						watcher_;
			};

			ZookeeperSession::ZookeeperSession()
			{
				impl_ = std::make_unique<Impl>();
				impl_->zh = nullptr;
				impl_->pchannel_.reset();

				zoo_set_debug_level(ZOO_LOG_LEVEL_WARN);
			}
			ZookeeperSession::~ZookeeperSession()
			{
				Close();
			}

			bool
				ZookeeperSession::Capture(Argument* arg)
			{
				using SessionCreateArgument = ::network::session::SessionCreateArgument;

				SessionCreateArgument* psc_argument = nullptr;
				::utility::object::ArgumentUtil::CastArgument(arg, psc_argument);
				if (nullptr == psc_argument) { return false; }

				impl_->pchannel_.reset(psc_argument->pchannel_);

				return true;
			}

			void
				ZookeeperSession::Release()
			{
			}

			void
				ZookeeperSession::Connect(Argument* arg)
			{
				using ZookeeperSessionConnectArgument = ::network::session::ZookeeperSessionConnectArgument;

				ZookeeperSessionConnectArgument* pzsc_argument = nullptr;
				::utility::object::ArgumentUtil::CastArgument(arg, pzsc_argument);
				if (nullptr == pzsc_argument) { return; }

				impl_->path_ = pzsc_argument->path_;
				impl_->value_ = pzsc_argument->value_;

				impl_->zookeeper_server_ip_.append(pzsc_argument->ipaddress_);
				impl_->zookeeper_server_ip_.append(":");
				impl_->zookeeper_server_ip_.append(std::to_string(static_cast<long long>(pzsc_argument->port_)));

				impl_->timeout_ = pzsc_argument->time_out_;
				impl_->watcher_ = pzsc_argument->watcher_;

				impl_->watcher_state_ = (int)CONNECTING;
				impl_->zh = zookeeper_init(impl_->zookeeper_server_ip_.c_str(), impl_->watcher_, impl_->timeout_, 0, this, 0);
			}

			void
				ZookeeperSession::Send(Argument* arg)
			{
				impl_->pchannel_->Send(arg);
			}

			void
				ZookeeperSession::Recv()
			{
			}

			void
				ZookeeperSession::Close()
			{
				if (nullptr != impl_->zh)
				{
					zookeeper_close(impl_->zh);
					impl_->zh = nullptr;
				}
			}
			bool
				ZookeeperSession::IsConnect()
			{
				return nullptr != impl_->zh;
			}

			void
				ZookeeperSession::ReConnect()
			{
				if (nullptr != impl_->zh)
				{
					Close();
				}

				impl_->watcher_state_ = (int)CONNECTING;
				impl_->zh = zookeeper_init(impl_->zookeeper_server_ip_.c_str(), impl_->watcher_, impl_->timeout_, 0, this, 0);
			}

			void*
				ZookeeperSession::GetSocket()
			{
				return impl_->zh;
			}

			const char*
				ZookeeperSession::GetPath()
			{
				return impl_->path_.c_str();
			}
			const char*
				ZookeeperSession::GetValue()
			{
				return impl_->value_.c_str();
			}
		}
	}
} /// network::session::object