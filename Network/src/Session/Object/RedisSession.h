﻿#pragma once
#include "../Session.h"
#include <boost/shared_ptr.hpp>

class RedisAsyncClient;
namespace network {
	namespace session {
		namespace object {
			/**
				@class : RedisSession
				@date : 2015/04/30
				@brief : Redis와 연결을 위한 세션을 정의합니다.
				@author : jinny20123(안 정웅)
			*/
			class RedisSession
				: public SessionBase
			{
			public:
				using Socket = RedisAsyncClient;
				RedisSession();
				virtual ~RedisSession();
				virtual bool Capture(Argument* arg) override;
				virtual void Release() override;
				virtual void Connect(Argument* arg) override;
				virtual void Send(Argument* arg) override;
				virtual void Recv() override;
				virtual void Close() override;
				Socket* GetSocket();
			private:
				struct Impl;
				std::unique_ptr<Impl> impl_;
			};
		}
	}
} /// network::session::object
