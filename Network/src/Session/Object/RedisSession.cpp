﻿#include "stdafx.h"
#include "RedisSession.h"
#include "../SessionArgument.h"
#include "../../Channel/Object/RedisSendChannel.h"
#include "../../../../Utility/src/RedisClient/redisclient.h"
#include <boost/bind.hpp>

namespace network {
namespace session {
namespace object {
/// 내부에서 사용할 변수를 정의합니다.
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct RedisSession::Impl
{
	boost::asio::io_context				io_service_;
	std::shared_ptr<RedisClient>		predis_client_;
	std::shared_ptr<IChannel>			pchannel_;
	bool								bConnect;
};

RedisSession::RedisSession()
{
	impl_ = std::make_unique<Impl>();
	impl_->predis_client_.reset();
}
RedisSession::~RedisSession()
{
}

bool
RedisSession::Capture(Argument* arg)
{
	RedisSessionCreateArgument* prsc_argument = nullptr;
	utility::object::ArgumentUtil::CastArgument(arg, prsc_argument);

	if (nullptr == prsc_argument ||
		false == prsc_argument->IsEnable())
	{
		return false;
	}

	impl_->predis_client_ = std::make_shared<RedisClient>(*prsc_argument->pio_service_);
	impl_->pchannel_.reset(prsc_argument->pchannel_);
	impl_->bConnect = false;

	return true;
}

void
RedisSession::Release()
{
	impl_->predis_client_.reset();
}

void
RedisSession::Connect(Argument* arg)
{
	namespace ip = ::boost::asio::ip;
	SessionConnectArgument* prsc_argument = nullptr;
	utility::object::ArgumentUtil::CastArgument(arg, prsc_argument);

	if (nullptr == prsc_argument)
	{
		return;
	}

	ip::tcp::endpoint endpoint(ip::address::from_string(prsc_argument->ipaddress_.c_str()), prsc_argument->port_);
	impl_->predis_client_->asyncConnect(endpoint,
		[this](bool ret, const std::string& msg)
		{
			BOOST_ASSERT(true == ret);
			impl_->bConnect = true;
		}
	);
}
//
//bool
//RedisSession::IsConnect()
//{
//	return impl_->bConnect;
//}

void
RedisSession::Send(Argument* arg)
{
	RedisSessionPacketArgument* prss_argument = nullptr;
	utility::object::ArgumentUtil::CastArgument(arg, prss_argument);
	prss_argument->psession_ = this;

	impl_->pchannel_->Send(arg);
}
void
RedisSession::Recv()
{
}

void
RedisSession::Close()
{
	impl_->bConnect = false;
}

RedisSession::Socket*
RedisSession::GetSocket()
{
	return impl_->predis_client_.get();
}

}}} /// network::session::object