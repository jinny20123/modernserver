﻿#pragma once
#include "../Session.h"
#include "../../../../Utility/src/Factory/Container/factory_container.h"
#include "../../../../Utility/src/Policy/Create/new_create_policy.h"
#include "../../../../Utility/src/Object/SingleThread.h"
#include <memory>
#include <boost/asio.hpp>
#include <boost/serialization/singleton.hpp>


namespace network { namespace session { namespace object { 

/**
@class : AsioTcpSession
@date : 2015/04/30
@brief : Redis와 연결을 위한 세션을 정의합니다.
@author : jinny20123(안 정웅)
*/
class AsioTcpSession 
	: public SessionBase
{
public: 
	using Socket = boost::asio::ip::tcp::socket;
	using CloseCallBack = std::function<void(AsioTcpSession*)>;

	AsioTcpSession();
	virtual ~AsioTcpSession();
	virtual bool Capture(Argument * arg) override;
	virtual void Release() override;
	virtual void Connect(Argument * arg) override;
	virtual void Send(Argument * arg) override;
	virtual void Recv() override;
	virtual void Close() override;

	bool IsConnect();
	bool RegistCloseCustomFunc(CloseCallBack close_f);
private :

	Socket * GetSocket();

	struct Impl;
	std::unique_ptr<Impl> impl_;
};

template<typename Type>
class AsioSocketFactory 
	: private boost::serialization::singleton< AsioSocketFactory<Type> >
{
public:
	typedef ::utility::object::Argument
		Argument; 
	typedef boost::asio::ip::tcp::socket
		Interface;

	AsioSocketFactory(){};
	virtual ~AsioSocketFactory(){};

	Interface * Create(Argument * arg = nullptr)
	{
		auto pobj = factory_.Create();
		return pobj;
	}

	void Destroy( Interface * pobj)
	{
		factory_.Destroy(pobj);
	}

	static AsioSocketFactory<Type> & GetInstance()
	{
		return get_mutable_instance();
	}

private :
	::utility::factory::Factory<Interface, Type, ::utility::object::SingleThread ,::utility::policy::create::NoneCreatePolicy > factory_;
};

	
}}} /// network::session::object