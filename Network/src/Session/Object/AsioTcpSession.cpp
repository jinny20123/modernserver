﻿#include "stdafx.h"
#include "AsioTcpSession.h"
#include "../Type.h"
#include "../SessionArgument.h"
#include "../Factory/SessionFactory.h"
#include "../Logic/ISessionLogic.h"
#include "../Logic/SessionLogicArgument.h"
#include "../../Channel/IChannel.h"
#include "../../Channel/ChannelArgument.h"
#include "../../../../Utility/src/Object/argument.h"
#include <iostream>

namespace network {
namespace session {
namespace object {
/// 내부에서 사용할 변수를 정의합니다.
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct AsioTcpSession::Impl
{
	Impl() {}
	typedef channel::IChannel
		IChannel;
	typedef boost::asio::ip::tcp::socket
		Socket;
	typedef ::network::session::logic::ISessionLogic
		ISessionLogic;

	Socket*					psocket_;
	ISessionLogic*			psessionlogic_;
	char					recv_data_[MAX_BUFFER_SIZE * 2];
	// 남은 버퍼가 recv_data_에 복사된 위치
	size_t					rest_pos_;

	CloseCallBack			close_callback_;
};

AsioTcpSession::AsioTcpSession()
{
	MAKE_UNIQUE(impl_, Impl);
	memset(impl_->recv_data_, 0x00, sizeof(impl_->recv_data_));
	impl_->rest_pos_ = 0;
	impl_->close_callback_ = nullptr;
}
AsioTcpSession::~AsioTcpSession()
{}

void
AsioTcpSession::Connect(Argument* arg)
{
	boost::asio::ip::tcp::endpoint endpoint(
		boost::asio::ip::address::from_string("127.0.0.1"), 12000);

	impl_->psocket_->connect(endpoint);
}

bool
AsioTcpSession::IsConnect()
{
	return impl_->psocket_->is_open();
}

bool
AsioTcpSession::Capture(Argument* arg)
{
	typedef ::network::session::logic::SessionLogicArgument
		SessionLogicArgument;

	AsioTcpSessionCreateArgument* ptcp_argument = nullptr;
	::utility::object::ArgumentUtil::CastArgument(arg, ptcp_argument);

	if (nullptr == ptcp_argument ||
		false == ptcp_argument->IsEnable())
	{
		return false;
	}

	// 사용할 소켓 등록
	impl_->psocket_ = ptcp_argument->psocket_;
	impl_->psessionlogic_ = ptcp_argument->psessionlogic_;

	impl_->psessionlogic_->Capture(SessionLogicArgument(this));
	impl_->close_callback_ = nullptr;

	// 세션 사용하기 전에 초기화 작업
	memset(impl_->recv_data_, 0x00, sizeof(impl_->recv_data_));
	impl_->rest_pos_ = 0;

	return true;
}

void
AsioTcpSession::Release()
{
	AsioSocketFactory<boost::asio::ip::tcp::socket>::GetInstance().Destroy(impl_->psocket_);
	impl_->psessionlogic_->Release();
}

void
AsioTcpSession::Send(Argument* arg)
{
	typedef ::network::session::logic::SessionLogicSendArgument
		SessionLogicSendArgument;

	SessionLogicSendArgument* sa_arg;
	::utility::object::ArgumentUtil::CastArgument(arg, sa_arg);
	if (nullptr == sa_arg) { return; }

	impl_->psocket_->async_send(boost::asio::buffer(sa_arg->buffer_, sa_arg->buffer_size_),
		[this](boost::system::error_code ec, std::size_t bytes_recvd)
		{
			if (0 != ec.value())
			{
				std::cerr << ec.message().c_str() << std::endl;
				Close();
				return;
			}
			// 패킷 pool에 반납 처리를 등록해야 합니다. 외부에서 전달된 send closer 함수를 호출을 받도록 
			// 인터페이스 수정 필요
		});
}

void
AsioTcpSession::Recv()
{
	impl_->psocket_->async_receive(
		boost::asio::buffer(impl_->recv_data_ + impl_->rest_pos_, MAX_BUFFER_SIZE),
		[this](boost::system::error_code ec, std::size_t bytes_recvd)
		{
			typedef ::network::session::logic::SessionLogicRecvArgument
				SessionLogicRecvArgument;

			if (0 != ec.value())
			{
				std::cerr << ec.message().c_str() << std::endl;
				Close();
				return;
			}

			size_t buffer_size = bytes_recvd + impl_->rest_pos_;
#ifdef _DEBUG
			if (buffer_size > 0)
			{
			}
#endif

			// 처리한 버퍼 사이즈
			size_t processing_size = 0;
			bool bTryRecv = true;

			try
			{
				auto result = impl_->psessionlogic_->Recv(SessionLogicRecvArgument(impl_->recv_data_, buffer_size, this));
				processing_size = result.first;
				bTryRecv = result.second;
			}
			catch (::network::session::logic::ERROR_TYPE /*type*/)
			{
				Close();
				return;
			}

			if (processing_size == buffer_size)
			{
				impl_->rest_pos_ = 0;
			}
			else
			{
				// 남아 있는 버퍼가 있다면
				if (buffer_size > processing_size)
				{
					// 남은 버퍼를 앞쪽으로 이동시킵니다.
					//if( processing_size > 0 )
					{
						memmove(impl_->recv_data_, impl_->recv_data_ + processing_size, buffer_size - processing_size);
						impl_->rest_pos_ = buffer_size - processing_size;
					}
				}
				else
				{
					// 비정상적인 상태입니다. 우선 접속을 종료 시킵니다.
					// Todo 추적을 위한 로그 필요
					Close();
					return;
				}
			}

			// 다시 데이터를 받을 준비를 합니다.
			if (bTryRecv)
			{
				Recv();
			}
		}
	);
}

void
AsioTcpSession::Close()
{
	typedef ::network::session::logic::SessionLogicCloseArgument
		SessionLogicCloseArgument;

	// 로직적으로 종료 처리를 진행합니다.
	if (true == impl_->psocket_->is_open())
	{
		// 커스텀 callback이 있다면 호출해줍니다.
		auto & fun = FindCallBackFunc(SessionCallBackType::CLOSED);
		if (fun) { fun(this); }
		
		impl_->psocket_->close();

		impl_->psessionlogic_->Close(SessionLogicCloseArgument(this,
			[this]()
			{
				typedef ::network::session::factory::SessionFactory<AsioTcpSession>
					AsioTcpSessionFactory;
				AsioTcpSessionFactory::GetInstance().Destroy(this);
			}));
	}
}

AsioTcpSession::Socket*
	AsioTcpSession::GetSocket()
{
	return impl_->psocket_;
}

bool 
AsioTcpSession::RegistCloseCustomFunc(CloseCallBack close_f) {
	impl_->close_callback_ = close_f;
	return true;
}

}}} /// network::session::object