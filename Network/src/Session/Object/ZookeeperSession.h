﻿#pragma once
#include <boost/shared_ptr.hpp>
#include <zookeeper.h>
#include "../Session.h"

namespace network {
	namespace session {
		namespace object {
			/**
				@class : ZookeeperSession
				@date : 2015/07/16
				@brief : Zookeeper Server와의 연결을 위한 세션을 정의합니다.
				@author : jinny20123(안 정웅)
			*/
			class ZookeeperSession
				: public SessionBase
			{
			public:
				enum
				{
					NONE = 0,
					CONNECTING = 1,
					CONNECTED,
				};

				ZookeeperSession();
				virtual ~ZookeeperSession();
				virtual bool Capture(Argument* arg) override;
				virtual void Release() override;
				virtual void Connect(Argument* arg) override;
				virtual void Send(Argument* arg) override;
				virtual void Recv() override;
				virtual void Close() override;

				bool IsConnect();
				void ReConnect();
				void* GetSocket();
				const char* GetPath();
				const char* GetValue();
			private:
				struct Impl;
				std::unique_ptr<Impl> impl_;
			};
		}
	}
} /// network::session::object
