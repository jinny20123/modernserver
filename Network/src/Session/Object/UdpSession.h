﻿#pragma once
#include <boost/asio.hpp>
#include <memory>
#include "../Session.h"

namespace network {
	namespace session {
		namespace object {
			/**
				@class : UdpSession
				@date : 2015/05/11
				@brief : Udp통신을 할수 있는 세션객체의 구현부를 정의합니다.
				@author : jinny20123(안 정웅)
			*/
			class UdpSession
				: public SessionBase
			{
			public:
				using Socket = boost::asio::ip::udp::socket;

				UdpSession();
				virtual ~UdpSession();
				virtual bool Capture(Argument* arg) override;
				virtual void Release() override;
				virtual void Connect(Argument* arg) override;
				virtual void Send(Argument* arg) override;
				virtual void Recv() override;
				virtual void Close() override;

				bool IsConnect();
				virtual Socket* GetSocket();
			private:
				struct Impl;
				std::unique_ptr<Impl> impl_;
			};
		}
	}
} /// network::session::object
