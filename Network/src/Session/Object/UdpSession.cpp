﻿#include "stdafx.h"
#include <tbb/concurrent_queue.h>
#include "UdpSession.h"
#include "../SessionArgument.h"
#include "../../Channel/IChannel.h"
#include "../../Channel/ChannelArgument.h"
#include "../../../../Utility/src/Object/argument.h"
#include "../../../../CommonPacket/src/Packet.h"

namespace network {
	namespace session {
		namespace object {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct UdpSession::Impl
			{
				Impl() {}
				typedef channel::IChannel
					IChannel;

				std::shared_ptr<Socket>								psocket_;
				std::shared_ptr<IChannel>							pchannel_;
			};

			UdpSession::UdpSession()
			{
				MAKE_UNIQUE(impl_, Impl);
			}
			UdpSession::~UdpSession()
			{}

			bool
				UdpSession::Capture(Argument* arg)
			{
				AsioUdpSessionCreateArgument* pusc_argument = nullptr;
				utility::object::ArgumentUtil::CastArgument(arg, pusc_argument);

				if (nullptr == pusc_argument ||
					false == pusc_argument->IsEnable())
				{
					return false;
				}

				// 사용할 소켓 생성
				impl_->psocket_ = std::make_shared<Socket>(*pusc_argument->pio_service_, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), pusc_argument->bindport_));

				// 채널 생성 및 세션 등록
				impl_->pchannel_.reset(pusc_argument->pchannel_);
				if (nullptr == impl_->pchannel_.get())
				{
					assert(FALSE);
					return false;
				}

				return true;
			}

			void
				UdpSession::Release()
			{
				impl_->pchannel_.reset();
			}

			UdpSession::Socket*
				UdpSession::GetSocket()
			{
				return impl_->psocket_.get();
			}

			void
				UdpSession::Connect(Argument* arg)
			{
			}

			bool
				UdpSession::IsConnect()
			{
				// 사용하지 말아야합니다.
				//BOOST_ASSERT( false );
				return false;
			}

			void
				UdpSession::Send(Argument* arg)
			{
				UdpSessionPacketArgument* pusp_argument = nullptr;
				utility::object::ArgumentUtil::CastArgument(arg, pusp_argument);

				if (nullptr == pusp_argument ||
					false == pusp_argument->IsEnable())
				{
					return;
				}
				pusp_argument->psession_ = this;

				impl_->pchannel_->Send(arg);
			}

			void
				UdpSession::Recv()
			{
				// Recv를 통해서 패킷을 받을 수 있도록 등록합니다.
				channel::ChannelRecvArgument arg(this);
				impl_->pchannel_->Recv(&arg);
			}

			void
				UdpSession::Close()
			{
			}
		}
	}
} /// network::session::object