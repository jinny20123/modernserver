﻿#include "stdafx.h"
#include "Session.h"
#include <unordered_map>

namespace network { 
namespace session { 

struct SessionBase::Impl {
	using CallBackCon = std::unordered_map<SessionCallBackType, CallBackFunc>;
	CallBackCon callbackcon_;
};

SessionBase::SessionBase() {
	impl_ = std::make_unique<Impl>();
}

bool
SessionBase::RegistCallBackFunc(SessionCallBackType type, CallBackFunc f) {
	const auto itr = impl_->callbackcon_.insert(Impl::CallBackCon::value_type(type, f));
	return true;
}

const SessionBase::CallBackFunc
SessionBase::FindCallBackFunc(SessionCallBackType type) const {
	const auto itr = impl_->callbackcon_.find(type);
	if (impl_->callbackcon_.end() != itr){
		return itr->second;
	}
	return nullptr;
}

void
SessionBase::Accepted() {
	// 커스텀 callback이 있다면 호출해줍니다.
	auto& fun = FindCallBackFunc(SessionCallBackType::ACCEPTED);
	if (fun) { fun(this); }
}


}} /// network::session
