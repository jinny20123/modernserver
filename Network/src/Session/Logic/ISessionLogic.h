﻿#pragma once
#include <utility>

namespace utility { namespace object {
	struct Argument;
}}

namespace network { namespace session { namespace logic { 
/**
	@class : ISessionLogic
	@date : 2015/04/30
	@brief : 세션에서 전달 받은 데이터를 처리하는 방식을 구현합니다.(프로젝트별로 거의 다르기 때문에 따로 구현해야합니다. ) 
	@author : jinny20123(안 정웅)
*/
class ISessionLogic 
{
public: 
	using Argument = ::utility::object::Argument;
	/**
	@class : RecvResult
	@date : 2015/04/30
	@brief : Recv Logic을 수행하고 리턴하는 데이터 
			 first : 처리한 버퍼 데이터
			 second : 다시 Recv로직을 실행할지 여부 체크
	@author : jinny20123(안 정웅)
	*/
	using RecvResult = std::pair<size_t, bool>;
	virtual ~ISessionLogic(){};

	
	virtual void Capture(Argument && arg)				= 0; ///< 세션이 풀에서 할당 될 때 실행할 작업을 등록합니다. 
	virtual void Release()								= 0; ///< 세션이 풀에 반납 할 떄 실행할 작업을 등록합니다.

	virtual RecvResult Recv(Argument && arg)			= 0; ///< 세션에서 데이터를 전달 받았을 때 처리를 진행합니다. 
	virtual void Close(Argument && arg)					= 0; ///< 세션이 종료되었을 때의 처리를 진행합니다. 
};

}}} /// network::session::logic
