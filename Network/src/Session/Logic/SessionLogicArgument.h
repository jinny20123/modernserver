﻿#pragma once
#include "../Session_fwd.h"
#include "../../../../Utility/src/Object/argument.h"
#include <functional>

namespace network {
namespace session {
namespace logic {
	using Argument = ::utility::object::Argument;
	using ISession =::network::session::SessionBase;

	struct SessionLogicArgument
		: public Argument
	{
		SessionLogicArgument() = default;
		SessionLogicArgument(ISession* psession) :
			psession_(psession)
		{}

		ISession* psession_{ nullptr };
	};

	struct SessionLogicRecvArgument
		: public Argument
	{
		SessionLogicRecvArgument() = default;
		SessionLogicRecvArgument(char* buffer, size_t buffer_size, ISession* psession) :
			buffer_(buffer), buffer_size_(buffer_size), psession_(psession)
		{}

		char* buffer_{ nullptr };
		size_t		buffer_size_{ 0 };
		ISession* psession_{ nullptr };
	};

	struct SessionLogicCloseArgument
		: public SessionLogicArgument
	{
		SessionLogicCloseArgument() = default;
		SessionLogicCloseArgument(ISession* psession, ::network::session::CloseCallBack callback) :
			SessionLogicArgument(psession), callback_(callback)
		{}

		ISession* psession_{ nullptr };
		::network::session::CloseCallBack callback_;
	};

	struct SessionLogicSendArgument
		: public Argument
	{
		SessionLogicSendArgument() = default;
		SessionLogicSendArgument(char* buffer, size_t buffer_size) :
			buffer_(buffer), buffer_size_(buffer_size)
		{}

		char*		buffer_{ nullptr };
		size_t		buffer_size_{ 0 };
	};
}}} /// network::session::logic