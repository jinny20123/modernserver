﻿#ifndef NETWORK_SESSION_LOGIC_OBJECT_ECHOSESSIONLOGIC_H_
#define NETWORK_SESSION_LOGIC_OBJECT_ECHOSESSIONLOGIC_H_


#include "../ISessionLogic.h"
#include <memory>

namespace network { namespace session { namespace logic { namespace object { 


class EchoSessionLogic 
	: public ::network::session::logic::ISessionLogic
{
public: 
	EchoSessionLogic();
	virtual ~EchoSessionLogic();

	// 세션이 생성될 때 로직에서 실행할 작업을 등록합니다. 
	virtual void Capture(Argument && arg) override {}
	// 세션이 종료될 때 로직에서 실행할 작업을 등록합니다.
	virtual void Release() override {}

	virtual RecvResult Recv(Argument && arg) override;
	virtual void Close(Argument && arg) override;
	void RegistBuffer(char * buffer, size_t buffer_size);
private :
	struct Impl;
	std::unique_ptr<Impl> impl_;
};
	
}}}} /// network::session::logic::object
#endif /// NETWORK_SESSION_LOGIC_OBJECT_ECHOSESSIONLOGIC_H_
