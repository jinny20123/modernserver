﻿#include "stdafx.h"
#include "EchoSessionLogic.h"
#include "../../../../../Network/src/Session/Session.h"
#include "../../../../../Network/src/Session/Logic/SessionLogicArgument.h"
#include "../../../../../Network/src/Handler/IHandler.h"
#include "../../../../../Utility/src/Object/argument.h"
#include "../../../../../CommonPacket/src/Packet.h"
#include "../../../../../CommonPacket/src/PacketBuilder.h"
#include <cstdint>

namespace network {
	namespace session {
		namespace logic {
			namespace object {
				struct EchoSessionLogic::Impl
				{
					Impl() {}
					char* buffer;
					size_t buffer_size;
				};

				EchoSessionLogic::EchoSessionLogic()
				{
					impl_ = std::make_unique<Impl>();
				}
				EchoSessionLogic::~EchoSessionLogic()
				{
				}

				EchoSessionLogic::RecvResult
					EchoSessionLogic::Recv(Argument&& arg)
				{
					typedef ::packet::Packet
						Packet;
					typedef ::network::session::logic::SessionLogicRecvArgument
						SessionLogicRecvArgument;
					typedef ::network::session::logic::SessionLogicSendArgument
						SessionLogicSendArgument;

					unsigned int ret = 0;
					// 처리완료된 사이즈
					size_t complete_size = 0;

					SessionLogicRecvArgument* sa_arg = nullptr;
					::utility::object::ArgumentUtil::CastArgument(&arg, sa_arg);
					if (nullptr == sa_arg) { return RecvResult(complete_size, false); }
					// 현재 처리하고 있는 버퍼 사이즈
					size_t current_buffer_size = sa_arg->buffer_size_;

					if (impl_->buffer_size >= sa_arg->buffer_size_)
					{
						memcpy(impl_->buffer, sa_arg->buffer_, sa_arg->buffer_size_);
					}

					// 받은 데이터 그대로 리턴해줍니다.
					SessionLogicSendArgument sls_arg(sa_arg->buffer_, sa_arg->buffer_size_);
					sa_arg->psession_->Send(&sls_arg);

					complete_size = sa_arg->buffer_size_;

					return RecvResult(complete_size, false);
				}
				void
					EchoSessionLogic::Close(Argument&& arg)
				{
					return;
				}

				void
					EchoSessionLogic::RegistBuffer(char* buffer, size_t buffer_size)
				{
					impl_->buffer = buffer;
					impl_->buffer_size = buffer_size;
				}
			}
		}
	}
} /// network::session::logic::object