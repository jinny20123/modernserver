﻿#pragma once

#include <memory>

/// network::session
namespace network { namespace session { 

enum class SessionType : uint8_t
{
	ASIO,
	ENET,
	REDIS,
	ZOOKEEPER,
};


/// network::session::object
namespace object {
	constexpr size_t MAX_BUFFER_SIZE = 4096;
}		
/// network::session::object

/// network::session::logic
namespace logic {
	enum class ERROR_TYPE : uint8_t
	{
		ARGUMENT_NULL,
		ABNORMAL_CHECK,
		PACKET_MAKE_FAIL,
		ARGUMENT_MAKE_FAIL,
	};
}
/// network::session::logic


}}		/// network::session
