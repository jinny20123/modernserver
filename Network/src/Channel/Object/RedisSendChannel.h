﻿#pragma once

#include <boost/shared_ptr.hpp>
#include "../IChannel.h"

namespace network {
	namespace channel {
		namespace object {
			/**
				@class : RedisSendChannel
				@date : 2015/04/30
				@brief :
				@author :
			*/
			class RedisSendChannel
				: public ISendChannel
			{
			public:
				RedisSendChannel();
				virtual ~RedisSendChannel();
				virtual bool Capture(Argument* arg);
				virtual void Release();

				virtual void Connect(Argument* arg);
				virtual void Send(Argument* arg);
				virtual void Recv(Argument* arg);
			private:
				struct Impl;
				std::unique_ptr<Impl> impl_;
			};
		}
	}
} /// network::channel::object
