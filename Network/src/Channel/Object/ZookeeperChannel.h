﻿#pragma once
#include "../IChannel.h"

namespace network {
	namespace channel {
		namespace object {
			/**
				@class : ZookeeperChannel
				@date : 2015/07/16
				@brief : Zookeeper를 연동하는 채널 구현
				@author :
			*/
			class ZookeeperChannel
				: public IChannel
			{
			public:
				ZookeeperChannel();
				virtual ~ZookeeperChannel();
				virtual bool Capture(Argument* arg);
				virtual void Release();

				virtual void Send(Argument* arg);
				virtual void Recv(Argument* arg);
			private:
				void Regist(Argument* arg);
				struct Impl;
				std::unique_ptr<Impl> impl_;
			};
		}
	}
} /// network::channel::object
