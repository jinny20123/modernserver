﻿#include "stdafx.h"
#include <boost/unordered_map.hpp>
#include <functional>
#include "ZookeeperChannel.h"
#include "../ChannelArgument.h"
#include "../../Session/Object/ZookeeperSession.h"
#include "../../../../Log/src/Common/CommonLogger.h"

namespace network {
	namespace channel {
		namespace object {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct ZookeeperChannel::Impl
			{
				typedef boost::unordered_map<unsigned int, std::function<void(Argument*)> >
					ProcessImpContainer;
				Impl() {}

				ProcessImpContainer container_;
				typedef ::network::channel::ZookeeperChannelArgumentRegist
					ZookeeperChannelArgumentRegist;

				ZookeeperChannelArgumentRegist reservation_arg;
			};

			ZookeeperChannel::ZookeeperChannel()
			{
				impl_ = std::make_unique<Impl>();

				// command 별로 동작할 로직 등록
				impl_->container_.insert(Impl::ProcessImpContainer::value_type(ZookeeperChannelArgument::REGIST, [this](Argument* arg) { this->Regist(arg); }));
			}
			ZookeeperChannel::~ZookeeperChannel()
			{}

			bool
				ZookeeperChannel::Capture(Argument* arg)
			{
				return true;
			}

			void
				ZookeeperChannel::Release()
			{
			}

			void
				ZookeeperChannel::Send(Argument* arg)
			{
				ZookeeperChannelArgument* pzc_argument = nullptr;
				::utility::object::ArgumentUtil::CastArgument(arg, pzc_argument);

				auto iter = impl_->container_.find(pzc_argument->type_);

				if (impl_->container_.end() == iter)
				{
					// 로그 남기자!
					return;
				}

				iter->second(arg);
			}

			void
				ZookeeperChannel::Recv(Argument* arg)
			{
			}

			void
				ZookeeperChannel::Regist(Argument* arg)
			{
				ZookeeperChannelArgumentRegist* pzcr_argument = nullptr;
				::utility::object::ArgumentUtil::CastArgument(arg, pzcr_argument);

				session::object::ZookeeperSession* pzoo_session =
					static_cast<session::object::ZookeeperSession*>(pzcr_argument->psession_);

				zhandle_t* zh = static_cast<zhandle_t*>(pzoo_session->GetSocket());

				if (nullptr == zh)
				{
					return;
				}

				const int buffer_size = 256;
				char buffer[buffer_size] = { 0, };

				int rc = 0;
				int watch_val = 0;
				struct Stat stat;

				rc = zoo_exists(zh, pzcr_argument->nodename_.c_str(), watch_val, &stat);

				// NODE가 존재하지 않는다.
				if (ZNONODE == rc)
				{
					struct ACL CREATE_ONLY_ACL_ACL[] = { {ZOO_PERM_ALL, ZOO_ANYONE_ID_UNSAFE} };
					struct ACL_vector CREATE_ONLY_ACL = { 1, CREATE_ONLY_ACL_ACL };
					int result = zoo_create(zh, pzcr_argument->nodename_.c_str(), "", static_cast<int>(strlen("")), &CREATE_ONLY_ACL, 0, buffer, buffer_size - 1);
				}
				else if (ZOK == rc)
				{
					// 존재한다.
				}

				// zookeeper 등록할 정보를 생성합니다.
				std::string regist_info(pzcr_argument->nodename_ + "/");
				regist_info.append(pzcr_argument->regist_info_.c_str());

				struct ACL CREATE_ONLY_ACL_ACL[] = { {ZOO_PERM_ALL, ZOO_ANYONE_ID_UNSAFE} };
				struct ACL_vector CREATE_ONLY_ACL = { 1, CREATE_ONLY_ACL_ACL };
				// ZOO_EPHEMERAL : 세션이 접속된 상태에서 데이터를 유지하라는 명령어
				rc = zoo_create(zh, regist_info.c_str(), "", static_cast<int>(strlen("")), &CREATE_ONLY_ACL, ZOO_EPHEMERAL, buffer, buffer_size - 1);

				// 전 세션에 대한 등록이 존재한다.
				COMMONLOG().ToInfo(NS_COMMONLOG::NETWORK, "rc(%d) ZOO_EPHEMERAL Regist info(%s)", rc, regist_info.c_str());

				if (ZNODEEXISTS == rc)
				{
					impl_->reservation_arg.psession_ = pzoo_session;
					impl_->reservation_arg.nodename_ = pzoo_session->GetPath();
					impl_->reservation_arg.regist_info_ = pzoo_session->GetValue();
				}
			}
		}
	}
} /// network::channel::object