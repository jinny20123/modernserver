﻿#include "stdafx.h"
#include <boost/assert.hpp>
#include "RedisSendChannel.h"
#include "../../Session/Object/RedisSession.h"
#include "../../Session/SessionArgument.h"
#include "../../../../Utility/src/String/string_util.h"
#include "../../../../Utility/src/RedisClient/redisclient.h"

namespace network {
	namespace channel {
		namespace object {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct RedisSendChannel::Impl
			{
				Impl() {}
			};

			RedisSendChannel::RedisSendChannel()
			{
				impl_ = std::make_unique<Impl>();
			}
			RedisSendChannel::~RedisSendChannel()
			{}

			bool
				RedisSendChannel::Capture(Argument* arg)
			{
				return true;
			}

			void
				RedisSendChannel::Release()
			{
			}

			void
				RedisSendChannel::Connect(Argument* arg) {}

			void
				RedisSendChannel::Send(Argument* arg)
			{
				typedef ::utility::string::StringUtil
					StringUtil;
				typedef ::network::session::object::RedisSession::Socket
					Socket;

				session::RedisSessionPacketArgument* prss_argument = nullptr;
				utility::object::ArgumentUtil::CastArgument(arg, prss_argument);

				StringUtil::TokenContainer tokens =
					StringUtil::SplitString(prss_argument->command_, " ");

				session::object::RedisSession* predis_session =
					static_cast<session::object::RedisSession*>(prss_argument->psession_);
				Socket* psocket = predis_session->GetSocket();

				if (nullptr == psocket)
				{
					return;
				}

				if (tokens.empty() ||
					tokens.size() > 7)
				{
					BOOST_ASSERT(false);
					return;
				}

				switch (tokens.size())
				{
				case 1:
					psocket->command(tokens[0], prss_argument->callback_);
					break;
				case 2:
					psocket->command(tokens[0], tokens[1], prss_argument->callback_);
					break;
				case 3:
					psocket->command(tokens[0], tokens[1], tokens[2], prss_argument->callback_);
					break;
				case 4:
					psocket->command(tokens[0], tokens[1], tokens[2], tokens[3], prss_argument->callback_);
					break;
				case 5:
					psocket->command(tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], prss_argument->callback_);
					break;
				case 6:
					psocket->command(tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], tokens[5], prss_argument->callback_);
					break;
				case 7:
					psocket->command(tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], tokens[5], tokens[6], prss_argument->callback_);
					break;
				}
			}

			void
				RedisSendChannel::Recv(Argument* arg)
			{
			}
		}
	}
} /// network::channel::object