﻿#pragma once
namespace utility { namespace object{ 
	struct Argument;
}}

namespace network { namespace channel { 

/** 
	@class : ISession
	@date : 2015/05/11
	@brief : 세션을 통한 네트워크 작업 처리를 담당하는 객체의 인터페이스를 제공합니다. 
	@author : jinny20123(안 정웅) 
*/
class IChannel 
{
public: 
	typedef ::utility::object::Argument
			Argument;

	virtual ~IChannel(){};

	/// IChannel 인터페이스를 정의합니다. 
	virtual bool Capture(Argument * arg)			= 0;
	virtual void Release()							= 0;
	virtual void Send(Argument * arg)				= 0;
	virtual void Recv(Argument * arg)				= 0;
};

class ISendChannel
	: public IChannel
{
public :
	virtual void Connect(Argument * arg)			= 0;
};

}} /// network::channel
