﻿#pragma once

#include "../../../../Utility/src/Factory/Container/factory_container.h"
#include <boost/shared_ptr.hpp>

namespace utility { namespace object {
	struct Argument;
}}

namespace network { namespace channel {
	class IChannel;
}}

namespace network { namespace channel { namespace factory { 
/** 
	@class : ChannelFactory
	@date : 2015/05/11
	@brief : 채널 오브젝트를 관리하는 팩토리 객체입니다.
	@author : jinny20123(안 정웅)
*/


template<typename Type>
class ChannelFactory 
{
public:
	using Argument = ::utility::object::Argument; 
	using Interface = IChannel;

	ChannelFactory();
	virtual ~ChannelFactory();

	static Interface * Create(Argument * arg = nullptr)
	{
		auto pobj = factory_.Create();
		if (false == pobj->Capture(arg) ) 
		{ return nullptr; }

		return pobj;
	}

	static void Destroy(Interface * pobj)
	{
		pobj->Release();
		factory_.Destroy(pobj);
	}

private :
	static ::utility::factory::Factory<Interface, Type > factory_;
};

STATIC_FACTORY(ChannelFactory, factory_);


	
}}} /// network::channel::factory