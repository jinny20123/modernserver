﻿#pragma once
#include <functional>
#include "../../../Utility/src/Object/argument.h"

namespace network { namespace session {
	class SessionBase;
}}

namespace network { namespace channel { 

typedef ::utility::object::Argument
		Argument;
typedef ::network::session::SessionBase
		ISession;


struct RecvChannelCreateArgument 
	: public Argument
{
	typedef std::function<void(Argument *)>
			RecvParserFunction;

	RecvChannelCreateArgument() {}
	RecvChannelCreateArgument(RecvParserFunction func) : recv_parser_fun_(func)
	{}

	RecvParserFunction		recv_parser_fun_;
};

struct EnetRecvChannelCreateArgument 
	: public Argument
{
	typedef std::function<void(Argument *)>
			RecvParserFunction;

	EnetRecvChannelCreateArgument()
	{
	}
	EnetRecvChannelCreateArgument(int32_t timeout_ms, int64_t max_queue_count, int32_t enet_wait_time, RecvParserFunction func) 
		: timeout_ms_(timeout_ms), max_queue_count_(max_queue_count), enet_wait_time_(enet_wait_time), recv_parser_fun_(func)
	{
	}

	
	int32_t					timeout_ms_;
	int64_t					max_queue_count_;
	int32_t					enet_wait_time_;
	RecvParserFunction		recv_parser_fun_;
};

struct ChannelRecvArgument
	: public Argument
{
	ChannelRecvArgument() {}
	ChannelRecvArgument(ISession * psession) : psession_(psession)
	{}

	virtual bool IsEnable()
	{
		if(nullptr == psession_)
		{ return false; }
		return true;
	}
	ISession *				psession_;
};

struct ZookeeperChannelArgument
	: public ChannelRecvArgument
{
	ZookeeperChannelArgument(){}
	ZookeeperChannelArgument(ISession * psession) : ChannelRecvArgument(psession)
	{}

	enum
	{
		REGIST = 0,
	};

	unsigned int type_;
};

struct ZookeeperChannelArgumentRegist
	:  public ZookeeperChannelArgument
{
	ZookeeperChannelArgumentRegist() 
	{
		type_ = REGIST;
	}
	ZookeeperChannelArgumentRegist(ISession * psession, const char * nodename, const char * regist_info) 
		: ZookeeperChannelArgument(psession), nodename_(nodename), regist_info_(regist_info)
	{ 
		type_ = REGIST;
	}

	std::string nodename_;
	std::string regist_info_;
};

}} /// network::channel

