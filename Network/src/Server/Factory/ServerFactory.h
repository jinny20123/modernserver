﻿#pragma once

#include "../../../../Utility/src/Factory/Container/factory_container.h"
#include <memory>
#include <boost/serialization/singleton.hpp>

namespace utility {
	namespace object {
		struct Argument;
	}
}

namespace network {
	namespace server {
		class IServer;
	}
}

namespace network {
	namespace server {
		namespace factory {
			/**
				@class : ServerFactory
				@date : 2016-02-23
				@brief : 서버 객체의 팩토리 객체 입니다.
				@author : jinny20123(안 정웅)
			*/
			template<typename Type>
			class ServerFactory
				: private boost::serialization::singleton< ServerFactory<Type> >
			{
			public:
				using Argument = ::utility::object::Argument;
				using Interface = ::network::server::IServer;

				ServerFactory() {};
				virtual ~ServerFactory() {};

				Interface* Create(Argument* arg = nullptr)
				{
					auto pobj = factory_.Create();
					pobj->Capture(arg);
					return pobj;
				}

				void Destroy(Interface* pobj)
				{
					pobj->Release();
					factory_.Destroy(pobj);
				}

				static ServerFactory<Type>& GetInstance()
				{
					return get_mutable_instance();
				}
			private:
				::utility::factory::Factory<Interface, Type > factory_;
			};
		}
	}
} /// network::server::factory