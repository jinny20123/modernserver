﻿#pragma once
#include <memory>
namespace utility {
	namespace object {
		struct Argument;
	}
}

namespace network {
	namespace server {
		/**
			@class : IServer
			@date : 2065/02/23
			@brief : 포트를 바인딩하는 통신객체를 생성합니다.
			@author : jinny20123(안 정웅)
		*/
		class IServer
		{
		public:
			using Argument = ::utility::object::Argument;

			virtual ~IServer() {};

			/// IServer 인터페이스를 정의합니다.
			virtual bool Capture(Argument* arg) = 0;
			virtual void Release() = 0;
			virtual void Accept() = 0;
		};
	}
} /// network::server
