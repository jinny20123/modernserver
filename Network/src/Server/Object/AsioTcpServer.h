﻿#pragma once
#include "../IServer.h"
#include <memory>
namespace network {
	namespace server {
		namespace object {
			class AsioTcpServer
				: public IServer
			{
			public:
				AsioTcpServer();
				virtual ~AsioTcpServer();

				virtual bool Capture(Argument* arg);
				virtual void Release() override;
				virtual void Accept() override;
			private:
				struct Impl;
				std::unique_ptr<Impl> mImpl;
			};
		}
	}
} /// network::server::object