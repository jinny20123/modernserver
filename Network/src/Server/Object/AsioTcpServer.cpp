﻿#include "stdafx.h"
#include "AsioTcpServer.h"
#include "../ServerArgument.h"
#include "../../Channel/ChannelArgument.h"
#include "../../Channel/Factory/ChannelFactory.h"
#include "../../Session/SessionArgument.h"
#include "../../Session/Factory/SessionFactory.h"
#include "../../Session/Object/AsioTcpSession.h"
#include "../../Session/Logic/ISessionLogic.h"
#include "../../../../Utility/src/Object/argument.h"
#include "../../../../Log/src/Common/CommonLogger.h"

namespace network {
	namespace server {
		namespace object {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct AsioTcpServer::Impl
			{
				using Acceptor = boost::asio::ip::tcp::acceptor;
				using Socket = boost::asio::ip::tcp::socket;
				using IChannel = ::network::channel::IChannel;

				using ISessionLogic = ::network::session::logic::ISessionLogic;

				Socket*							socket_;
				std::shared_ptr<Acceptor>		acceptor_;			
				ISessionLogic*					sessionlogic_;
				boost::asio::io_context *		io_context_;
			};

			AsioTcpServer::AsioTcpServer()
			{
				MAKE_UNIQUE(mImpl, Impl);
			}
			AsioTcpServer::~AsioTcpServer()
			{}

			bool
				AsioTcpServer::Capture(Argument* arg)
			{
				AsioServerCaptureArgument* pasio_argument = nullptr;
				::utility::object::ArgumentUtil::CastArgument(arg, pasio_argument);

				// 소켓 정보 및 Acceptor 정보 셋팅
				mImpl->acceptor_ = std::make_shared<Impl::Acceptor>(*pasio_argument->pio_context_,
					boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), pasio_argument->bind_port_));
				mImpl->sessionlogic_ = pasio_argument->psession_logic_;
				mImpl->io_context_ = pasio_argument->pio_context_;
				mImpl->socket_ = new boost::asio::ip::tcp::socket(mImpl->acceptor_->get_executor());

				COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "[AsioTcpServer] Bind Port %d", pasio_argument->bind_port_);
				COMMONLOG().ToDebug(NS_COMMONLOG::CONSOLE, "[AsioTcpServer] Bind Port %d", pasio_argument->bind_port_);

				return true;
			}
			void
			AsioTcpServer::Release()
			{
				boost::system::error_code ec;
				mImpl->acceptor_->cancel();
				mImpl->acceptor_->close(ec);
				if (ec)
				{
				}
			}

			void
			AsioTcpServer::Accept()
			{
				using AsioTcpSessionCreateArgument = ::network::session::AsioTcpSessionCreateArgument;

				mImpl->acceptor_->async_accept(*mImpl->socket_, [this](boost::system::error_code ec)
					{
						using AsioTcpSession = ::network::session::object::AsioTcpSession;
						using AsioSocketFactory = ::network::session::object::AsioSocketFactory<boost::asio::ip::tcp::socket>;

						if (!ec)	// 에러가 발생하지 않았다면
						{
							auto session = ::network::session::factory::SESSION_FACTORY<AsioTcpSession>().Create(&AsioTcpSessionCreateArgument(mImpl->socket_, mImpl->sessionlogic_));
							session->Accepted();
							
							session->Recv();
							auto new_socket = AsioSocketFactory::GetInstance().Create();
							// AsioSocketFactory는 NONE CREATE POLICY를 사용하기 때문에 아래의 작업을 해주어야 한다.
							if (nullptr == new_socket)
							{
								new_socket = new boost::asio::ip::tcp::socket(mImpl->acceptor_->get_executor());
							}
							mImpl->socket_ = new_socket;
						}
						else
						{
							if (ec.value() == 10038)
							{
								return;
							}
						}

						Accept();
					});
			}
		}
	}
} /// network::server::object