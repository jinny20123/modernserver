﻿#pragma once
#include "../../../Utility/src/Object/argument.h"
#include <functional>
#include <memory>
#include <boost/asio/io_service.hpp>

namespace network {
	namespace session {
		namespace logic {
			class ISessionLogic;
		}
	}
}

namespace network {
	namespace server {
		using Argument = ::utility::object::Argument;
		using ISessionLogic = ::network::session::logic::ISessionLogic;

		struct ServerCaptureArgument
			: public Argument
		{
			ServerCaptureArgument(unsigned short port) :
				bind_port_(port)
			{
			}

			unsigned short bind_port_;
		};

		struct AsioServerCaptureArgument
			: public ServerCaptureArgument
		{
			AsioServerCaptureArgument(unsigned short port, boost::asio::io_context* pio_context, ISessionLogic* psession_logic) :
				ServerCaptureArgument(port), pio_context_(pio_context), psession_logic_(psession_logic)
			{
			}

			boost::asio::io_context* pio_context_;
			ISessionLogic* psession_logic_;
		};
	}
} /// network::server
