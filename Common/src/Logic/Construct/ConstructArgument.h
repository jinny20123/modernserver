﻿#pragma once
#include "../../../../Utility/src/Object/argument.h"

// 전방선언 클래스 Start
namespace network {
	namespace session {
		namespace manager {
			class SendSessionManager;
		}
	}
}

namespace network {
	namespace session {
		namespace logic {
			class ISessionLogic;
		}
	}
}
// 전방선언 클래스 End

namespace common {
	namespace logic {
		namespace construct {
			using Argument = ::utility::object::Argument;

			struct AsioConstructArgument
				: public Argument
			{
				using ISessionLogic = ::network::session::logic::ISessionLogic;

				enum class PROTOCOLTYPE
				{
					TCP,
					UDP,
				};

				AsioConstructArgument(int bind_count, unsigned short port, ISessionLogic* session_logic, PROTOCOLTYPE protocol_type) :
					bind_count(bind_count), port(port), sessionlogic(session_logic), protocol_type(protocol_type)
				{}

				int						bind_count;
				unsigned short			port;
				ISessionLogic* sessionlogic;
				PROTOCOLTYPE			protocol_type;
			};

			struct ZookeeperConstructArgument
				: public Argument
			{
				using SendSessionManager = ::network::session::manager::SendSessionManager;

				ZookeeperConstructArgument(const char* zookeeper_address, unsigned short zookeeper_port, int32_t zookeeper_timeout, const char* nodename, const char* server_address, unsigned short server_port, SendSessionManager* psendmanager)
					: zookeeper_server_address(zookeeper_address), zookeeper_server_port(zookeeper_port), zookeeper_timeout(zookeeper_timeout),
					nodename(nodename), reg_server_address(server_address), reg_server_port(server_port), psendSessionmanager(psendmanager)
				{}

				const char* zookeeper_server_address;
				unsigned short			zookeeper_server_port;
				int32_t					zookeeper_timeout;
				const char* nodename;
				const char* reg_server_address;
				unsigned short			reg_server_port;
				SendSessionManager* psendSessionmanager;
			};

			struct RedisConstructArgument
				: public Argument
			{
				using SendSessionManager = ::network::session::manager::SendSessionManager;

				RedisConstructArgument(const char* redis_address, unsigned short redis_port, unsigned short process_count, SendSessionManager* psendmanager)
					: redis_server_address(redis_address), redis_server_port(redis_port), session_count(process_count), psendSessionmanager(psendmanager)
				{}

				const char* redis_server_address;
				unsigned short			redis_server_port;
				unsigned short			session_count;
				SendSessionManager* psendSessionmanager;
			};
		}
	}
} /// common::logic::construct
