﻿#pragma once
#include <cstdint>
#include "../../../../Utility/src/Object/argument.h"
#include "../../../../Utility/src/macro.h"

namespace common {
	namespace logic {
		namespace configure {
			namespace object {
				class XmlConfigure;
			}
		}
	}
}

namespace common {
	namespace logic {
		namespace construct {
			/**
				@class : Construct
				@date : 2015/05/08
				@brief : 프로그램을 구동하기 위해서 필요한 작업을 등록하는 클래스입니다.
						 미리 준비할 작업을 SetUp() 함수에 등록합니다.
						 프로그램을 구동할떄 필요한 작업을 Run() 함수에 등록합니다.
				@author : jinny20123(안 정웅)
			*/
			class IConstruct
			{
			public:
				using Argument = ::utility::object::Argument;
				using XmlConfigure = ::common::logic::configure::object::XmlConfigure;

				IConstruct() {};
				virtual ~IConstruct() {};

				virtual void SetUp() = 0;
				virtual void Run() = 0;
			};
		}
	}
} /// common::logic::construct
