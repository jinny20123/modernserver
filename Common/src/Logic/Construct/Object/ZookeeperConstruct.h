﻿#pragma once
#include "../IConstruct.h"

namespace network {
	namespace session {
		class SessionBase;
	}
}

namespace common {
	namespace logic {
		namespace construct {
			namespace object {
				/**
					@class : ZookeeperConstruct
					@date : 2015/04/30
					@brief :
					@author :
				*/
				class ZookeeperConstruct
					: public IConstruct
				{
				public:
					using ISession = ::network::session::SessionBase;
					struct Impl;

					ZookeeperConstruct(Argument&& argument);
					virtual ~ZookeeperConstruct();
					virtual void SetUp();
					virtual void Run();
					static void SessionConnect(ISession* psession, Impl* This);
				private:
					void ConstructNetwork();
					std::unique_ptr<Impl> impl_;
				};
			}
		}
	}
} /// common::logic::construct::object