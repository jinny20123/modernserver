﻿#include "AsioConstruct.h"
#include "../ConstructArgument.h"
#include "../../../Logic/Configure/Object/XmlConfigure.h"
#include "../../../../../Network/src/Server/Object/AsioTcpServer.h"
#include "../../../../../Network/src/Server/Factory/ServerFactory.h"
#include "../../../../../Network/src/Server/ServerArgument.h"
#include "../../../../../Network/src/Session/Logic/ISessionLogic.h"
#include <boost/asio/io_service.hpp>
#include <memory>
#include <thread>

namespace common {
	namespace logic {
		namespace construct {
			namespace object {
				/// 내부에서 사용할 변수를 정의합니다.
				/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
				struct AsioConstruct::Impl
				{
					using IServer = ::network::server::IServer;
					using ServerContainer = std::vector<IServer*>;
					typedef std::function<size_t(Argument*)>
						RecvParserFunction;
					typedef ::network::session::logic::ISessionLogic
						ISessionLogic;

					boost::asio::io_context								io_context_;
					ServerContainer										server_container_;
					int													bind_count;
					unsigned short										port;
					ISessionLogic* psessionlogic_;

					AsioConstructArgument::PROTOCOLTYPE					protocol_type;
					std::unique_ptr<boost::asio::io_context::work>		pwork_;
					std::thread											io_run_thread;
				};

				AsioConstruct::AsioConstruct(Argument&& argument)
				{
					impl_ = std::make_unique<Impl>();

					AsioConstructArgument* pasio_argument = nullptr;
					::utility::object::ArgumentUtil::CastArgument(&argument, pasio_argument);
					impl_->bind_count = pasio_argument->bind_count;
					impl_->port = pasio_argument->port;
					impl_->protocol_type = pasio_argument->protocol_type;
					impl_->psessionlogic_ = pasio_argument->sessionlogic;
				}
				AsioConstruct::~AsioConstruct()
				{
					typedef ::network::server::object::AsioTcpServer
						AsioTcpServer;
					typedef ::network::server::factory::ServerFactory<AsioTcpServer>
						AsioTcpServerFactory;
					/// pool에 객체를 반납합니다.
					for (auto& var : impl_->server_container_)
					{
						AsioTcpServerFactory::GetInstance().Destroy(var);
					}
				}

				void
					AsioConstruct::ConstructNetwork()
				{
					// 프로토콜별로 세션을 다르게 생성합니다.
					switch (impl_->protocol_type)
					{
					case AsioConstructArgument::PROTOCOLTYPE::TCP:
					{
						tcpConstruct();
					}
					break;
					case AsioConstructArgument::PROTOCOLTYPE::UDP:
					{
					}
					break;
					}

					//LOG().ToInfo(NS_LOG::KeyType::CONSOLE, "Asio Server Run Port[%d]", port );
				}

				void
				AsioConstruct::SetUp()
				{
					ConstructNetwork();
				}

				void
				AsioConstruct::Run()
				{
					// io_service를 work에 등록시켜서 비활성화 상태 일때도 동작하도록 합니다.
					MAKE_UNIQUE(impl_->pwork_, boost::asio::io_service::work, impl_->io_context_);
					impl_->io_run_thread = std::thread([this]()
						{
							impl_->pwork_->get_io_context().run();
						});
				}

				void
				AsioConstruct::tcpConstruct()
				{
					using AsioTcpServerFactory = ::network::server::factory::ServerFactory<::network::server::object::AsioTcpServer>;
					using AsioServerCaptureArgument = ::network::server::AsioServerCaptureArgument;

					auto pserver = AsioTcpServerFactory::GetInstance().Create(&AsioServerCaptureArgument(impl_->port, &impl_->io_context_, impl_->psessionlogic_));
					if (pserver)
					{
						pserver->Accept();
						impl_->server_container_.push_back(pserver);
					}
				}
			}
		}
	}
} /// common::logic::construct::object