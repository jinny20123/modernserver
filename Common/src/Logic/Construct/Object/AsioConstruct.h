﻿#pragma once
#include "../IConstruct.h"
#include <memory>

namespace common {
	namespace logic {
		namespace construct {
			namespace object {
				/**
					@class : AsioConstruct
					@date : 2015/04/30
					@brief : Asio 라이브러리를 사용하기 위한 준비 작업을 진행합니다.
					@author :
				*/
				class AsioConstruct
					: public IConstruct
				{
				public:
					AsioConstruct(Argument&& argument);
					virtual ~AsioConstruct();
					void ConstructNetwork();
					virtual void SetUp();
					virtual void Run();
				private:
					void tcpConstruct();

					struct Impl;
					std::unique_ptr<Impl> impl_;
				};
			}
		}
	}
} /// common::logic::construct::object