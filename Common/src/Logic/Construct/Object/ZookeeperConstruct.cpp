﻿#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include "ZookeeperConstruct.h"
#include "../ConstructArgument.h"
#include "../../../Logic/Configure/Object/XmlConfigure.h"
#include "../../../../../Network/src/Session/Object/ZookeeperSession.h"
#include "../../../../../Network/src/Session/Factory/SessionFactory.h"
#include "../../../../../Network/src/Session/SessionArgument.h"
#include "../../../../../Network/src/Session/Manager/SendSessionManager.h"
#include "../../../../../Network/src/Channel/Object/ZookeeperChannel.h"
#include "../../../../../Network/src/Channel/Factory/ChannelFactory.h"
#include "../../../../../Network/src/Util/Watcher/ZookeeperWatcher.h"

namespace common {
	namespace logic {
		namespace construct {
			namespace object {
				/// 내부에서 사용할 변수를 정의합니다.
				/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
				struct ZookeeperConstruct::Impl
				{
					using ZookeeperSession = ::network::session::object::ZookeeperSession;
					using ZookeeperSessionFactory = ::network::session::factory::SessionFactory<ZookeeperSession>;
					using ZookeeperChannel = ::network::channel::object::ZookeeperChannel;
					using ZookeeperChannelFactory = ::network::channel::factory::ChannelFactory<ZookeeperChannel>;
					using SendSessionManager = ::network::session::manager::SendSessionManager;

					Impl() {}
					ZookeeperSession* pzookeeper_session;
					ZookeeperChannel* pzookeeper_channel_;

					std::string				zookeeper_address;
					unsigned short			zookeeper_port;
					int32_t					zookeeper_timeout_ms;
					std::string				nodename;
					std::string				server_ipaddress;
					unsigned short			server_port;
					SendSessionManager* psend_manager;
				};

				ZookeeperConstruct::ZookeeperConstruct(Argument&& argument)
				{
					impl_ = std::make_unique<Impl>();
					impl_->pzookeeper_session = nullptr;

					ZookeeperConstructArgument* pzookeeper_argument = nullptr;
					utility::object::ArgumentUtil::CastArgument(&argument, pzookeeper_argument);

					impl_->zookeeper_address = pzookeeper_argument->zookeeper_server_address;
					impl_->zookeeper_port = pzookeeper_argument->zookeeper_server_port;
					impl_->zookeeper_timeout_ms = pzookeeper_argument->zookeeper_timeout;
					impl_->nodename = pzookeeper_argument->nodename;
					impl_->server_ipaddress = pzookeeper_argument->reg_server_address;
					impl_->server_port = pzookeeper_argument->reg_server_port;
					impl_->psend_manager = pzookeeper_argument->psendSessionmanager;
				}
				ZookeeperConstruct::~ZookeeperConstruct()
				{
					Impl::ZookeeperChannelFactory::Destroy(impl_->pzookeeper_channel_);
					Impl::ZookeeperSessionFactory::GetInstance().Destroy(impl_->pzookeeper_session);
				}

				void
					ZookeeperConstruct::SetUp()
				{
					ConstructNetwork();
				}
				void
					ZookeeperConstruct::Run()
				{
				}

				void
					ZookeeperConstruct::ConstructNetwork()
				{
					typedef ::network::session::SessionCreateArgument
						SessionCreateArgument;
					typedef ::network::session::SessionType
						SessionType;

					impl_->pzookeeper_channel_ = static_cast<Impl::ZookeeperChannel*>(Impl::ZookeeperChannelFactory::Create());

					impl_->pzookeeper_session =
						static_cast<Impl::ZookeeperSession*>(Impl::ZookeeperSessionFactory::GetInstance().Create(&SessionCreateArgument(impl_->pzookeeper_channel_)));

					ZookeeperConstruct::SessionConnect(impl_->pzookeeper_session, impl_.get());

					impl_->psend_manager->Release(SessionType::ZOOKEEPER, impl_->pzookeeper_session);
				}

				void
					ZookeeperConstruct::SessionConnect(ISession* psession, Impl* impl_)
				{
					typedef ::network::session::object::ZookeeperSession
						ZookeeperSession;
					typedef ::network::session::ZookeeperSessionConnectArgument
						ZookeeperSessionConnectArgument;

					ZookeeperSession* pzookeeper_session = static_cast<ZookeeperSession*>(psession);

					std::string value = impl_->server_ipaddress;
					value.append(":");
					value.append(std::to_string(static_cast<long long>(impl_->server_port)));

					pzookeeper_session->Connect(&ZookeeperSessionConnectArgument(impl_->zookeeper_address, impl_->zookeeper_port, impl_->zookeeper_timeout_ms, impl_->nodename, value, &network::util::watcher::watcher));
				}
			}
		}
	}
} /// common::logic::construct::object