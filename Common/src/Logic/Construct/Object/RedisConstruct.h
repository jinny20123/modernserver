﻿#pragma once
#include "../IConstruct.h"
#include <memory>

namespace common {
	namespace logic {
		namespace construct {
			namespace object {
				/**
					@class : RedisConstruct
					@date : 2015/04/30
					@brief :
					@author :
				*/

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif

				class RedisConstruct
					: public IConstruct
				{
				public:
					struct Impl;

					RedisConstruct(Argument&& argument);
					virtual ~RedisConstruct();

					virtual void SetUp();
					virtual void Run();
				private:
					void ConstructReservation();
					void ConstructRedisNetwork();
					void ReservationUserCount();
					std::unique_ptr<Impl> impl_;
				};
			}
		}
	}
} /// common::logic::construct::object