﻿#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include "RedisConstruct.h"
#include "../ConstructArgument.h"
#include "../../../Logic/Configure/Object/XmlConfigure.h"
#include "../../../../../Network/src/Session/Object/RedisSession.h"
#include "../../../../../Network/src/Session/Factory/SessionFactory.h"
#include "../../../../../Network/src/Session/Manager/SendSessionManager.h"
#include "../../../../../Network/src/Session/SessionArgument.h"
#include "../../../../../Network/src/Channel/Object/RedisSendChannel.h"
#include "../../../../../Network/src/Channel/Factory/ChannelFactory.h"
#include "../../../../../Network/src/Channel/ChannelArgument.h"

namespace common {
	namespace logic {
		namespace construct {
			namespace object {
				/// 내부에서 사용할 변수를 정의합니다.
				/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
				struct RedisConstruct::Impl
				{
					typedef network::session::SessionBase
						ISession;
					typedef ::network::session::SessionType
						SessionType;
					typedef ::network::session::object::RedisSession
						RedisSession;
					typedef ::network::session::manager::SendSessionManager
						SendSessionManager;

					boost::asio::io_service				io_service_;

					std::string							redis_address;
					unsigned short						redis_port;
					unsigned short						session_count;
					SendSessionManager* psend_manager;
				};

				RedisConstruct::RedisConstruct(Argument&& argument)
				{
					impl_ = std::unique_ptr<Impl>();

					RedisConstructArgument* predis_argument = nullptr;
					::utility::object::ArgumentUtil::CastArgument(&argument, predis_argument);
					impl_->redis_address = predis_argument->redis_server_address;
					impl_->redis_port = predis_argument->redis_server_port;
					impl_->session_count = predis_argument->session_count;
					impl_->psend_manager = predis_argument->psendSessionmanager;
				}
				RedisConstruct::~RedisConstruct()
				{}

				void
					RedisConstruct::SetUp()
				{
					ConstructRedisNetwork();
					ConstructReservation();
				}

				void
					RedisConstruct::Run()
				{
					boost::thread t(boost::bind(&boost::asio::io_service::run, &impl_->io_service_));
				}

				void
					RedisConstruct::ConstructReservation()
				{
				}

				void
					RedisConstruct::ConstructRedisNetwork()
				{
					namespace session = ::network::session;
					namespace channel = ::network::channel;

					typedef session::factory::SessionFactory<Impl::RedisSession>
						RedisSessionFactory;
					typedef channel::object::RedisSendChannel
						RedisSendChannel;
					typedef channel::factory::ChannelFactory<RedisSendChannel>
						ChannelFactory;

					auto predis_channel = ChannelFactory::Create(nullptr);

					for (int i = 0; i < impl_->session_count; ++i)
					{
						Impl::ISession* redis_session = RedisSessionFactory::GetInstance().Create(&session::RedisSessionCreateArgument(&impl_->io_service_, predis_channel));
						redis_session->Connect(&session::SessionConnectArgument(impl_->redis_address, impl_->redis_port));
						impl_->psend_manager->Release(Impl::SessionType::REDIS, redis_session);
					}
				}
			}
		}
	}
} /// common::logic::construct::object