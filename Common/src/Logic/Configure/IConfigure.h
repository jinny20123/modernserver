﻿#pragma once
#if _HAS_CXX17
#include <string_view>
#endif
#include <string>
#include <boost/lexical_cast.hpp>

namespace utility {
	namespace object {
		struct Argument;
	}
}

namespace common {
	namespace logic {
		namespace configure {
			/**
				@class : Construct
				@date : 2015/05/08
				@brief : Configure 파일을 읽어 들이는 작업을 정의하는 클래스입니다.
				@author : jinny20123(안 정웅)
			*/
			class IConfigure
			{
			public:
				using Argument = ::utility::object::Argument;
#if _HAS_CXX17
				using KeyValueType = std::string_view;
				using ReturnValueType = std::string_view;
#else
				using KeyValueType = const char*;
				using ReturnValueType = std::string;
#endif

				virtual ~IConfigure() {};

				/// IConfigure 인터페이스를 정의합니다.

				virtual ReturnValueType Get(KeyValueType key, KeyValueType defualt) = 0;
				template<typename T>
				void TGet(KeyValueType key, T& value)
				{
					try {
						std::string result = Get(key, "");
						if (result != "")
						{
							value = boost::lexical_cast<T>(result);
						}
					}
					catch (...) {
					}
				}
			};
		}
	}
} /// common::logic::configure
