﻿#pragma once
#include <zookeeper.h>

namespace relayserver { namespace network { namespace watcher { 

void watcher(zhandle_t * zh, int type, int state, const char * path, void * watcherCtx);
	
}}} /// relayserver::network::watcher