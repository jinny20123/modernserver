﻿#include <boost/filesystem.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <iostream>
#include "InIConfigure.h"
#include "../../../../../Utility/src/String/string_util.h"

namespace common {
	namespace logic {
		namespace configure {
			namespace object {
				/// 내부에서 사용할 변수를 정의합니다.
				/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
				struct InIConfigure::Impl
				{
					boost::property_tree::ptree ptree_;
					std::string file_path_;
				};
				InIConfigure::InIConfigure(const char* ini_path)
				{
					try {
						impl_ = std::make_unique<Impl>();

						std::string config_path;
						config_path.append(ini_path);
						boost::property_tree::ini_parser::read_ini(config_path, impl_->ptree_);
						impl_->file_path_ = config_path;
					}
					catch (boost::exception& ) {
						std::cerr << "[Error] InIConfigure::InIConfigure ini_path = " << ini_path << std::endl;
						std::this_thread::sleep_for(std::chrono::seconds(3));
						throw;
					}
				}
				InIConfigure::~InIConfigure()
				{}

				IConfigure::ReturnValueType
				InIConfigure::Get(IConfigure::KeyValueType key, IConfigure::KeyValueType defaultkey)
				{
					typedef ::utility::string::StringUtil
						StringUtil;
					IConfigure::ReturnValueType result;
					StringUtil::TokenContainer tokens = StringUtil::SplitString(key, ".");

					try {
						boost::property_tree::ptree tree;
						tree = impl_->ptree_;

						for (StringUtil::TokenContainer::iterator itr = tokens.begin(); itr != tokens.end(); itr++)
						{
							StringUtil::TokenContainer::iterator next = itr + 1;
							if (tokens.end() != next)
							{
								tree = impl_->ptree_.get_child(itr->c_str());
							}
							else
							{
								result = tree.get<std::string>(itr->c_str());
							}
						}
					}
					catch (boost::exception & ) {
						std::cerr << "[Error] InIConfigure::Get() key = " << key << " path = " << impl_->file_path_ << std::endl;
						result = defaultkey;
					}

					return result;
				}
			}
		}
	}
} /// common::logic::configure::object