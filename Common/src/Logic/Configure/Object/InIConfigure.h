﻿#pragma once
#include "../IConfigure.h"
#include "../../../../../Utility/src/Time/TimeToString.h"
#include "../../../../../Utility/src/Error/ErrorLog.h"
#include <boost/lexical_cast.hpp>
#include <boost/exception/all.hpp>
#include <memory>

namespace common {
	namespace logic {
		namespace configure {
			namespace object {
				/**
					@class : XmlConfigure
					@date : 2015/07/22
					@brief : InI 형식으로 된 설정파일을 파싱하여 사용합니다.

					@author : jinny20123(안 정웅)
				*/
				class InIConfigure
					: public IConfigure
				{
				public:
					InIConfigure(const char* ini_path);
					virtual ~InIConfigure();

					virtual ReturnValueType Get(KeyValueType key, KeyValueType defaultkey);

					template<typename T>
					void Get(const char* key, T& value)
					{
						value = boost::lexical_cast<T>(Get(key));
					}
				private:
					struct Impl;
					std::unique_ptr<Impl> impl_;
				};
			}
		}
	}
} /// common::logic::configure::object