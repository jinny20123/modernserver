﻿#pragma once
#include "../IConfigure.h"
#include "../../../../../Utility/src/Time/TimeToString.h"
#include "../../../../../Utility/src/Error/ErrorLog.h"
#include <boost/lexical_cast.hpp>
#include <boost/exception/all.hpp>
#include <memory>

namespace common {
	namespace logic {
		namespace configure {
			namespace object {
				/**
					@class : XmlConfigure
					@date : 2015/04/30
					@brief : Xml 형식으로 된 설정파일을 파싱하여 사용합니다.
							Get 함수의 경우 . 문자열을 기준으로 부모관계를 설명합니다.
							<serverinfo>
								<port></port>
							</serverinfo>
							Get("serverinfo.port) 와 같은 형식으로 데이터를 상속관계를 처리합니다.
					@author : jinny20123(안 정웅)
				*/
				class XmlConfigure
					: public IConfigure
				{
				public:
					XmlConfigure(const char* xml_path);
					virtual ~XmlConfigure();
					virtual ReturnValueType Get(KeyValueType key, KeyValueType defaultkey);

				private:
					struct Impl;
					std::unique_ptr<Impl> impl_;
				};
			}
		}
	}
} /// common::logic::configure::object