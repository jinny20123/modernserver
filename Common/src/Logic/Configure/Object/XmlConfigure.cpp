﻿#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem.hpp>
#include "XmlConfigure.h"
#include "../../../../../Utility/src/String/string_util.h"

namespace common {
	namespace logic {
		namespace configure {
			namespace object {
				/// 내부에서 사용할 변수를 정의합니다.
				/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
				struct XmlConfigure::Impl
				{
					boost::property_tree::ptree ptree_;
				};
				XmlConfigure::XmlConfigure(const char* xml_path)
				{
					impl_ = std::unique_ptr<Impl>();

					std::string config_path;
					config_path.append(xml_path);
					boost::property_tree::xml_parser::read_xml(config_path, impl_->ptree_);
				}
				XmlConfigure::~XmlConfigure()
				{}

				IConfigure::ReturnValueType
					XmlConfigure::Get(IConfigure::KeyValueType key, KeyValueType defaultkey)
				{
					typedef ::utility::string::StringUtil
						StringUtil;
					IConfigure::ReturnValueType result;
					StringUtil::TokenContainer tokens = StringUtil::SplitString(key, ".");

					boost::property_tree::ptree tree;
					tree = impl_->ptree_;

					for (StringUtil::TokenContainer::iterator itr = tokens.begin(); itr != tokens.end(); itr++)
					{
						StringUtil::TokenContainer::iterator next = itr + 1;
						if (tokens.end() != next)
						{
							tree = impl_->ptree_.get_child(itr->c_str());
						}
						else
						{
							result = tree.get<std::string>(itr->c_str());
						}
					}

					return result;
				}
			}
		}
	}
} /// common::logic::configure::object