﻿#include "TcpServerStatic.h"
#include "../Logic/Configure/Manager/ConfigureManager.h"
#include "../Logic/PacketProcess/PacketProcessContainer.h"
#include "../../Utility/src/macro.h"
#include "../../Utility/src/Dump/mini_dump.h"
#include <memory>

namespace tcpserver {
	namespace static_ {
		struct TCPServerStatic::Impl
		{
			std::shared_ptr<ConfigureManager>				pconfigure_manager;
			std::shared_ptr<PacketProcessContainer>			ppacketprocess_container;
			std::shared_ptr<Producer>						ppacketproducer;
		};

		TCPServerStatic::TCPServerStatic()
		{
			// dump 파일 생성되도록 등록 작업
			::utility::dump::minidump::MiniDump::Begin();
			MAKE_UNIQUE(mImpl, Impl);

			mImpl->pconfigure_manager = std::make_shared<ConfigureManager>();
			mImpl->ppacketprocess_container = std::make_shared<PacketProcessContainer>();
			mImpl->ppacketproducer = std::make_shared<Producer>();
		}

		TCPServerStatic::~TCPServerStatic()
		{
			::utility::dump::minidump::MiniDump::End();
		}

#define GetFunction(classname, variable)	\
TCPServerStatic::classname				*	\
TCPServerStatic::Get##classname()			\
{											\
	return variable.get();					\
}

#define GetFunction3(classname, variable, funname)	\
TCPServerStatic::classname			*				\
TCPServerStatic::Get##funname()						\
{													\
	return variable.get();							\
}

		GetFunction(ConfigureManager, mImpl->pconfigure_manager)
			GetFunction(PacketProcessContainer, mImpl->ppacketprocess_container);
		GetFunction3(Producer, mImpl->ppacketproducer, PacketProducer);
	}
} /// tcpserver::static_