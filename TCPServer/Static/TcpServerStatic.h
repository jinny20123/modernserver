﻿#pragma once
#include <boost/serialization/singleton.hpp>
#include <memory>
#include "../../Utility/src/Engine/Processor/producer.hpp"

namespace tcpserver {
	namespace logic {
		namespace configure {
			namespace manager {
				class ConfigureManager;
			}
		}
	}
}

namespace tcpserver {
	namespace logic {
		namespace packetprocess {
			class PacketProcessContainer;
		}
	}
}

namespace tcpserver {
	namespace network {
		namespace processor {
			class PacketProcessor;
		}
	}
}

namespace tcpserver {
	namespace static_ {
		/**
			@class : WebMiddleStatic
			@date : 2016/01/29
			@brief : 웹미들서버에서 싱글톤으로 접근할 객체를 정의합니다.
			@author : jinny20123(안 정웅)
		*/

		class TCPServerStatic
			: public boost::serialization::singleton<TCPServerStatic>
		{
		public:
			typedef	ENGINE_PRODUCER(::utility::object::message)
				Producer;
			typedef ::tcpserver::logic::configure::manager::ConfigureManager
				ConfigureManager;
			typedef ::tcpserver::logic::packetprocess::PacketProcessContainer
				PacketProcessContainer;
			typedef ::tcpserver::network::processor::PacketProcessor
				PacketProcessor;

			TCPServerStatic();
			virtual ~TCPServerStatic();

			ConfigureManager* GetConfigureManager();
			PacketProcessContainer* GetPacketProcessContainer();
			Producer* GetPacketProducer();

		private:
			struct Impl;
			std::unique_ptr<Impl>  mImpl;
		};

		inline TCPServerStatic& TCPSERVER_STATIC()
		{
			return TCPServerStatic::get_mutable_instance();
		}
	}
} /// tcpserver::static_
