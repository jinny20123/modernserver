﻿
#include "LogicHandler.h"
#include "LogicHandlerArgument.h"
#include "../../Logic/PacketProcess/PacketProcessContainer.h"
#include "../../Static/TcpServerStatic.h"
#include "../../../Utility/src/macro.h"


namespace tcpserver { namespace network { namespace handler {


/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct LogicHandler::Impl
{
	typedef ::tcpserver::logic::packetprocess::PacketProcessContainer
			PacketProcessContainer;

	PacketProcessContainer * packetprocess_container;;
}; 

LogicHandler::LogicHandler()
{
	MAKE_UNIQUE(mImpl, Impl);
	mImpl->packetprocess_container = ::tcpserver::static_::TCPSERVER_STATIC().GetPacketProcessContainer();
}
LogicHandler::~LogicHandler()
{
	
}

void 
LogicHandler::Prepare()
{
}

void 
LogicHandler::OnRecv(Argument * arg)
{
	// 패킷 컨테이너에 전달합니다. 
	if( Impl::PacketProcessContainer::RUN_RESULT::SUCCESS != mImpl->packetprocess_container->Run(arg) )
	{
		// 로그를 남기자!
	}
}

void 
LogicHandler::OnSend(Argument * arg)
{
}

void 
LogicHandler::OnReservation(Argument * arg)
{
}

void 
LogicHandler::OnClose(Argument * arg)
{
	CloseHandlerArgument * phandler_arg = nullptr;
	::utility::object::ArgumentUtil::CastArgument( arg, phandler_arg);

	if( nullptr == phandler_arg){ return;}

	// 로직스레드상에서 정리해야할 작업을 등록합니다.


	// 콜백함수를 실행합니다. 
	phandler_arg->close_callback_();

}
	
}}} /// tcpserver::network::handler