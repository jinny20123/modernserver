﻿#pragma once
#include "../../../Network/src/Handler/IHandler.h"
#include <memory>

namespace tcpserver {
	namespace network {
		namespace handler {
			/**
				@class : LogicHandler
				@date : 2015/05/07
				@brief : WebMiddle 서버로 전달 받은 데이터를 동작에 맞도록 처리합니다.
				@author : jinny20123(안 정웅)
			*/
			class LogicHandler
				: public ::network::handler::IHandler
			{
			public:
				LogicHandler();
				virtual ~LogicHandler();

				virtual void Prepare();
				virtual void OnRecv(Argument* arg);
				virtual void OnSend(Argument* arg);
				virtual void OnReservation(Argument* arg);
				virtual void OnClose(Argument* arg);
			private:
				struct Impl;
				std::unique_ptr<Impl> mImpl;
			};
		}
	}
} /// tcpserver::network::handler