﻿#pragma once

#include "../../../Utility/src/Object/argument.h"
#include "../../../CommonPacket/src/Packet.h"
#include "../../../Network/src/Session/Session_fwd.h"
#include <functional>

 namespace network { namespace util { namespace endpoint {
	class IEndPoint;
}}}

namespace tcpserver { namespace logic { namespace reservation {
	class IReservation;
}}}

namespace tcpserver { namespace network { namespace handler {

typedef ::utility::object::Argument
		Argument;
typedef ::network::session::SessionBase
		ISession;
typedef ::tcpserver::logic::reservation::IReservation
		IReservation;

/** 
	@class : PacketHandlerArgument
	@date : 2015/05/07
	@brief : Udp패킷 처리시 사용되는 매개변수입니다. 
	@author : jinny20123(안 정웅) 
*/
struct PacketHandlerArgument
	: public Argument
{
	typedef ::packet::Packet
			Packet;
	

	PacketHandlerArgument() : ppacket_(nullptr), psession_(nullptr)
	{		
	}

	bool IsEnable()
	{
		if ( nullptr == this->ppacket_ || 
			nullptr == this->psession_ ) 
		{ return false; }  

		return true;
	}

	Packet	*							ppacket_;
	ISession *							psession_;
};

struct ReservationHandlerArgument
	: public Argument
{
	typedef std::function<void()>
			RunFunc;

	ReservationHandlerArgument() : sec_time_(0), pReservation_(nullptr) {}
	ReservationHandlerArgument(time_t sec_time, IReservation * pReservation) :
		sec_time_(sec_time), pReservation_(pReservation)
	{}

	time_t			sec_time_;	
	IReservation *	pReservation_;
};

struct CloseHandlerArgument
	: public Argument
{
	typedef ::network::session::CloseCallBack
			CloseCallBack;
	CloseHandlerArgument() : psession_(nullptr) {}
	CloseHandlerArgument(ISession * psession, CloseCallBack close_callback)
		: psession_(psession), close_callback_(close_callback)
	{}

	ISession *					psession_;
	CloseCallBack				close_callback_;
};
	
}}} /// tcpserver::network::handler