﻿#include "../../../Utility/src/macro.h"
#include "PacketProcessor.h"
#include "../handler/LogicHandler.h"
#include "../../../Utility/src/Object/argument.h"

namespace tcpserver {
	namespace network {
		namespace processor {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct PacketProcessor::Impl
			{
				std::unique_ptr<::network::handler::IHandler> phandler_;
			};

			PacketProcessor::PacketProcessor(ProducerObj* pproducer, size_t thread_count)
				: Consumer(pproducer, thread_count)
			{
				MAKE_UNIQUE(mImpl, Impl);
				MAKE_UNIQUE(mImpl->phandler_, ::tcpserver::network::handler::LogicHandler);

				ThreadRun();
			}
			PacketProcessor::~PacketProcessor()
			{}

			void
				PacketProcessor::Handle(const object& obj)
			{
				typedef utility::object::Argument
					Argument;

				if (nullptr == obj.pValue_)
				{
					// 로그 삽입
				}

				Argument* arg = static_cast<Argument*>(obj.pValue_);

				switch (static_cast<IHandler::HANDLE_TYPE>(obj.Handle_))
				{
				case IHandler::HANDLE_TYPE::HANDLE_ONRECV:
					mImpl->phandler_->OnRecv(arg);
					break;
				case IHandler::HANDLE_TYPE::HANDLE_ONSEND:
					mImpl->phandler_->OnSend(arg);
					break;
				case IHandler::HANDLE_TYPE::HANDLE_ONRESERVATION:
					mImpl->phandler_->OnReservation(arg);
					break;
				case IHandler::HANDLE_TYPE::HANDLE_CLOSE:
					mImpl->phandler_->OnClose(arg);
					break;
				default:
				{
					// 로그 기록
				}
				break;
				}
			}
		}
	}
} /// tcpserver::network::processor