﻿#pragma once
#include <memory>
#include "../../../Utility/src/Engine/Processor/consumer.hpp"

namespace network {
	namespace handler {
		class IHandler;
	}
}

namespace tcpserver {
	namespace network {
		namespace processor {
			/**
				@class : PacketProcessor
				@date : 2016/02/12
				@brief : 네트워크로 전달 받은 패킷을 Handle함수를 통해서 처리합니다.
				@author : jinny20123(안 정웅)
			*/
			class PacketProcessor
				: public CONSUMER(utility::object::message)
			{
			public:
				using IHandler = ::network::handler::IHandler;

				PacketProcessor(ProducerObj * pproducer, size_t thread_count);
				virtual ~PacketProcessor();
			protected:
				virtual void Handle(const object & obj);
			private:
				struct Impl;
				std::unique_ptr < Impl > mImpl;
			};
		}
	}
} /// tcpserver::network::processor