﻿#include "SessionLogic.h"
#include "../../../Network/Handler/LogicHandlerArgument.h"
#include "../../../Static/TcpServerStatic.h"
#include "../../../Logic/PacketProcess/PacketProcessContainer.h"
#include "../../../../Network/src/Session/Session.h"
#include "../../../../Network/src/Session/Type.h"
#include "../../../../Network/src/Session/Logic/SessionLogicArgument.h"
#include "../../../../Network/src/Handler/IHandler.h"
#include "../../../../CommonPacket/src/Packet.h"
#include "../../../../CommonPacket/src/PacketBuilder.h"
#include "../../../../Utility/src/Object/ArgumentFactory.h"

namespace tcpserver {
namespace network {
namespace session {
namespace logic {
	/// 내부에서 사용할 변수를 정의합니다.
	/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
	struct SessionLogic::Impl
	{
		Impl() {}
	};

	SessionLogic::SessionLogic()
	{
		MAKE_UNIQUE(mImpl, Impl);
	}
	SessionLogic::~SessionLogic()
	{}

	void
	SessionLogic::Capture(Argument&& arg)
	{
		//
	}

	void
	SessionLogic::Release()
	{
	}

	SessionLogic::RecvResult
	SessionLogic::Recv(Argument&& arg)
	{
		typedef ::packet::Packet
			Packet;
		typedef ::tcpserver::network::handler::PacketHandlerArgument
			PacketHandlerArgument;
		typedef ::utility::object::ArgumentFactory<PacketHandlerArgument>
			ArgumentFactory;
		typedef ::network::handler::IHandler
			IHandler;
		using SessionLogicRecvArgument =::network::session::logic::SessionLogicRecvArgument;

		unsigned int ret = 0;
		// 처리완료된 사이즈
		size_t complete_size = 0;

		SessionLogicRecvArgument* sa_arg = nullptr;
		::utility::object::ArgumentUtil::CastArgument(&arg, sa_arg);
		if (nullptr == sa_arg) { return RecvResult(complete_size, true); }
		// 현재 처리하고 있는 버퍼 사이즈
		size_t current_buffer_size = sa_arg->buffer_size_;

		Packet* pMakePacket = nullptr;
		PacketHandlerArgument* phandler_argument = nullptr;

		try
		{
			do
			{
				// 전달받은패킷이 최소 사이즈를 만족 시키지 못한다면 리턴
				if (Packet::GetLeastSize() > current_buffer_size)
				{
					break;
				}

				if (nullptr == sa_arg)
				{
					throw ::network::session::logic::ERROR_TYPE::ARGUMENT_NULL;
				}

				Packet* pExcutePacket = reinterpret_cast<Packet*>(sa_arg->buffer_);
				size_t buffersize = sa_arg->buffer_size_;

				if (packet::MAXBUFFERSIZE < pExcutePacket->GetSize() ||
					::tcpserver::static_::TCPSERVER_STATIC().GetPacketProcessContainer()->GetMaxCommand() < pExcutePacket->GetCommand())	// [LOG] 무언가 잘못된 패킷이 전송된 것이다. 어떻게 처리하는게 좋을까? 세션을 끊어 버릴까?
				{
					throw ::network::session::logic::ERROR_TYPE::ABNORMAL_CHECK;
				}

				// 패킷을 처리할 만큼 전달받지 못한 경우
				if (current_buffer_size < pExcutePacket->GetSize())
				{
					break;
				}

				// 패킷 객체를 풀에서 생성합니다. 사용완료 후 풀에 반환해야합니다.(자동으로 반환되도록 구성 해야 한다.)
				pMakePacket = PACKET_BUILDER::Build<Packet*>();
				if (nullptr == pMakePacket)
				{
					throw ::network::session::logic::ERROR_TYPE::PACKET_MAKE_FAIL;
				}

				// Argument 객체를 풀에서 생성합니다. 사용완료 후 풀에 반환해야합니다.(자동으로 반환되도록 구성 해야 한다.)
				phandler_argument = ArgumentFactory::CreateT();
				if (nullptr == phandler_argument)
				{
					throw ::network::session::logic::ERROR_TYPE::ARGUMENT_MAKE_FAIL;
				}

				memcpy(pMakePacket, pExcutePacket, pExcutePacket->GetTotalSize());
				phandler_argument->ppacket_ = pMakePacket;
				phandler_argument->psession_ = sa_arg->psession_;

				complete_size += pExcutePacket->GetTotalSize();
				current_buffer_size -= pExcutePacket->GetTotalSize();
				//WebMiddleStatic().GetPacketProducer()->push 이벤트로 패킷 전달
				::tcpserver::static_::TCPSERVER_STATIC().GetPacketProducer()->push(static_cast<int>(IHandler::HANDLE_TYPE::HANDLE_ONRECV), static_cast<void*>(phandler_argument));
			} while (current_buffer_size >= Packet::GetLeastSize());
		}
		catch (::network::session::logic::ERROR_TYPE & ret)
		{
			// 에러에 대한 처리를 합니다.
			// 사용되지 않고 할당된 메모리에 대한 반환
			if (nullptr != pMakePacket)
			{
				PACKET_BUILDER::Destroy(pMakePacket);
			}

			if (nullptr != phandler_argument)
			{
				ArgumentFactory::Destroy(phandler_argument);
			}

			// 비정상적인 유저라면 세션을 종료합니다.
			if (ret == ::network::session::logic::ERROR_TYPE::ABNORMAL_CHECK)
			{
				throw;
			}

			// 로그 출력 하기
		}

		return RecvResult(complete_size, true);
	}

	void
	SessionLogic::Close(Argument&& arg)
	{
		typedef ::network::session::logic::SessionLogicCloseArgument
			SessionLogicCloseArgument;
		typedef ::network::handler::IHandler
			IHandler;
		typedef ::tcpserver::network::handler::CloseHandlerArgument
			CloseHandlerArgument;
		typedef ::utility::object::ArgumentFactory<CloseHandlerArgument>
			ArgumentFactory;

		CloseHandlerArgument* phandler_argument = nullptr;
		SessionLogicCloseArgument* psession_arg = nullptr;

		try
		{
			::utility::object::ArgumentUtil::CastArgument(&arg, psession_arg);

			if (nullptr == psession_arg) { throw ::network::session::logic::ERROR_TYPE::ARGUMENT_NULL; }

			// 다른 스레드에서 사용 될 데이터이기 때문에 풀로 관리합니다. 사용후 반납이 필요합니다.
			phandler_argument = ArgumentFactory::CreateT();

			if (nullptr == phandler_argument) { throw ::network::session::logic::ERROR_TYPE::ARGUMENT_MAKE_FAIL; }

			phandler_argument->psession_ = psession_arg->psession_;
			phandler_argument->close_callback_ = psession_arg->callback_;

			::tcpserver::static_::TCPSERVER_STATIC().GetPacketProducer()->push(static_cast<int>(IHandler::HANDLE_TYPE::HANDLE_CLOSE), static_cast<void*>(phandler_argument));
		}
		catch (::network::session::logic::ERROR_TYPE& /*ret*/)
		{
			// 에러에 대한 처리를 합니다.
			// 사용되지 않고 할당된 메모리에 대한 반환
			if (nullptr != phandler_argument)
			{
				ArgumentFactory::Destroy(phandler_argument);
			}

			// 로그 출력 하기
		}
	}
}}}} /// tcpserver::network::session::logic