﻿#pragma once
#include "../../../../Utility/src/macro.h"
#include "../../../../Network/src/Session/Logic/ISessionLogic.h"
#include <memory>
namespace tcpserver { namespace network { namespace session { namespace logic {

typedef ::network::session::logic::ISessionLogic
		ISessionLogic;
/** 
	@class : SASessionLogic 
	@date : 2016-02-24
	@brief : 서든어택 서버와의 통
	@author : jinny20123(안 정웅)
*/
class SessionLogic 
	: public ISessionLogic
{
public: 
	SessionLogic();
	virtual ~SessionLogic();
	virtual void Capture(Argument && arg)		OVERRIDE;
	virtual void Release()						OVERRIDE;

	virtual RecvResult Recv(Argument && arg)	OVERRIDE;
	virtual void Close(Argument && arg)			OVERRIDE;
private :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};
	
}}}} /// tcpserver::network::session::logic