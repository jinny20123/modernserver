﻿#pragma once

#include <memory>
#include "../../../Utility/src/Object/argument.h"

namespace tcpserver { namespace logic { namespace construct { 

typedef ::utility::object::Argument
		Argument;

struct TCPConsturctCreateArgument
	: public Argument
{
	TCPConsturctCreateArgument() {}
	TCPConsturctCreateArgument(size_t packet_thread_count, unsigned short bind_port)
		: packet_thread_count_(packet_thread_count), bind_port_(bind_port)
	{}

	size_t packet_thread_count_;		///< 패킷을 처리하는 쓰레드 수
	unsigned short bind_port_;				///< 바인딩할 포트
	
};
	
}}} /// tcpserver::logic::construct