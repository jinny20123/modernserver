﻿#include "TCPServerConstruct.h"
#include "../TCPServerConstructArgument.h"
#include "../../Configure/Manager/ConfigureManager.h"
#include "../../../Network/Session/Logic/SessionLogic.h"
#include "../../../Network/Processor/PacketProcessor.h"
#include "../../../Static/TcpServerStatic.h"
#include "../../../../Common/src/Logic/Configure/IConfigure.h"
#include "../../../../Common/src/Logic/Construct/ConstructArgument.h"
#include "../../../../Common/src/Logic/Construct/Object/AsioConstruct.h"
#include <boost/foreach.hpp>
#include <vector>

namespace tcpserver {
	namespace logic {
		namespace construct {
			namespace object {
				/// 내부에서 사용할 변수를 정의합니다.
				/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
				struct TCPServerConstruct::Impl
				{
					typedef std::vector<IConstruct*>
						ConstructContainer;
					typedef	::common::logic::configure::IConfigure
						IConfigure;
					typedef ::network::session::logic::ISessionLogic
						ISessionLogic;
					typedef ::tcpserver::network::processor::PacketProcessor
						PacketProcessor;

					Impl() {}

					std::unique_ptr<ISessionLogic>		psession_logic_;
					std::unique_ptr<PacketProcessor>	ppacket_processor_;
					ConstructContainer					construct_container_;
					IConfigure* server_config;
					size_t								packet_thread_count_;
					unsigned short							bind_port_;
				};

				TCPServerConstruct::TCPServerConstruct(Argument* arg)
				{
					typedef ::tcpserver::network::session::logic::SessionLogic
						SessionLogic;

					MAKE_UNIQUE(mImpl, Impl);

					TCPConsturctCreateArgument* tcp_arg = nullptr;
					::utility::object::ArgumentUtil::CastArgument(arg, tcp_arg);
					BOOST_ASSERT(tcp_arg);

					mImpl->packet_thread_count_ = tcp_arg->packet_thread_count_;
					mImpl->bind_port_ = tcp_arg->bind_port_;

					MAKE_UNIQUE(mImpl->psession_logic_, SessionLogic);
				}
				TCPServerConstruct::~TCPServerConstruct()
				{
					BOOST_FOREACH(auto pcontruct, mImpl->construct_container_)
					{
						{ delete(pcontruct); pcontruct = nullptr; }
					}
				}

				void
					TCPServerConstruct::SetUp()
				{
					Prepare();

					BOOST_FOREACH(auto pconstruct, mImpl->construct_container_)
					{
						pconstruct->SetUp();
					}
				}
				void
					TCPServerConstruct::Run()
				{
					BOOST_FOREACH(auto pconstruct, mImpl->construct_container_)
					{
						pconstruct->Run();
					}
				}

				void
					TCPServerConstruct::Insert(IConstruct* pconstruct)
				{
					mImpl->construct_container_.push_back(pconstruct);
				}

				void
					TCPServerConstruct::Prepare()
				{
					PrepareProecessor();
					PrepareAsio();
				}

				/**
					@date : 2016-09-12
					@brief : Asio 라이브러리를 통해서 TCP 세션을 등록하기 위한 선작업을 처리합니다.
					@author : jinny20123(안 정웅)
				*/
				void
					TCPServerConstruct::PrepareAsio()
				{
					namespace CConstruct = ::common::logic::construct;

					Insert(new CConstruct::object::AsioConstruct(CConstruct::AsioConstructArgument(1, mImpl->bind_port_, mImpl->psession_logic_.get(), CConstruct::AsioConstructArgument::PROTOCOLTYPE::TCP)));
				}

				/**
					@date : 2016-09-12
					@brief : 패킷 처리를 위한 프로세서를 실행하기 위한 작업을 등록합니다.
					@author : jinny20123(안 정웅)
				*/
				void
					TCPServerConstruct::PrepareProecessor()
				{
					size_t nPacketProcessorThreadCount = mImpl->packet_thread_count_;
					// 패킷 프로세서 생성
					MAKE_UNIQUE(mImpl->ppacket_processor_, Impl::PacketProcessor, ::tcpserver::static_::TCPSERVER_STATIC().GetPacketProducer(), nPacketProcessorThreadCount);
				}
			}
		}
	}
} /// webmiddle::logic::construct::object