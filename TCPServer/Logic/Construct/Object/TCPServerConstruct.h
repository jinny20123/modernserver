﻿#pragma once
#include "../../../../Common/src/Logic/Construct/IConstruct.h"
#include <memory>

namespace tcpserver { namespace logic { namespace construct { namespace object { 


/**
	@class : WebMiddleConstruct
	@date : 2016-09-12
	@brief : WebMiddle 서버를 구성하기 위해서 해야 할 작업들을 정의합니다. 
	@author : jinny20123(안 정웅)
*/
class TCPServerConstruct 
	: public ::common::logic::construct::IConstruct 
{
public: 
	TCPServerConstruct(Argument * arg);
	virtual ~TCPServerConstruct();

	virtual void SetUp() OVERRIDE;
	virtual void Run() OVERRIDE;
	virtual void Insert(IConstruct * pconstruct);

private :
	void			Prepare();
	void			PrepareAsio();
	void			PrepareProecessor();

	struct Impl;
	std::unique_ptr<Impl> mImpl;
};
	
}}}} /// tcpserver::logic::construct::object