﻿#pragma once
#include <boost/serialization/singleton.hpp>
#include <memory>

namespace common {
	namespace logic {
		namespace configure {
			class IConfigure;

			namespace object
			{
				class InIConfigure;
			}
		}
	}
}

namespace tcpserver {
	namespace logic {
		namespace configure {
			namespace manager {
				/**
					@class : ConfigureManager
					@date : 2016-09-12
					@brief : PokerServer의 Config 정보를 처리합니다.
					@author : jinny20123(안 정웅)
				*/
				class ConfigureManager
				{
				public:
					typedef ::common::logic::configure::IConfigure
						IConfigure;
					typedef ::common::logic::configure::object::InIConfigure
						InIConfigure;

					ConfigureManager();
					virtual ~ConfigureManager();

					enum ConfigureType
					{
						SERVERINFO,			///< server.xml 파일
					};

					void SetUp();			///< ConfigureManager를 설정하는 작업 실행 - 파일 연결하기
					IConfigure* Get(ConfigureType type);	///< 해당 타입의 Configure를 리턴합니다.
				private:
					struct Impl;
					std::unique_ptr<Impl> mImpl;
				};
			}
		}
	}
} /// tcpserver::logic::configure::manager