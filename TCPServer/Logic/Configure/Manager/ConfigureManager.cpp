﻿#include "ConfigureManager.h"
#include "../../../../Utility/src/macro.h"
#include "../../../../Common/src/Logic/Configure/Object/InIConfigure.h"
#include <boost/filesystem.hpp>
#include <unordered_map>
#include <iostream>

namespace tcpserver {
	namespace logic {
		namespace configure {
			namespace manager {
#define INSERT_CONFIGURE(type, Object, filename) \
	mImpl->container_.insert(Impl::ConfigureContainer::value_type(type, std::make_shared<Object>( ( config_path + filename).c_str() ) ) );
				/// 내부에서 사용할 변수를 정의합니다.
				/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
				struct ConfigureManager::Impl
				{
					typedef std::unordered_map<ConfigureType, std::shared_ptr<IConfigure>>
						ConfigureContainer;

					Impl() {}

					ConfigureContainer container_;
					std::shared_ptr<IConfigure> base_ini_;
				};

				ConfigureManager::ConfigureManager()
				{
					MAKE_UNIQUE(mImpl, Impl);
				}
				ConfigureManager::~ConfigureManager()
				{
				}

				void
					ConfigureManager::SetUp()
				{
					boost::filesystem::path current_path = boost::filesystem::current_path();
					mImpl->base_ini_ = std::make_shared<InIConfigure>((current_path.string() + "\\Base.ini").c_str());

					std::string config_detail_path = mImpl->base_ini_->Get("INFO.ConfigFolder", "");

					std::cout << "read config : " << config_detail_path.c_str() << std::endl;
					/// 해당 경로에 폴더 만들기

					std::string config_path = current_path.string();
					config_path.append("\\config\\");
					config_path.append(config_detail_path);

					if (!boost::filesystem::exists(boost::filesystem::path(config_path)))
					{
						boost::filesystem::create_directory(config_path);
					}

					// Configure 파일을 읽습니다.
					INSERT_CONFIGURE(SERVERINFO, InIConfigure, "\\server.ini");
				}

				ConfigureManager::IConfigure*
					ConfigureManager::Get(ConfigureType type)
				{
					Impl::ConfigureContainer::iterator itr = mImpl->container_.find(type);
					if (mImpl->container_.end() == itr)
					{
						return nullptr;
					}

					return itr->second.get();
				}
			}
		}
	}
} /// tcpserver::logic::configure::manager