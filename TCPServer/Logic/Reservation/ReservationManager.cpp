﻿#include "ReservationManager.h"
#include "IReservation.h"
#include <thread>
#include <vector>
#include <list>
#include <mutex>
#include <boost/foreach.hpp>

namespace tcpserver {
	namespace logic {
		namespace reservation {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct ReservationManager::Impl
			{
				Impl() {}

				std::thread run_thread;
				std::mutex insert_lock;
				std::vector<IReservation*> insert_container;
				std::list<IReservation*> run_container;

				void Excute();
			};

			ReservationManager::ReservationManager()
			{
				MAKE_UNIQUE(mImpl, Impl);
				mImpl->insert_container.clear();
				mImpl->run_container.clear();
			}
			ReservationManager::~ReservationManager()
			{
			}

			void
				ReservationManager::Insert(IReservation* pReservation)
			{
				mImpl->insert_lock.lock();
				mImpl->insert_container.push_back(pReservation);
				mImpl->insert_lock.unlock();
			}

			void
				ReservationManager::Impl::Excute()
			{
				insert_lock.lock();
				BOOST_FOREACH(auto preservation, insert_container)
				{
					run_container.insert(run_container.end(), preservation);
				}
				insert_container.clear();
				insert_lock.unlock();
			}
		}
	}
} /// tcpserver::logic::reservation