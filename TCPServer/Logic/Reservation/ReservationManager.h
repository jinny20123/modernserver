﻿#pragma once

#include <memory>
#include "../../../Utility/src/macro.h"
namespace tcpserver { namespace logic { namespace reservation { 

class IReservation;

/** 
	@class : ReservationManager 
	@date : 2016-10-07
	@brief : 예약 등록된 작업을 주기적으로 검사하여 실행 로직에 넣습니다. 
	@author : jinny20123(안 정웅)
*/
class ReservationManager 
{
public: 
	ReservationManager();
	virtual ~ReservationManager();

	void Insert(IReservation * pReservation);
private :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};
	
}}} /// tcpserver::logic::reservation