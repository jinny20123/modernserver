﻿#pragma once

#include <memory>
#include "../../../Utility/src/macro.h"
namespace tcpserver { namespace logic { namespace reservation { 

class IReservation 
{
public: 
	virtual ~IReservation(){};

	/// IReservation 인터페이스를 정의합니다. 
	virtual void Run() = 0;							///< 시간이 되어서 실행시킬 객체 
	virtual time_t GetRuntime() = 0;				///< 실행될 시간을 셋팅합니다. 
};

}}} /// tcpserver::logic::reservation
