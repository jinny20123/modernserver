﻿#pragma once

#include "../../../Utility/src/Object/argument_fwd.h"
#include "../../../CommonPacket/src/PacketDefine.h"
#include <memory>
#include <functional>
#include <unordered_map>

namespace tcpserver {
	namespace logic {
		namespace packetprocess {
			class IPacketRegister
			{
			public:
				using Argument = ::utility::object::Argument;
				using ProcessPacketLogicFun = std::function<bool(Argument* arg)>;
				using PacketLogicFuncContainer = std::unordered_map<::packet::packet_commandtype, ProcessPacketLogicFun>;

				virtual ~IPacketRegister() {};

				/// IPacketRegister 인터페이스를 정의합니다.
				virtual PacketLogicFuncContainer& GetRecvProcessCon() = 0;
				virtual void Prepare() = 0;
				virtual const unsigned int GetMaxCommand() const = 0;
			};
		}
	}
} /// tcpserver::logic::packetprocess
