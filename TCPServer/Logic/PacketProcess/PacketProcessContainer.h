﻿#pragma once
#include <cstdint>
#include <memory>
#include <boost/serialization/singleton.hpp>

namespace utility { namespace object {
	struct Argument;
}}

namespace tcpserver { namespace logic { namespace packetprocess {

class IPacketRegister;
class PacketProcessContainer 
{
public: 
	typedef ::utility::object::Argument
			Argument;

	struct RUN_RESULT
	{
		enum RESULT
		{
			SUCCESS = 0,
			ARGUMENT_ERROR,
			NOT_FIND,
		};
	};

	PacketProcessContainer();
	virtual ~PacketProcessContainer();

	const RUN_RESULT::RESULT Run(Argument * arg);
	virtual void Prepare(std::shared_ptr<IPacketRegister>& pregister);
	const unsigned int GetMaxCommand() const;

protected :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
private :
	

};

	
}}} /// tcpserver::logic::packetprocess