﻿#include "PacketProcessContainer.h"
#include "IPacketRegister.h"
#include "../../network/Handler/LogicHandlerArgument.h"
#include "../../../Utility/src/macro.h"
#include "../../../CommonPacket/src/Packet.h"
#include <functional>
#include <vector>
#include <unordered_map>

namespace tcpserver {
	namespace logic {
		namespace packetprocess {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct PacketProcessContainer::Impl
			{
				/// Recv 처리 함수 객체
				std::shared_ptr<IPacketRegister> pRegister;
			};

			PacketProcessContainer::PacketProcessContainer()
			{
				mImpl = std::make_unique<Impl>();
			}
			PacketProcessContainer::~PacketProcessContainer()
			{
			}

			const PacketProcessContainer::RUN_RESULT::RESULT
			PacketProcessContainer::Run(Argument* arg)
			{
				typedef ::tcpserver::network::handler::PacketHandlerArgument
					PacketHandlerArgument;
				auto & con = mImpl->pRegister->GetRecvProcessCon();

				PacketHandlerArgument* pph_argument = nullptr;
				::utility::object::ArgumentUtil::CastArgument(arg, pph_argument);
				if (nullptr == pph_argument
					|| false == pph_argument->IsEnable())
				{
					return RUN_RESULT::ARGUMENT_ERROR;
				}

				const auto itr = con.find(pph_argument->ppacket_->GetCommand());
				if (con.end() == itr)
				{
					return RUN_RESULT::NOT_FIND;
				}

				itr->second(arg);

				return RUN_RESULT::SUCCESS;
			}

			
			void
			PacketProcessContainer::Prepare(std::shared_ptr<IPacketRegister> & pregister)
			{	
				mImpl->pRegister = std::move(pregister);
				mImpl->pRegister->Prepare();
			}

			const unsigned int
				PacketProcessContainer::GetMaxCommand() const
			{
				return mImpl->pRegister->GetMaxCommand();
			}
		}
	}
} /// tcpserver::logic::packetprocess