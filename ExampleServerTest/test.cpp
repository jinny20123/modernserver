﻿#include "pch.h"
#include "Utility/src/Object/queue.h"
#include "ExamServer/ExamChatServerLib/src/Session/ServerProxyManager.h"
#include "Network/src/Session/Object/AsioTcpSession.h"
#include "ExamServer/ExamChatServerLib/src/Logic/User/UserManager.h"
#include "ExamServer/ExamChatServerLib/src/Logic/User/User.h"


// 서버 프록시 매니져 테스트 로직 
// 세션을 추가 삭제 하는 로직을 테스트합니다. 
TEST(ServerProxyManager, ServerProxyAddAndDel){
    using namespace ::examchatserver::session;

    ServerProxyManager spm;
    uint32_t added_index = 0;
    ::network::session::SessionBase* psession = new ::network::session::object::AsioTcpSession;
    ASSERT_TRUE(psession != nullptr);

    // server proxy 추가
    EXPECT_TRUE(spm.AddServerProxy(1, psession, added_index));
    EXPECT_EQ(kFrontServerStartIndex, added_index);
    EXPECT_TRUE(spm.AddServerProxy(1, psession, added_index));
    EXPECT_EQ(kFrontServerStartIndex + 1, added_index);

    // server proxy 삭제
    EXPECT_TRUE(spm.DelServerProxy(added_index));
}

// 유저 Manager 테스트 로직 
// 유저 Manager에 유저를 추가 삭제를 테스트 합니다.
TEST(UserManager, UserManagerAddAndDel) {
    using namespace ::examchatserver::logic;

    // sn 발급
    UserManager um;
    uint64_t issued_sn = um.IssueUserSn();
    EXPECT_EQ(kUserSnStartIndex, issued_sn);

    // user 객체 추가
    auto puser = std::make_shared<User>();
    puser->SetUserId("test01");
    ASSERT_TRUE(puser != nullptr);
    EXPECT_FALSE(um.FindUserName(puser->UserId()));
    EXPECT_TRUE(um.AddUser(issued_sn, puser));

    // user 찾기
    const auto finduser = um.FindUser(issued_sn);
    ASSERT_TRUE(finduser);

    uint64_t find_sn = 0;
    um.FindUserFunc(issued_sn, [&find_sn](std::shared_ptr<User>& puser) {
        find_sn = puser->UserSn();
        });
    EXPECT_EQ(find_sn, issued_sn);

    // user 삭제
    EXPECT_TRUE(um.DelUser(issued_sn));
}

// UserManager가 스레드 세이프 한지 확인합니다. 
TEST(UserManager, ThreadSafeCheck) {
    using namespace ::examchatserver::logic;

    UserManager um;
    std::vector<std::future<void>> threadContainer;
    int ThreadCount = 1000;
    for (int i = 0; i < ThreadCount; i++) {
        threadContainer.emplace_back(std::async(std::launch::async,
        [&um]() {
            try {
                uint64_t issued_sn = um.IssueUserSn();
                auto puser = std::make_shared<User>();
                EXPECT_TRUE(um.AddUser(issued_sn, puser));

                const auto finduser = um.FindUser(issued_sn);
                ASSERT_TRUE(finduser);

                uint64_t find_sn = 0;
                um.FindUserFunc(issued_sn, [&find_sn](std::shared_ptr<User> & puser){
                    find_sn = puser->UserSn();
                    });
                EXPECT_EQ(find_sn, issued_sn);

                // user 삭제
                if (0 == issued_sn % 2)
                    EXPECT_TRUE(um.DelUser(issued_sn));
            }
            catch (...) {
                FAIL() << "exception";
            }
        }));
    }

    for (auto& fut : threadContainer)
    {
        fut.get();
    }

    EXPECT_EQ(ThreadCount / 2, um.UserCount());
}

TEST(UTILTEST, queue) {
    utility::object::Queue<int> queue_;

    std::vector<std::future<void>> threadContainer;
    int ThreadCount = 1000;
    std::set<int> processed_data;

    auto pop_fut = std::async(std::launch::async,
        [&queue_,&processed_data, ThreadCount]() {
            do {
                int temp(0);
                queue_.Pop(temp);
                processed_data.insert(temp);
            } while (processed_data.size() < ThreadCount);
        });

    for (int i = 0; i < ThreadCount; i++) {
        threadContainer.emplace_back(std::async(std::launch::async,
            [&queue_, i]() {
                int temp = i;
                queue_.Push(temp);
            }));
    }

    for (auto& fut : threadContainer)
    {
        fut.get();
    }

    pop_fut.get();

    EXPECT_EQ(processed_data.size(), ThreadCount);
    EXPECT_TRUE(queue_.Empty());

}

// 특별히 초기화 작업을 하지 않는다면 main함수를 구성할 필요가 없습니다. 
//int main(int argc, char** argv) {
//    ::testing::InitGoogleTest(&argc, argv);
//    return RUN_ALL_TESTS();
//}