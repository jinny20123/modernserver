#include "pch.h"
#include "PacketLogicTest.h"
#include "ExamServer/ExamChatServerLib/src/Logic/PacketProcess/Obj/LobbyLogic.h"
#include "ExamServer/ExamChatServerLib/src/Singleton/ServerInstance.h"
#include "ExamServer/ExamChatServerLib/src/Logic/User/UserManager.h"
#include "ExamServer/ExamChatServerLib/src/Logic/User/User.h"
#include "ExamServer/ExamChatServerLib/src/Share/LogicPacket.pb.h"
#include "ExamServer/ExamChatServerLib/src/Share/PacketCmd.pb.h"

using namespace examshare;
using namespace ::examchatserver::logic;

void 
PacketLobbyLogicTest::SetUp() {
	logic_ = std::make_shared<packetprocess::LobbyLogic>();
	testuser_sn = 10000;

	// 테스트용 유저 추가 작업
	auto puser = std::make_shared<User>();
	if (nullptr != puser) {
		puser->SetUserId("LogicTest01");
		puser->SetUserSn(testuser_sn);
		puser->SetFrontServerIndex(1);
		EXAMCHAT_CONST_INSTANCE().GetUserManager()->AddUser(testuser_sn, puser);
	}
}

TEST_F(PacketLobbyLogicTest, LobbyMsgReq) {

	// 테스트 유저가 있는지 확인
	ASSERT_EQ(EXAMCHAT_CONST_INSTANCE().GetUserManager()->UserCount(), 1);

	// 테스트 패킷을 만들어서 전송
	C2CS_LobbySendMsgReq req;
	req.set_usersn(testuser_sn);
	req.set_msg("hello world");
	EXPECT_EQ(logic_->LobbyMsgReqProcessTest(C2SLobbyMsgReq, req), ErrCode::ResultSuccess);
}