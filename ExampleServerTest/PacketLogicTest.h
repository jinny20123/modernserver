#pragma once
#include "gtest/gtest.h"


namespace examchatserver { namespace logic { namespace packetprocess { 
	class LobbyLogic;
}}}

class PacketLobbyLogicTest : public ::testing::Test
{
protected:
	void SetUp() override;

	std::shared_ptr<::examchatserver::logic::packetprocess::LobbyLogic> logic_;
	uint64_t testuser_sn;
};

