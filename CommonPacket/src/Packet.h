﻿#pragma once
#include "PacketDefine.h"
#include <type_traits>

namespace packet {
#pragma pack(push, 1)
	struct PacketHeader
	{
		packet_serialkeytype	serialkey_;
		packet_sizetype			size_;
		packet_commandtype		command_;

		packet_serialkeytype GetSerialkey() { return serialkey_; }
		void SetSerialkey(const packet_serialkeytype& key) { serialkey_ = key; }

		packet_sizetype GetSize() { return size_; }
		void SetSize(const packet_sizetype& size) { size_ = size; }

		packet_commandtype GetCommand() { return command_; }
		void SetCommand(const packet_commandtype& command) { command_ = command; }

		static constexpr size_t GetLeastSize() { return sizeof(PacketHeader); }

		packet_sizetype GetTotalSize() { return size_ + sizeof(PacketHeader); }


	};

	struct Packet : public PacketHeader
	{
		char* BodyBuffer() { return reinterpret_cast<char*>(this) + sizeof(PacketHeader); }
	};
#pragma pack(pop)
	struct PacketBuffer
	{
		unsigned char* buffer_[MAXBUFFERSIZE];


		template<typename T>
		decltype(auto) CastPacket()
		{
			// 포인터 타입만 전달 받습니다.
			static_assert(std::is_pointer_v <T>, "PacketBuffer::CastPacket Not Pointer Type");
			return reinterpret_cast<T>(this);
		}

		Packet* GetPacket()
		{
			return CastPacket<Packet*>();
		}
	};
} /// packet

#define PACKETDEFINE(packetname, code) \
constexpr ::packet::packet_commandtype  packetname = code;
