﻿#pragma once

#include "../../../Utility/src/Factory/Container/factory_container.h"
#include <boost/serialization/singleton.hpp>

namespace utility {
	namespace object {
		struct Argument;
	}
}

namespace packet {
	struct Packet;
	struct PacketBuffer;
}

namespace packet {
	namespace factory {
		/**
			@class : PacketBufferFactory
			@date : 2015/04/30
			@brief : 패킷 버퍼 객체를 factory 함수를 통해서 할당 및 반환합니다.
			@author : jinny20123(안 정웅)
		*/
		class PacketBufferFactory
			: private boost::serialization::singleton<PacketBufferFactory>
		{
		public:
			using Argument = ::utility::object::Argument;
			using PacketBuffer = ::packet::PacketBuffer;

			enum PacketKey : int
			{
				PACKETBUFFER = 0,
			};

			using Interface = PacketBuffer;
			using FactoryContainer = ::utility::factory::container::FactoryContainer<Interface, int>;

			PacketBufferFactory();
			virtual ~PacketBufferFactory();

			static PacketBufferFactory& GetInstance();

			Interface* Create(int key);

			void Destroy(int key, Interface* pobj);

			template<typename T>
			void CreateT(int key, T* pObj)
			{
				pObj = static_cast<T*>(Create(key));
			}
		private:
			struct Impl;
			std::shared_ptr<Impl> impl_;
		};
	}
} /// packet::factory

#define PACKEBUFFERFACTORY() ::packet::factory::PacketBufferFactory::GetInstance()