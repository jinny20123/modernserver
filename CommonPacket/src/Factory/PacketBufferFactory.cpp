﻿#include "PacketBufferFactory.h"
#include "../Packet.h"
#include <string>

namespace packet {
	namespace factory {
		/// 내부에서 사용할 변수를 정의합니다.
		/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
		struct PacketBufferFactory::Impl
		{
			std::shared_ptr<FactoryContainer> pFContainer;
		};

		PacketBufferFactory::PacketBufferFactory()
		{
			impl_ = std::make_shared<Impl>();
			impl_->pFContainer = std::make_shared<FactoryContainer>();

			KEY_FACTORY_SET(impl_->pFContainer, PACKETBUFFER, packet::PacketBuffer);
		}
		PacketBufferFactory::~PacketBufferFactory()
		{
		}

		PacketBufferFactory::Interface*
			PacketBufferFactory::Create(int key)
		{
			FactoryContainer::FactoryInterface* pfactory = impl_->pFContainer->Get(key);
			if (nullptr == pfactory) { return nullptr; }
			auto pobj = pfactory->Create();
			return pobj;
		}

		void
			PacketBufferFactory::Destroy(int key, Interface* pObj)
		{
			impl_->pFContainer->Get(key)->Destroy(pObj);
		}

		PacketBufferFactory&
			PacketBufferFactory::GetInstance()
		{
			return PacketBufferFactory::get_mutable_instance();
		}
	}
} /// packet::factory