﻿#include "PacketBuilder.h"
#include "Factory/PacketBufferFactory.h"

namespace packet {
	PacketBuffer*
		PacketBuilder::Create()
	{
		return PACKEBUFFERFACTORY().Create(factory::PacketBufferFactory::PACKETBUFFER);
	}

	void
		PacketBuilder::Destroy(PacketBuffer* pbuffer)
	{
		PACKEBUFFERFACTORY().Destroy(factory::PacketBufferFactory::PACKETBUFFER, pbuffer);
	}
} /// packet