﻿#pragma once
#include "Packet.h"
#include <cassert>
#include <type_traits>

namespace packet {
	class PacketBuilder
	{
	public:
		template<typename T>
		static T Build()
		{
			using TArg = typename std::remove_pointer_t<T>;
#ifdef _DEBUG
			assert(sizeof(TArg) < sizeof(PacketBuffer));
#endif
			PacketBuffer* pbuffer = Create();
			T* pt = pbuffer->CastPacket<T>();
			pt->SetCommand(TArg::s_nCode);
			pt->SetSize(sizeof(TArg));
			return pt;
		}

		/// Packet에 대한 Template 특수화
		template<>
		static Packet* Build<Packet*>()
		{
			PacketBuffer* pbuffer = Create();
			return pbuffer->GetPacket();
		}

		template<typename T>
		static void Destroy(T* pbuffer)
		{
			Destroy(reinterpret_cast<PacketBuffer*>(pbuffer));
		}
	private:
		static PacketBuffer* Create();
		static void Destroy(PacketBuffer* pbuffer);
	};
} /// packet

#define PACKET_BUILDER ::packet::PacketBuilder
