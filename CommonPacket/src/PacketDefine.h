﻿#pragma once
#include <cstdint>

namespace packet {
	using packet_serialkeytype = uint32_t;
	using packet_sizetype = uint16_t;
	using packet_commandtype = uint32_t;
	

	constexpr packet_sizetype MAXBUFFERSIZE = 4096 + 256;
} /// packet
