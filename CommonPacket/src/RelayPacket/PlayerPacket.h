﻿#pragma once
#include "../Packet.h"

namespace packet {
	namespace player {
		struct LogicPacket :
			public ::packet::Packet
		{
			static const packet_commandtype PLAYER_INDEX = 1000;
		};

		struct UniqueKeyPacket
			: public LogicPacket
		{
			typedef unsigned __int64
				Tokenkey;
			typedef unsigned int
				PlayerKey;

			Tokenkey	room_token_;
			PlayerKey	player_key_;
		};

		struct EndpointPacket
			: public UniqueKeyPacket
		{
			static const size_t STR_ADDRESS_SIZE = 20;
			unsigned __int16 port;
			char address[STR_ADDRESS_SIZE];

			void InitBuffer()
			{
				memset(address, 0x00, sizeof(address));
			}
		};

		struct RelayToEndPointPacket
			: public EndpointPacket
		{
			const static size_t BUFFERSIZE = 512;
			char		buffer_[BUFFERSIZE];

			void InitBuffer()
			{
				EndpointPacket::InitBuffer();
				memset(buffer_, 0x00, sizeof(buffer_));
			}
		};

#define PACKETDEFINE(packetname, basename, code)			\
struct packetname											\
: public basename											\
{															\
	const static packet_commandtype s_nCode = code;				\
}

		// 패킷 통신에 사용할 패킷 목록
		PACKETDEFINE(RequestPing, LogicPacket, PLAYER_INDEX + 1);
		PACKETDEFINE(ResponsePing, LogicPacket, PLAYER_INDEX + 2);
		PACKETDEFINE(RequestEndPoint, UniqueKeyPacket, PLAYER_INDEX + 3);
		PACKETDEFINE(ResponseEndPoint, EndpointPacket, PLAYER_INDEX + 4);
	}
} /// packet::player
