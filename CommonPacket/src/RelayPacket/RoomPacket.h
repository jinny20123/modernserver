﻿#pragma once
#include <memory.h>
#include "../Packet.h"

namespace packet {
	namespace room {
		struct LogicPacket :
			public ::packet::Packet

		{
			// Command List
			static const packet_commandtype ROOM_INDEX = 0;
		};

		struct RoomType
		{
			typedef __int64
				Tokenkey;
			typedef unsigned int
				PlayerKey;
			typedef unsigned int
				error_code;

			static const unsigned int SUCCESS = 0;
			static const unsigned int NONE = 5;
			static const unsigned int NOTENABLE_TOKEN = 10;
			static const unsigned int NOTCREATE_CONNECTOR = 11;
			static const unsigned int INSERT_ERROR = 12;
			static const unsigned int ERASE_ERROR = 13;
			static const unsigned int ALREADY_PLAYER = 14;
		};

		struct RelayPacket
			: public LogicPacket, public RoomType
		{
			const static size_t BUFFERSIZE = 4096;

			static const packet_commandtype TO_SESSION = 0;
			static const packet_commandtype TO_ROOMALL = 1;

			void DataCopy(RelayPacket* packet)
			{
				this->SetSize(packet->GetSize());
				this->sub_type_ = packet->sub_type_;
				this->room_token_ = packet->room_token_;
				this->from_player_key_ = packet->from_player_key_;
				this->to_player_key_ = packet->to_player_key_;
				memcpy(this->buffer_, packet->buffer_, sizeof(this->buffer_));
			}

			void SetUseBufferSize(unsigned short  buffer_size)
			{
				SetSize(buffer_size + sizeof(RelayPacket) - BUFFERSIZE);
			}

			unsigned short GetUseBufferSize()
			{
				return GetSize() - (sizeof(RelayPacket) - BUFFERSIZE);
			}

			packet_commandtype	sub_type_;
			Tokenkey	room_token_;
			PlayerKey	from_player_key_;				// 패킷을 전송하는 Player index
			PlayerKey	to_player_key_;					// 패킷을 수신하는 Player index
			char		buffer_[BUFFERSIZE];

			void InitBuffer() { memset(buffer_, 0x00, sizeof(buffer_)); }
		};

		struct RoomPacket
			: public LogicPacket, public RoomType
		{
			Tokenkey	room_token_;
			PlayerKey	player_key_;
		};

		struct RoomPacketResponse
			: RoomPacket
		{
			RoomPacketResponse()
			{
				er_code = NONE;
			}

			RoomPacketResponse(RoomPacket& r_packet) : RoomPacket(r_packet) { er_code = NONE; }

			error_code er_code;
		};

#define PACKETDEFINE(packetname, basename, code)			\
struct packetname											\
	: public basename										\
{															\
	const static packet_commandtype s_nCode = code;				\
}

		// 패킷 통신에 사용할 패킷 목록
		PACKETDEFINE(RequestRoomRelay, RelayPacket, ROOM_INDEX + 1);
		PACKETDEFINE(ResponseRoomRelay, RelayPacket, ROOM_INDEX + 2);
		PACKETDEFINE(RequestEnterRoom, RoomPacket, ROOM_INDEX + 3);
		PACKETDEFINE(ResponseEnterRoom, RoomPacketResponse, ROOM_INDEX + 4);
		PACKETDEFINE(RequestLeaveRoom, RoomPacket, ROOM_INDEX + 5);
		PACKETDEFINE(ResponseLeaveRoom, RoomPacketResponse, ROOM_INDEX + 6);
		PACKETDEFINE(RequestAliveRoom, RoomPacket, ROOM_INDEX + 7);
	}
} /// packet::room
