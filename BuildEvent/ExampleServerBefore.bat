set CURRENT_PATH=%~dp0
set TARGET_DEBUG_PATH=%CURRENT_PATH%..\OutDir\Debug
set TARGET_RELEASE_PATH=%CURRENT_PATH%..\OutDir\Release

:: Debug 파일 삭제
del /q /s "%CURRENT_PATH%..\x64\Debug\*.exe"
del /q /s "%CURRENT_PATH%..\x64\Debug\*.pdb"

:: Debug 파일 삭제
IF %TARGET_DEBUG_PATH% neq "" (
del /q /s "%TARGET_DEBUG_PATH%\*.exe"
del /q /s "%TARGET_DEBUG_PATH%\*.dll"
del /q /s "%TARGET_DEBUG_PATH%\*.pdb"
)

:: Release 파일 삭제
del /q /s "%CURRENT_PATH%..\x64\Release\*.exe"
del /q /s "%CURRENT_PATH%..\x64\Release\*.pdb"

:: Release 파일 삭제 
IF %TARGET_RELEASE_PATH% neq "" (
del /q /s "%TARGET_RELEASE_PATH%\*.exe"
del /q /s "%TARGET_RELEASE_PATH%\*.dll"
del /q /s "%TARGET_RELEASE_PATH%\*.pdb"
)