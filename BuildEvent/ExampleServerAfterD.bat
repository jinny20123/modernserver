set CURRENT_PATH=%~dp0
set TARGET_DEBUG_PATH=%CURRENT_PATH%..\OutDir\Debug

:: Debug 파일 복사
xcopy /y /I %CURRENT_PATH%..\x64\Debug\*.exe %TARGET_DEBUG_PATH%
xcopy /y /I %CURRENT_PATH%..\x64\Debug\*.dll %TARGET_DEBUG_PATH%
xcopy /y /I %CURRENT_PATH%..\x64\Debug\*.pdb %TARGET_DEBUG_PATH%
