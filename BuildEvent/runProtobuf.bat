:: 현재 경로 지정
set CURRENT_PATH=%~dp0
set TARGET_PATH=%CURRENT_PATH%..\ExamServer\ExamChatServerLib\src\Share\

:: proto 파일 컴파일 
protoc -I%TARGET_PATH%Proto/ --cpp_out=%TARGET_PATH% %TARGET_PATH%Proto/*.proto
pause()