set CURRENT_PATH=%~dp0
set TARGET_DEBUG_PATH=%CURRENT_PATH%..\OutDir\Debug
set TARGET_RELEASE_PATH=%CURRENT_PATH%..\OutDir\Release


:: Release 파일 복사
xcopy /y /I %CURRENT_PATH%..\x64\Release\*.exe %TARGET_RELEASE_PATH%
xcopy /y /I %CURRENT_PATH%..\x64\Release\*.dll %TARGET_RELEASE_PATH%
xcopy /y /I %CURRENT_PATH%..\x64\Release\*.pdb %TARGET_RELEASE_PATH%