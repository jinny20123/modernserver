라이브러리를 압축 파일을 해제 하는 방식에 대해서 설명 드립니다. 
라이브러리는 해당 압축파일명의 폴더를 안에다 풉니다. 

예)
boost_1_72.zip => boost_1_72폴더에 압축 풀기 =>  boost_1_72/include, boost_1_72/lib 형식
protobuf.zip => protobuf폴더에 압축풀기 => protobuf/include, protobuf/lib 형식