﻿#include <string>
#include <memory>
#include <thread>
#include <iostream>
#include <boost/exception_ptr.hpp>
#include "Logic/Construct/Construct.h"
#include "Log/src/Common/CommonLogger.h"

int main(int argc, char ** argv)
{

	typedef ::examchatserver::logic::construct::object::ExamChatConstruct
		ExamChatConstruct;

	// 로거를 위한 초기화 작업 
	COMMONLOG().Prepare();		
	
	// 프로그램 시작전에 수행할 작업을 등록하는 Construct 객체 생성
	std::unique_ptr<::examchatserver::logic::construct::object::ExamChatConstruct> exam_construct;
	MAKE_UNIQUE(exam_construct, ExamChatConstruct);

	try
	{
		exam_construct->SetUp();
		exam_construct->Run();

		while (1)
		{
			std::this_thread::sleep_for(std::chrono::minutes(60));
			std::cout << "현재 서버 상태 출력" << std::endl;
		}
	}
	catch (boost::exception & e)
	{
		COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "%s", diagnostic_information(e).c_str());
		throw;
	}
	catch (std::exception & e)
	{
		COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "%s", e.what());
		throw;
	}
	catch (...)
	{
		COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "Not Catch");
		throw;
	}

	return 0;
}