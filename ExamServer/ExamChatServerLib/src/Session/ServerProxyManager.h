#pragma once
#include <memory>
#include <functional>
#include "ServerProxy.h"


namespace examchatserver {
namespace session {

constexpr uint32_t kFrontServerStartIndex = 1000;
class ServerProxy;

class ServerProxyManager
{
public:
	using ProxyRunFunc = std::function<void(ServerProxy*)>;
	ServerProxyManager() noexcept;
	virtual ~ServerProxyManager();

	bool AddServerProxy(ServerProxy::ServerType server_type, ::network::session::SessionBase * session, uint32_t & added_index);
	bool DelServerProxy(ServerProxy::ServerIndex server_idx);
	bool FindProxyRun(ServerProxy::ServerIndex server_idx, ProxyRunFunc run_fun);
	void AllProxyRun(ProxyRunFunc run_fun);
private:
	struct Impl;
	std::unique_ptr<Impl> mimpl_;
};

}} // examchatserver::session

