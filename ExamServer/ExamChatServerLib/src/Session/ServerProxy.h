#pragma once
#include <memory>
#include "Utility/src/Object/argument_fwd.h"
#include "Network/src/Session/Session_fwd.h"

namespace network {
namespace session {
namespace logic {
	struct SessionLogicSendArgument;
}}}


namespace examchatserver {
namespace session {

class ServerProxy {
public:
	using ServerType = uint32_t;
	using ServerIndex = uint32_t;

	ServerProxy() noexcept;
	virtual ~ServerProxy();

	void Initialize(ServerType server_type, ServerIndex server_idx, ::network::session::SessionBase * session);

	uint32_t GetServerType() noexcept;
	uint32_t GetServerIndex() noexcept;
	void Send(char* buffer, size_t buffer_size);
	void Send(::network::session::logic::SessionLogicSendArgument& arg);
protected:
private :	
	struct Impl;
	std::unique_ptr<Impl> mimpl_;
};

}} // examchatserver::session