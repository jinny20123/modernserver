#include "Utility/src/macro.h"
#include "ServerProxyManager.h"
#include "Network/src/Session/Session.h"
#include <unordered_map>
#include <atomic>
#include <shared_mutex>

namespace examchatserver {
namespace session {

using namespace ::network::session;

struct ServerProxyManager::Impl
{
	using SvrProxyCon = std::unordered_map<ServerProxy::ServerIndex, std::unique_ptr<ServerProxy>>;
	Impl(){
	}

	std::atomic<uint32_t>	front_server_indexer;
	SvrProxyCon				proxy_con_;
	std::shared_mutex		sm_proxycon_;
};

ServerProxyManager::ServerProxyManager() noexcept {
	MAKE_UNIQUE(mimpl_, Impl);
	mimpl_->front_server_indexer.store(kFrontServerStartIndex);
}

ServerProxyManager::~ServerProxyManager() {
}

bool 
ServerProxyManager::AddServerProxy(ServerProxy::ServerType server_type, ::network::session::SessionBase* session, uint32_t & added_index ){
	auto server_proxy = std::make_unique<ServerProxy>();
	server_proxy->Initialize(server_type, mimpl_->front_server_indexer.fetch_add(1), session);
	added_index = server_proxy->GetServerIndex();

	bool ret = false;
	{
		std::unique_lock<std::shared_mutex> ul(mimpl_->sm_proxycon_);
		auto itr = mimpl_->proxy_con_.insert(Impl::SvrProxyCon::value_type(server_proxy->GetServerIndex(), std::move(server_proxy)));
		ret = itr.second;
	}
	return ret;
}

bool 
ServerProxyManager::DelServerProxy(ServerProxy::ServerIndex server_idx) {
	std::unique_lock<std::shared_mutex> ul(mimpl_->sm_proxycon_);
	return mimpl_->proxy_con_.erase(server_idx) != 0 ? true : false;
}

bool 
ServerProxyManager::FindProxyRun(ServerProxy::ServerIndex server_idx, ProxyRunFunc run_fun){
	std::shared_lock<std::shared_mutex> ul(mimpl_->sm_proxycon_);
	auto itr = mimpl_->proxy_con_.find(server_idx);
	if (itr != mimpl_->proxy_con_.end()) {
		run_fun(itr->second.get());
		return true;
	}
	return false;
}

void 
ServerProxyManager::AllProxyRun(ProxyRunFunc run_fun) {
	std::shared_lock<std::shared_mutex> ul(mimpl_->sm_proxycon_);
	for (auto& itr : mimpl_->proxy_con_) {
		run_fun(itr.second.get());
	}
}

}} // examchatserver::session
