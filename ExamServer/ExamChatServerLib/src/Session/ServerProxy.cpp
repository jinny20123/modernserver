#include "ServerProxy.h"
#include "Utility/src/macro.h"
#include "Network/src/Session/Session.h"
#include "Network/src/Session/Logic/SessionLogicArgument.h"

namespace examchatserver {
namespace session {

using namespace ::network::session::logic;
		
struct ServerProxy::Impl
{
	Impl() : server_type_(0), server_idx_(0) {

	}
	ServerProxy::ServerType		server_type_;
	ServerProxy::ServerIndex	server_idx_;
	ISession *					psession_;
};

ServerProxy::ServerProxy() noexcept {
	MAKE_UNIQUE(mimpl_, Impl);
}

ServerProxy::~ServerProxy() {

}

void 
ServerProxy::Initialize(ServerType server_type, ServerIndex server_idx, ISession * session) {
	mimpl_->server_type_ = server_type;
	mimpl_->server_idx_ = server_idx;
	mimpl_->psession_ = session;
}

ServerProxy::ServerType
ServerProxy::GetServerType() noexcept {
	return mimpl_->server_type_;
}
ServerProxy::ServerIndex
ServerProxy::GetServerIndex() noexcept {
	return mimpl_->server_idx_;
}

void
ServerProxy::Send(char * buffer, size_t buffer_size) {
	if(buffer && mimpl_->psession_)
	{
		SessionLogicSendArgument sls_arg(buffer, buffer_size);
		Send(sls_arg);
	}
}

void 
ServerProxy::Send(SessionLogicSendArgument& arg)
{
	if (mimpl_->psession_)
	{
		mimpl_->psession_->Send(&arg);
	}
	
}

}} // examchatserver::session
