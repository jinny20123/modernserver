﻿#pragma once
#include <memory>
#include <boost/serialization/singleton.hpp>

namespace examchatserver {
namespace session {
	class ServerProxyManager;
}

namespace logic {
	class UserManager;
}

}

namespace examchatserver {
namespace Singleton {

/**
	@class : ServerInstance
	@date : 2020-05-06
	@brief : ServerInstance 객체는 singleton 객체이기 때문에 프로세스와 생명주기를 같이 합니다.
	@author : jinny20123(안 정웅)
*/
class ServerInstance : public boost::serialization::singleton<ServerInstance>
{
public :
	ServerInstance() noexcept;
	virtual ~ServerInstance();

	std::shared_ptr<session::ServerProxyManager> & GetServerProxyManager() const noexcept;
	std::shared_ptr<logic::UserManager> & GetUserManager() const noexcept;
private:
	struct Impl;
	std::unique_ptr<Impl> mimpl_;
};

}} // examchatserver::Singleton

#define EXAMCHAT_INSTANCE() examchatserver::Singleton::ServerInstance::get_instance()
#define EXAMCHAT_CONST_INSTANCE() examchatserver::Singleton::ServerInstance::get_const_instance()