﻿#include "ServerInstance.h"
#include "Utility/src/macro.h"
#include "src/Session/ServerProxyManager.h"
#include "src/Logic/User/UserManager.h"

namespace examchatserver {
namespace Singleton {

struct ServerInstance::Impl
{
	Impl() {
	}

	std::shared_ptr<session::ServerProxyManager>	server_proxy_manager_;
	std::shared_ptr<logic::UserManager>				user_manager_;
};

ServerInstance::ServerInstance()noexcept {
	MAKE_UNIQUE(mimpl_, Impl);
	mimpl_->server_proxy_manager_ = std::make_shared<session::ServerProxyManager>();
	mimpl_->user_manager_ = std::make_shared<logic::UserManager>();
}

ServerInstance::~ServerInstance() {
}

std::shared_ptr<session::ServerProxyManager> &
ServerInstance::GetServerProxyManager() const noexcept {
	return mimpl_->server_proxy_manager_;
}

std::shared_ptr<logic::UserManager>&
ServerInstance::GetUserManager() const noexcept {
	return mimpl_->user_manager_;
}

}} // examchatserver::Singleton


