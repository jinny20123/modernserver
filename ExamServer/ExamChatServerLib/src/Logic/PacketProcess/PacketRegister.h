﻿#pragma once
#include "TCPServer/Logic/PacketProcess/IPacketRegister.h"
#include "Utility/src/macro.h"
#include <memory>


	
namespace examchatserver { namespace logic { namespace packetprocess { 


/** 
	@class : PacketRegister 
	@date : 2020-04-21
	@brief : 서버에서 사용하는 패킷 프로세스 등록작업을 진행합니다. 
	@author : jinny20123(안 정웅)
*/
class PacketRegister 
	: public ::tcpserver::logic::packetprocess::IPacketRegister
{
public: 
	typedef ::utility::object::Argument
			Argument;

	PacketRegister();
	virtual ~PacketRegister();

	virtual PacketLogicFuncContainer & GetRecvProcessCon() OVERRIDE;
	virtual void Prepare() OVERRIDE;								///< PacketProcess를 등록 합니다. 
	virtual const unsigned int GetMaxCommand() const OVERRIDE;			///< 등록된 패킷 중 가장 높은 패킷 커맨드를 리턴합니다. 
private :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};
	
}}} /// examchatserver::logic::packetprocess