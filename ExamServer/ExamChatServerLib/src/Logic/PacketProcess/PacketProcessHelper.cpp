﻿#include "PacketProcessHelper.h"
#include "Utility/src/Object/argument.h"
#include "TCPServer/Network/Handler/LogicHandlerArgument.h"
#include "Network/src/Session/Logic/SessionLogicArgument.h"
#include "Network/src/Session/Session.h"
#include "CommonPacket/src/Packet.h"
#include "CommonPacket/src/PacketDefine.h"
#include "CommonPacket/src/PacketBuilder.h"
#include "src/Share/Common.pb.h"
#include "src/Share/PacketCmd.pb.h"
#include "src/Singleton/ServerInstance.h"
#include "src/Session/ServerProxyManager.h"
#include "src/Session/ServerProxy.h"
#include <google/protobuf/message_lite.h>


using ::tcpserver::network::handler::PacketHandlerArgument;
using  ::network::session::logic::SessionLogicSendArgument;
using namespace examshare;

namespace examchatserver { 
namespace logic { 
namespace packetprocess { 

bool PacketProcessHelper::PacketParserFromArg(::google::protobuf::MessageLite& msg, ::utility::object::Argument* arg, PacketHandlerArgument *& packet_argument) {
	::utility::object::ArgumentUtil::CastArgument(arg, packet_argument);
	if (nullptr == packet_argument) {
		return false;
	}

	if (true == msg.ParseFromArray(packet_argument->ppacket_->BodyBuffer(), packet_argument->ppacket_->GetSize())) {
		return true;
	}
	return false;
}

// To do : buffer pool 로 변경 작업 필요
bool PacketProcessHelper::PacketSerializetoArg(::google::protobuf::MessageLite& msg, ::packet::Packet& packet, SessionLogicSendArgument & sls_arg) {
	auto buf_size = static_cast<int>(msg.ByteSizeLong());
	packet.SetSerialkey(ExamplePacketSerialkey);
	packet.SetSize(buf_size);

	if (true == msg.SerializeToArray(packet.BodyBuffer(), buf_size)){
		sls_arg.buffer_ = reinterpret_cast<char*>(&packet);
		sls_arg.buffer_size_ = packet.GetTotalSize();
		return true;
	}

	return false;
}

//
//struct PacketTestHelper::Impl {
//	PacketTestHelper::TestLogicCon TestProcessLogics;
//};
//bool PacketTestHelper::Insert(int cmd, const PacketTestHelper::TestLogicCon::value_type& value) {
//
//}

bool
SendToUser::SendPacket(::packet::Packet& packet, ::google::protobuf::MessageLite& res, network::session::SessionBase* psession) {
	SessionLogicSendArgument sls_arg;
	if (PacketProcessHelper::PacketSerializetoArg(res, packet, sls_arg)) {
		if (psession) {
			psession->Send(&sls_arg);
			return true;
		}
	}
	// [res 패킷 serialize 작업 및 패킷 전송 End]
	return false;
}

bool 
SendToUser::SendPacket(::packet::Packet& packet, ::google::protobuf::MessageLite& res, unsigned int svr_idx) {
	SessionLogicSendArgument sls_arg;
	if (PacketProcessHelper::PacketSerializetoArg(res, packet, sls_arg)) {
		EXAMCHAT_CONST_INSTANCE().GetServerProxyManager()->FindProxyRun(svr_idx, [&sls_arg](session::ServerProxy* proxy) {
			proxy->Send(sls_arg);
			});
		return true;
	}
	// [res 패킷 serialize 작업 및 패킷 전송 End]
	return false;
}

bool 
SendToAllFront::SendPacket(::packet::Packet& packet, ::google::protobuf::MessageLite& res) {
	// [res 패킷 serialize 작업 및 패킷 전송]
	//auto p = PACKET_BUILDER::Build<::packet::Packet*>();
	//p->SetCommand(cmd);
	SessionLogicSendArgument sls_arg;

	if (PacketProcessHelper::PacketSerializetoArg(res, packet, sls_arg)) {
		// 로비에 있는 유저에게 보내야 하기 때문에 모든 Front서버에 전송합니다.
		EXAMCHAT_CONST_INSTANCE().GetServerProxyManager()->AllProxyRun([&sls_arg](session::ServerProxy* proxy) {
			proxy->Send(sls_arg);
			});
		return true;
	}
	// [res 패킷 serialize 작업 및 패킷 전송 End]
	return false;
}
	
}}} /// examchatserver::logic::packetprocess