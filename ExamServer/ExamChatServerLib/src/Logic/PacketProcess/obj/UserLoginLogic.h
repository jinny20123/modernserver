﻿#pragma once

#include <memory>
#include "Utility/src/Object/argument_fwd.h"

namespace examchatserver { namespace logic { namespace packetprocess { 


class UserLoginLogic 
{
public: 
	typedef ::utility::object::Argument
			Argument;

	UserLoginLogic();
	virtual ~UserLoginLogic();
	
	/// IPacketProcess::ProcessPacketLogicFun 형식의 패킷 처리 로직을 등록합니다.
	bool LoginUserReqProcessPacket(Argument * arg);
	bool LogOutUserReqProcessPacket(Argument* arg);

private :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};
	
}}} /// pokerserver::logic::packetprocess