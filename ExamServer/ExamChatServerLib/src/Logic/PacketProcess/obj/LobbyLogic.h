﻿#pragma once

#include <memory>
#include "Utility/src/Object/argument_fwd.h"

namespace google { 
namespace protobuf {
class MessageLite;
}}

namespace examchatserver { namespace logic { namespace packetprocess { 


class LobbyLogic 
{
public: 
	typedef ::utility::object::Argument
			Argument;

	LobbyLogic();
	virtual ~LobbyLogic();
	
	/// IPacketProcess::ProcessPacketLogicFun 형식의 패킷 처리 로직을 등록합니다.
	bool LobbyMsgReqProcessPacket(Argument* arg);
	int LobbyMsgReqProcessTest(int cmd, ::google::protobuf::MessageLite& arg);

private :
	struct Impl;
	std::unique_ptr<Impl> mimpl_;
};
	
}}} /// pokerserver::logic::packetprocess