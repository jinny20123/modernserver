﻿#pragma once

#include <memory>
#include "Utility/src/Object/argument_fwd.h"

namespace examchatserver { namespace logic { namespace packetprocess { 


class SystemLogic
{
public: 
	typedef ::utility::object::Argument
			Argument;

	SystemLogic();
	virtual ~SystemLogic();
	
	/// IPacketProcess::ProcessPacketLogicFun 형식의 패킷 처리 로직을 등록합니다.
	bool SysServerRegistReqProcessPacket(Argument* arg);
	bool SysServerUnregistReqProcessPacket(Argument* arg);
private :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};
	
}}} /// pokerserver::logic::packetprocess