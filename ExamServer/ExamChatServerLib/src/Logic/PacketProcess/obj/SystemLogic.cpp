﻿
#include "SystemLogic.h"
#include "Utility/src/macro.h"
#include "TCPServer/Network/Handler/LogicHandlerArgument.h"
#include "Network/src/Session/Session.h"
#include "Network/src/Session/Logic/SessionLogicArgument.h"
#include "CommonPacket/src/Packet.h"
#include "CommonPacket/src/PacketBuilder.h"
#include "src/Share/LogicPacket.pb.h"
#include "src/Share/PacketCmd.pb.h"
#include "src/Singleton/ServerInstance.h"
#include "src/Session/ServerProxyManager.h"
#include "src/Logic/PacketProcess/PacketProcessHelper.h"

namespace examchatserver { 
namespace logic { 
namespace packetprocess { 

using ::tcpserver::network::handler::PacketHandlerArgument;
using namespace examshare;
using namespace ::network::session::logic;

/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct SystemLogic::Impl
{
	Impl() {}
}; 

SystemLogic::SystemLogic()
{
	MAKE_UNIQUE(mImpl, Impl);
}
SystemLogic::~SystemLogic()
{
	
}

// 연결된 서버를 등록합니다. 
bool
SystemLogic::SysServerRegistReqProcessPacket(Argument* arg) {

	// request 패킷 Parser 작업 
	PacketHandlerArgument* packet_argument = nullptr;
	F2CS_ServerRegistReq req;
	if (false == PacketProcessHelper::PacketParserFromArg(req, arg, packet_argument) || packet_argument == nullptr) {
		return false;
	}
	// request 패킷 Parser 작업 End

	// [Logic Start]
	CS2F_ServerRegistRes res;

	auto & proxymanager = EXAMCHAT_CONST_INSTANCE().GetServerProxyManager();
	uint32_t added_index{ 0 };
	
	if (true == proxymanager->AddServerProxy(static_cast<uint32_t>(req.servertype()), packet_argument->psession_, added_index)) {
		res.set_result(ErrCode::ResultSuccess);
		res.set_issueserverindex(added_index);
	}
	else {
		res.set_result(ErrCode::ResultFail);
	}
	// [Logic End]

	// res 패킷 serialize 작업 및 패킷 전송
	auto p = PACKET_BUILDER::Build<::packet::Packet*>();
	p->SetCommand(CS2FServerRegistRes);
	return SendToUser::SendPacket(*p, res, packet_argument->psession_);
}

bool
SystemLogic::SysServerUnregistReqProcessPacket(Argument* arg) {
	return true;
}

}}} /// pokerserver::logic::packetprocess::player