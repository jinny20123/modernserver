﻿#pragma once

#include <memory>
#include "Utility/src/Object/argument_fwd.h"

namespace examchatserver { namespace logic { namespace packetprocess { 

class ChatRoomLogic
{
public: 
	typedef ::utility::object::Argument
			Argument;

	ChatRoomLogic();
	virtual ~ChatRoomLogic();
	
	/// IPacketProcess::ProcessPacketLogicFun 형식의 패킷 처리 로직을 등록합니다.


private :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};
	
}}} /// pokerserver::logic::packetprocess