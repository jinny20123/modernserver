﻿
#include "ChatRoomLogic.h"
#include "Utility/src/macro.h"
#include "TCPServer/Network/Handler/LogicHandlerArgument.h"
#include "src/Share/PacketCmd.pb.h"


namespace examchatserver { 
namespace logic { 
namespace packetprocess { 

	using namespace examshare;

/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct ChatRoomLogic::Impl
{
	Impl() {}
}; 

ChatRoomLogic::ChatRoomLogic()
{
	MAKE_UNIQUE(mImpl, Impl);
}
ChatRoomLogic::~ChatRoomLogic()
{
	
}

}}} /// pokerserver::logic::packetprocess::player