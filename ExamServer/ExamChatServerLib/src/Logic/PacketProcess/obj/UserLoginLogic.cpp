﻿
#include "UserLoginLogic.h"
#include "Utility/src/macro.h"
#include "TCPServer/Network/Handler/LogicHandlerArgument.h"
#include "Network/src/Session/Session.h"
#include "Network/src/Session/Logic/SessionLogicArgument.h"
#include "CommonPacket/src/Packet.h"
#include "CommonPacket/src/PacketBuilder.h"
#include "src/Share/LogicPacket.pb.h"
#include "src/Share/PacketCmd.pb.h"
#include "src/Singleton/ServerInstance.h"
#include "src/Logic/User/UserManager.h"
#include "src/Logic/User/User.h"
#include "src/Logic/PacketProcess/PacketProcessHelper.h"

namespace examchatserver { 
namespace logic {
namespace packetprocess { 

using ::tcpserver::network::handler::PacketHandlerArgument;
using namespace examshare;
using namespace ::network::session::logic;

/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct UserLoginLogic::Impl
{
	Impl() {}
}; 

UserLoginLogic::UserLoginLogic()
{
	MAKE_UNIQUE(mImpl, Impl);
}
UserLoginLogic::~UserLoginLogic(){
	
}


// LoginUserReqProcessPacket는 유저의 로그인 처리 로직을 추가합니다. 
bool 
UserLoginLogic::LoginUserReqProcessPacket(Argument * arg){

	// [request 패킷 Parser 작업]
	PacketHandlerArgument* packet_argument = nullptr;
	C2CS_LoginUserReq req;
	if (false == PacketProcessHelper::PacketParserFromArg(req, arg, packet_argument) || packet_argument == nullptr) {
		return false;
	}
	// [request 패킷 Parser 작업 End]

	// [Logic Start]
	CS2C_LoginUserRes res;
	res.set_result(ErrCode::ResultFail);
	res.set_issuedsessionsn(req.issuedsessionsn());

	do {		
		// 유저 생성 작업
		// To do : UserPool 생성 작업 필요
		auto& usermanager = EXAMCHAT_CONST_INSTANCE().GetUserManager();
		if (!usermanager){
			break;
		}

		// 이미 존재하는 user id인지 체크
		if (true == usermanager->FindUserName(req.userid())) {
			res.set_result(ErrCode::ResultExistUserID);
			break;
		}

		// 유저 생성 
		auto puser = std::make_shared<::examchatserver::logic::User>();
		if (nullptr == puser) {
			break;
		}

		// 유저 정보 등록
		puser->SetUserId(req.userid());
		auto issued_usersn = usermanager->IssueUserSn();
		puser->SetUserSn(issued_usersn);
		puser->SetFrontServerIndex(req.cidx().issueserverindex());

		if (usermanager->AddUser(issued_usersn, puser)) {
			res.set_result(ErrCode::ResultSuccess);
			res.set_usersn(issued_usersn);
			res.set_userid(req.userid());
		}
	} while (false);
	// [Logic End]

	// [res 패킷 serialize 작업 및 패킷 전송]
	auto p = PACKET_BUILDER::Build<::packet::Packet*>();
	p->SetCommand(S2CLoginUserRes);
	return SendToUser::SendPacket(*p, res, packet_argument->psession_);
	// [res 패킷 serialize 작업 및 패킷 전송 End]
}


// LogOutUserReqProcessPacket는 유저의 로그아웃 처리 로직을 추가합니다. 
bool
UserLoginLogic::LogOutUserReqProcessPacket(Argument* arg){
	// [request 패킷 Parser 작업]
	PacketHandlerArgument* packet_argument = nullptr;
	C2CS_LogOutUserReq req;
	if (false == PacketProcessHelper::PacketParserFromArg(req, arg, packet_argument) || packet_argument == nullptr) {
		return false;
	}
	// [request 패킷 Parser 작업 End]
	CS2C_LogOutUserRes res;
	res.set_result(ErrCode::ResultFail);

	// [Logic Start]
	auto & usermanager = EXAMCHAT_CONST_INSTANCE().GetUserManager();
	if (usermanager && true == usermanager->DelUser(req.usersn()) ) {
		res.set_result(ErrCode::ResultSuccess);
		res.set_userid(req.userid());
		res.set_usersn(req.usersn());
	}
	// [Logic End]

	// [res 패킷 serialize 작업 및 패킷 전송]
	auto p = PACKET_BUILDER::Build<::packet::Packet*>();
	p->SetCommand(S2CLogOutUserRes);
	return SendToUser::SendPacket(*p, res, packet_argument->psession_);
	// [res 패킷 serialize 작업 및 패킷 전송 End]
}
	
}}} /// pokerserver::logic::packetprocess::player