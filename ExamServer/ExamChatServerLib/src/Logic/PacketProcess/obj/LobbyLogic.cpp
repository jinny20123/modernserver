﻿
#include "LobbyLogic.h"
#include "Utility/src/macro.h"
#include "TCPServer/Network/Handler/LogicHandlerArgument.h"
#include "Network/src/Session/Session.h"
#include "Network/src/Session/Logic/SessionLogicArgument.h"
#include "CommonPacket/src/Packet.h"
#include "CommonPacket/src/PacketBuilder.h"
#include "Log/src/Common/CommonLogger.h"
#include "src/Share/LogicPacket.pb.h"
#include "src/Share/PacketCmd.pb.h"
#include "src/Singleton/ServerInstance.h"
#include "src/Session/ServerProxyManager.h"
#include "src/Session/ServerProxy.h"
#include "src/Logic/User/UserManager.h"
#include "src/Logic/User/User.h"
#include "src/Logic/PacketProcess/PacketProcessHelper.h"

namespace examchatserver { 
namespace logic {
namespace packetprocess { 

using ::tcpserver::network::handler::PacketHandlerArgument;
using namespace examshare;
using namespace ::network::session::logic;

/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct LobbyLogic::Impl
{
	using TestLogicCon = std::unordered_map<int, std::function<int(::google::protobuf::MessageLite& arg)>>;
	Impl() {}

	bool LobbyMsgReqProcessLogic(C2CS_LobbySendMsgReq& req, CS2C_LobbySendMsgRes& res);

	TestLogicCon TestProcessLogics;
}; 

bool 
LobbyLogic::Impl::LobbyMsgReqProcessLogic(C2CS_LobbySendMsgReq& req, CS2C_LobbySendMsgRes& res) {
	do {
		// To do : UserPool 생성 작업 필요
		auto& usermanager = EXAMCHAT_CONST_INSTANCE().GetUserManager();
		const auto isFindUser = usermanager->FindUserFunc(req.usersn(),
			[&res](std::shared_ptr<User>& puser) {
				res.set_userid(puser->UserId());
			});

		if (true == isFindUser) {
			res.set_result(ErrCode::ResultSuccess);
			res.set_msg(req.msg());
		}
	} while (false);

	return true;
}


LobbyLogic::LobbyLogic()
{
	MAKE_UNIQUE(mimpl_, Impl);

	mimpl_->TestProcessLogics.insert(Impl::TestLogicCon::value_type(C2SLobbyMsgReq, [this](::google::protobuf::MessageLite& arg) {
		// [request 패킷 Parser 작업]	
		C2CS_LobbySendMsgReq& req = static_cast<C2CS_LobbySendMsgReq&>(arg);
		// [request 패킷 Parser 작업 End]

		// [Logic Start]
		CS2C_LobbySendMsgRes res;
		res.set_result(ErrCode::ResultFail);
		mimpl_->LobbyMsgReqProcessLogic(req, res);
		// [Logic End]
		return res.result();
		}));
}
LobbyLogic::~LobbyLogic(){


}


// LoginUserReqProcessPacket는 유저의 로그인 처리 로직을 추가합니다. 
bool 
LobbyLogic::LobbyMsgReqProcessPacket(Argument * arg){

	// [request 패킷 Parser 작업]	
	C2CS_LobbySendMsgReq req;

	PacketHandlerArgument* packet_argument = nullptr;
	if (false == PacketProcessHelper::PacketParserFromArg(req, arg, packet_argument) || packet_argument == nullptr) {
		return false;
	}
	// [request 패킷 Parser 작업 End]

	// [Logic Start]
	CS2C_LobbySendMsgRes res;
	res.set_result(ErrCode::ResultFail);
	mimpl_->LobbyMsgReqProcessLogic(req, res);
	// [Logic End]

	// [res 패킷 serialize 작업 및 패킷 전송]
	auto p = PACKET_BUILDER::Build<::packet::Packet*>();
	p->SetCommand(S2CLobbyMsgRes);
	return SendToAllFront::SendPacket(*p, res);
	// [res 패킷 serialize 작업 및 패킷 전송 End]
}

int 
LobbyLogic::LobbyMsgReqProcessTest(int cmd, ::google::protobuf::MessageLite& arg) {

	auto itr = mimpl_->TestProcessLogics.find(cmd);
	if (itr != mimpl_->TestProcessLogics.end()) {
		return itr->second(arg);
	}

	return ResultTestFail;
}

	
}}} /// pokerserver::logic::packetprocess::player