﻿#pragma once
#include "Utility/src/Object/argument_fwd.h"
#include <unordered_map>


namespace google {
namespace protobuf {
	class  MessageLite;
}}

namespace tcpserver {
namespace network {
	namespace handler {
		struct  PacketHandlerArgument;
	}
}}

namespace network {
namespace session{
	namespace logic {
		struct SessionLogicSendArgument;
	}

	class SessionBase;
}}

namespace packet {
	struct Packet;
}

namespace examchatserver { 
namespace logic { 
namespace packetprocess { 

/**
@date : 2020-05-13
@brief : PacketProcess에서 사용하는 반복된 작업들을 사용하기 쉽도록 도움을 줍니다.
	 	 반복 작업을 최대한 단순하게 지원합니다.
@author : jinny20123(안 정웅)
*/
class PacketProcessHelper {
public:
	static bool PacketParserFromArg(::google::protobuf::MessageLite& msg, ::utility::object::Argument* arg, ::tcpserver::network::handler::PacketHandlerArgument*& packet_argument);
	static bool PacketSerializetoArg(::google::protobuf::MessageLite& msg, ::packet::Packet& packet, ::network::session::logic::SessionLogicSendArgument& sls_arg);

	template<typename SendPolicy, unsigned int frontserver_idx = 0>
	static bool FrontSendPacket(::packet::Packet& packet, ::google::protobuf::MessageLite& res) {
		return SendPolicy::SendPacket(packet, res, frontserver_idx);
	}

	//static bool FrontSendPacketMyself
};

//class PacketTestHelper : public boost::serialization::singleton<PacketTestHelper> {
//public:
//	using TestLogicCon = std::unordered_map<int, std::function<int(::google::protobuf::MessageLite& arg)>>;
//	bool Insert(int cmd, const TestLogicCon::value_type & value);
//
//private:
//	struct Impl;
//	std::unique_ptr<Impl> mimpl_;
//};



class SendToUser {
public:
	static bool SendPacket(::packet::Packet& packet, ::google::protobuf::MessageLite& res, network::session::SessionBase * psession);
	static bool SendPacket(::packet::Packet& packet, ::google::protobuf::MessageLite& res, unsigned int svr_idx);
};

class SendToAllFront {
public:
	static bool SendPacket(::packet::Packet& packet, ::google::protobuf::MessageLite& res);
};

	
}}} /// examchatserver::logic::packetprocess