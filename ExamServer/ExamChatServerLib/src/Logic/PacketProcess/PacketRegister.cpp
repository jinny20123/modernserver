﻿#include "PacketRegister.h"
#include "CommonPacket/src/Packet.h"
#include "Log/src/Common/CommonLogger.h"
#include "TCPServer/Network/Handler/LogicHandlerArgument.h"
#include "src/Share/PacketCmd.pb.h"
#include "src/Logic/PacketProcess/Obj/SystemLogic.h"
#include "src/Logic/PacketProcess/Obj/UserLoginLogic.h"
#include "src/Logic/PacketProcess/Obj/ChatRoomLogic.h"
#include "src/Logic/PacketProcess/Obj/LobbyLogic.h"
#include "CommonPacket/src/Packet.h"
#include "CommonPacket/src/PacketDefine.h"
#include <vector>
#include <assert.h>


namespace examchatserver { 
namespace logic { 
namespace packetprocess { 

using namespace ::packet;
using namespace ::examshare;

/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct PacketRegister::Impl
{
	/// Recv 처리 함수 객체
	using ProcessPacketLogicFun = PacketRegister::ProcessPacketLogicFun;
	using PacketLogicFuncContainer = PacketRegister::PacketLogicFuncContainer;

	enum
	{
		DEFAULT_MAX_COMMAND = 0,
	};

	
	PacketLogicFuncContainer				ppacketlogic_func_container_;
	::packet::packet_commandtype			nmax_command_;
	std::unique_ptr<SystemLogic>			psystem_logic;
	std::unique_ptr<UserLoginLogic>			puserlogin_logic;
	std::unique_ptr<ChatRoomLogic>			pchatroom_logic;
	std::unique_ptr<LobbyLogic>				plobby_logic;

	void InsertPacketProcess(::packet::packet_commandtype key, ProcessPacketLogicFun func)
	{
		auto ret = this->ppacketlogic_func_container_.insert(Impl::PacketLogicFuncContainer::value_type(key, func));

		if (nmax_command_ < key)
		{
			nmax_command_ = static_cast<::packet::packet_commandtype>(key);
		}
	}
};

#define INSERT_PACKET_LOGIC(cmd, logic) \
mImpl->InsertPacketProcess(cmd, [this](Argument* arg) -> bool  {\
return logic(arg);\
})


PacketRegister::PacketLogicFuncContainer &
PacketRegister::GetRecvProcessCon()
{
	return mImpl->ppacketlogic_func_container_;
}


PacketRegister::PacketRegister()
{
	MAKE_UNIQUE(mImpl, Impl);
	mImpl->nmax_command_ = Impl::DEFAULT_MAX_COMMAND;
	MAKE_UNIQUE(mImpl->psystem_logic, SystemLogic);
	MAKE_UNIQUE(mImpl->puserlogin_logic, UserLoginLogic);
	MAKE_UNIQUE(mImpl->pchatroom_logic, ChatRoomLogic);
	MAKE_UNIQUE(mImpl->plobby_logic, LobbyLogic);
}
PacketRegister::~PacketRegister()
{
}

void
PacketRegister::Prepare()
{
	// 패킷 로직 등록하는입니다. 
	// 만약에 패킷이 추가된다면 ExamServer/ExamChatServer/Logic/PacketProcess/Obj 위치에 알맞은 클래스를 찾거나 추가 한 후
	// PacketRegister::ProcessPacketLogicFun
	INSERT_PACKET_LOGIC(F2CSServerRegistReq, mImpl->psystem_logic->SysServerRegistReqProcessPacket);
	INSERT_PACKET_LOGIC(CS2FServerRegistRes, mImpl->psystem_logic->SysServerUnregistReqProcessPacket);

	INSERT_PACKET_LOGIC(C2SLoginUserReq, mImpl->puserlogin_logic->LoginUserReqProcessPacket);
	INSERT_PACKET_LOGIC(C2SLogOutUserReq, mImpl->puserlogin_logic->LogOutUserReqProcessPacket);

	INSERT_PACKET_LOGIC(C2SLobbyMsgReq, mImpl->plobby_logic->LobbyMsgReqProcessPacket);

	
	COMMONLOG().ToDebug(NS_COMMONLOG::CONSOLE, "max command(%d)", mImpl->nmax_command_);
	COMMONLOG().ToDebug(NS_COMMONLOG::CONSOLE, "Regist Command Count(%d)", mImpl->ppacketlogic_func_container_.size());
}


const unsigned int
PacketRegister::GetMaxCommand() const 
{
	return mImpl->nmax_command_;
}

	
}}} /// examchatserver::logic::packetprocess