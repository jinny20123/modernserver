﻿#pragma once

#include <memory>
#include "Common/src/Logic/Construct/IConstruct.h"
namespace examchatserver { namespace logic { namespace construct { namespace object { 


/** 
	@class : PokerConstruct 
	@date : 2020-04-21
	@brief : ExamChat 서버를 구동하는 작업을 등록합니다. 
	@author : jinny20123(안 정웅)
*/
class ExamChatConstruct 
	: public ::common::logic::construct::IConstruct
{
public: 
	ExamChatConstruct();
	virtual ~ExamChatConstruct();

	virtual void SetUp() OVERRIDE;
	virtual void Run() OVERRIDE;
private :
	struct Impl;
	std::unique_ptr<Impl> impl_;
};
	
}}}} /// pokerserver::logic::construct::object