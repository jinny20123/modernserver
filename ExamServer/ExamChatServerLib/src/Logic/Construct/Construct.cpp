﻿#include "Construct.h"
#include "Common/src/Logic/Configure/IConfigure.h"
#include "TCPServer/Static/TcpServerStatic.h"
#include "TCPServer/Logic/PacketProcess/PacketProcessContainer.h"
#include "TCPServer/Logic/Construct/Object/TCPServerConstruct.h"
#include "TCPServer/Logic/Construct/TCPServerConstructArgument.h"
#include "DataBase/src/Logic/Construct/MysqlDatabaseConstruct.h"
#include "DataBase/src/Logic/Construct/DatabaseConsturctArgument.h"
#include "src/Logic/PacketProcess/PacketRegister.h"
#include "src/Logic/Configure/ConfigureManager.h"
#include <vector>
#include <boost/foreach.hpp>

using namespace ::tcpserver::logic::packetprocess;
using namespace ::examchatserver::logic::packetprocess;

namespace examchatserver {
	namespace logic {
		namespace construct {
			namespace object {
				/// 내부에서 사용할 변수를 정의합니다.
				/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
				struct ExamChatConstruct::Impl
				{
					typedef ::common::logic::construct::IConstruct
						IConstruct;
					typedef ::tcpserver::logic::construct::object::TCPServerConstruct
						TCPServerConstruct;
					typedef ::database::logic::construct::MysqlDatabaseConstruct
						MysqlDatabaseConstruct;

					Impl() {}

					std::vector<IConstruct*> construct_container;
					std::unique_ptr<TCPServerConstruct> tcpserver_construct;
					std::unique_ptr<MysqlDatabaseConstruct> mysql_construct;
				};

				ExamChatConstruct::ExamChatConstruct()
				{
					examchatserver::logic::configure::GET_CONFIGUREMANAGER().SetUp();

					MAKE_UNIQUE(impl_, Impl);

					impl_->construct_container.clear();

					// Tcp 서버용 config 파일
					auto tcp_config = examchatserver::logic::configure::GET_CONFIGUREMANAGER().Get(::examchatserver::logic::configure::ConfigureManager::TCPSERVERINFO);
					size_t tcp_packet_thread_count = 1;
					tcp_config->TGet("INFO.PACKET_THREAD_COUNT", tcp_packet_thread_count);
					unsigned short bind_port = 20414;
					tcp_config->TGet("INFO.LISTEN_PORT", bind_port);

					// tcp 서버용 객체 생성
					MAKE_UNIQUE(impl_->tcpserver_construct, Impl::TCPServerConstruct, &tcpserver::logic::construct::TCPConsturctCreateArgument(tcp_packet_thread_count, bind_port));

					// 데이터 베이스용 config 파일
					auto database_config = examchatserver::logic::configure::GET_CONFIGUREMANAGER().Get(::examchatserver::logic::configure::ConfigureManager::DATABASEINFO);

					size_t thread_count = 1;
					database_config->TGet<size_t>("INFO.THREAD_COUNT", thread_count);
					auto szIP = database_config->Get("CONNECT.IP", "127.0.0.1");
					auto szUSER = database_config->Get("CONNECT.USER", "");
					auto szPW = database_config->Get("CONNECT.PW", "");

					// mysql 데이터 베이스 객체 생성
					MAKE_UNIQUE(impl_->mysql_construct, Impl::MysqlDatabaseConstruct, &database::logic::construct::MysqlConsturctCreateArgument(thread_count, szIP.c_str(), szUSER.c_str(), szPW.c_str()));

					// Construct 컨테이너에 삽입
					impl_->construct_container.push_back(impl_->tcpserver_construct.get());
					impl_->construct_container.push_back(impl_->mysql_construct.get());
				}
				ExamChatConstruct::~ExamChatConstruct()
				{
				}

				void
				ExamChatConstruct::SetUp()
				{
					std::shared_ptr<IPacketRegister> pPacketRegister(new PacketRegister);
					::tcpserver::static_::TCPSERVER_STATIC().GetPacketProcessContainer()->Prepare(pPacketRegister);

					for (auto consturct : impl_->construct_container)
					{
						consturct->SetUp();
					}
				}
				void
				ExamChatConstruct::Run()
				{
					for (auto consturct : impl_->construct_container)
					{
						consturct->Run();
					}
				}
			}
		}
	}
} /// pokerserver::logic::construct::object