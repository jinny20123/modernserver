﻿#pragma once

#include <memory>
#include "Utility/src/macro.h"
#include "DataBase/src/Logic/Query/IQuery.h"

namespace examchatserver { namespace logic { namespace query { 


/** 
	@class : PingQuery 
	@date : 2016-10-06
	@brief : 
	@author : jinny20123(안 정웅)
*/
class PingQuery 
	: public ::database::logic::query::IQuery
{
public: 
	PingQuery();
	virtual ~PingQuery();

	virtual bool Request(database::session::ISession * psession) OVERRIDE;
	virtual bool Response() OVERRIDE;
private :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};
	
}}} /// pokerserver::logic::query