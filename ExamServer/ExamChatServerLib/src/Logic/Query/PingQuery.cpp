﻿
#include "PingQuery.h"
#include "DataBase/src/Session/Object/MysqlSession.h"

namespace examchatserver { namespace logic { namespace query { 


/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct PingQuery::Impl
{
	Impl() {}
	bool bRet;
}; 

PingQuery::PingQuery()
{
	MAKE_UNIQUE(mImpl, Impl);
}
PingQuery::~PingQuery()
{
	
}

bool
PingQuery::Request(database::session::ISession * psession)
{
	typedef ::database::session::object::MysqlSession
			MysqlSession;	
	if (!psession) { return false; }
	if(psession->GetType() != MysqlSession::Type::MYSQL)
	{
		return false;
	}

	auto pMysqlSession = static_cast<MysqlSession*>(psession);

	// 간단한 쿼리 함수를 등록합니다. 
	mImpl->bRet = pMysqlSession->SimpleQuery("SELECT 1;");

	return true;
}

bool
PingQuery::Response()
{
	return mImpl->bRet;
}
	
}}} /// pokerserver::logic::query