﻿#pragma once

#include <memory>
#include "Utility/src/macro.h"
#include "DataBase/src/Logic/Query/IQuery.h"


namespace examchatserver { namespace logic { namespace query { namespace player { 


/** 
	@class : CreateUser 
	@date : 2016-10-06
	@brief : 데이터베이스에 유저를 생성해달라고 요청 합니다.
	@author : jinny20123(안 정웅)
*/
class QueryCreateUser 
	: public ::database::logic::query::IQuery
{
public: 
	QueryCreateUser();
	virtual ~QueryCreateUser();

	virtual bool Request(database::session::ISession * psession) OVERRIDE;
	virtual bool Response() OVERRIDE;
private :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};
	
}}}} /// pokerserver::logic::query::player