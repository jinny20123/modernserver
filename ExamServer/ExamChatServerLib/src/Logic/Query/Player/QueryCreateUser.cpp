﻿
#include "QueryCreateUser.h"
namespace examchatserver { namespace logic { namespace query { namespace player { 


/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct QueryCreateUser::Impl
{
	Impl() {}
}; 

QueryCreateUser::QueryCreateUser()
{
	MAKE_UNIQUE(mImpl, Impl);
}
QueryCreateUser::~QueryCreateUser()
{
	
}

bool 
QueryCreateUser::Request(database::session::ISession * psession)
{
	return true;
}

bool 
QueryCreateUser::Response()
{
	return true;
}
	
}}}} /// pokerserver::logic::query::player