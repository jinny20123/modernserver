﻿#pragma once
#include <memory>
#include <functional>
#include <string>
	
namespace examchatserver { namespace logic {

class User;

constexpr uint32_t kUserSnStartIndex = 10000;

class UserManager
{
public: 

	using UserCallBack = std::function<void(std::shared_ptr<User>&)>;

	UserManager();
	virtual ~UserManager();

	bool AddUser(uint64_t sn, std::shared_ptr<User> & puser);
	bool DelUser(uint64_t sn);
	const bool FindUser(uint64_t sn) const;
	const bool FindUserName(const std::string& name) const;
	const bool FindUserFunc(uint64_t sn, UserCallBack callback) const;

	size_t UserCount();

	// IssueUserSn 는 user sn을 발급합니다. 
	uint64_t IssueUserSn();

private :
	struct Impl;
	std::unique_ptr<Impl> mimpl_;
};
	
}} /// examchatserver::logic