﻿#include "User.h"
#include "Utility/src/macro.h"



namespace examchatserver { 
namespace logic { 


/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct User::Impl
{	
	uint32_t frontserveridx_;		// 접속한 front서버의 index
	std::string userid_;			// 유저의 id 정보
	uint64_t usersn_;				// 유저의 unique_번호
};

User::User()
{	
	MAKE_UNIQUE(mimpl_, Impl);
}

User::~User()
{
}

void 
User::SetFrontServerIndex(uint32_t idx) {
	mimpl_->frontserveridx_ = idx;
}
uint32_t 
User::FrontServerIndex() {
	return mimpl_->frontserveridx_;
}

void 
User::SetUserId(const std::string& id) {
	mimpl_->userid_ = id;
}
std::string& 
User::UserId() {
	return mimpl_->userid_;
}

void 
User::SetUserSn(const uint64_t sn) {
	mimpl_->usersn_ = sn;
}
uint64_t
User::UserSn() {
	return mimpl_->usersn_;
}
	
}} /// examchatserver::logic