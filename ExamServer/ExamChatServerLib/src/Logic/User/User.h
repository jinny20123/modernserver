﻿#pragma once
#include <memory>
#include <string>

namespace examchatserver { namespace logic {

class User 
{
public: 

	User();
	virtual ~User();

	User(const User& u) = default;
	User(User&& u) = default;
	User& operator=(const User& u) = default;
	User& operator=(User&& u) = default;

	void SetFrontServerIndex(uint32_t idx);
	uint32_t FrontServerIndex();
	void SetUserId(const std::string& id);
	std::string& UserId();
	void SetUserSn(const uint64_t sn);
	uint64_t UserSn();

private :
	struct Impl;
	std::unique_ptr<Impl> mimpl_;
};
	
}} /// pokerserver::logic