﻿#include "UserManager.h"
#include "Utility/src/macro.h"
#include "User.h"
#include <unordered_map>
#include <unordered_set>
#include <atomic>
#include <mutex>
#include <shared_mutex>
#include <cassert>


namespace examchatserver { 
namespace logic { 


/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct UserManager::Impl
{	
	using UserCon = std::unordered_map<uint64_t, std::shared_ptr<User>>;
	using UserNameCon = std::unordered_set<std::string>;

	UserCon							user_con_;
	UserNameCon						username_con_;
	std::atomic<uint64_t>			user_sn_indexer;
	mutable std::shared_mutex		user_con_sm_;
	std::shared_ptr<User>			null_user_;
};

UserManager::UserManager()
{
	MAKE_UNIQUE(mimpl_, Impl);
	mimpl_->user_sn_indexer.store(kUserSnStartIndex);
	assert(mimpl_->null_user_ == nullptr);
}

UserManager::~UserManager()
{
}

bool 
UserManager::AddUser(uint64_t sn, std::shared_ptr<User> & puser) {
	std::unique_lock<std::shared_mutex> lock(mimpl_->user_con_sm_);
	const auto find_itr = mimpl_->user_con_.find(sn);

	// 컨테이너에 이미 유저가 존재함
	if (find_itr != mimpl_->user_con_.end()) {
		// 로그 아웃 처리 및 삭제
		mimpl_->user_con_.erase(sn);
	}

	// 유저 추가 작업
	puser->SetUserSn(sn);
	auto result = mimpl_->user_con_.insert(Impl::UserCon::value_type(sn, puser));

	if (result.second) {
		mimpl_->username_con_.insert(puser->UserId());
	}

	return result.second;
}
bool 
UserManager::DelUser(uint64_t sn) {
	std::unique_lock<std::shared_mutex> lock(mimpl_->user_con_sm_);
	const auto itr = mimpl_->user_con_.find(sn);

	if (itr != mimpl_->user_con_.end()) {
		auto del_userid = itr->second->UserId();
		// 로그 아웃 처리 및 삭제 이후에 itr 무효화
		if (0 != mimpl_->user_con_.erase(sn)) {
			mimpl_->username_con_.erase(del_userid);
		}
		return true;
	}
	return false;
}

const bool
UserManager::FindUser(uint64_t sn) const {
	std::shared_lock<std::shared_mutex> lock(mimpl_->user_con_sm_);
	const auto itr = mimpl_->user_con_.find(sn);
	return itr != mimpl_->user_con_.end() ? true : false;
}

const bool 
UserManager::FindUserName(const std::string& name) const {
	std::shared_lock<std::shared_mutex> lock(mimpl_->user_con_sm_);
	const auto itr = mimpl_->username_con_.find(name);
	return itr != mimpl_->username_con_.end() ? true : false;
}

const bool 
UserManager::FindUserFunc(uint64_t sn, UserCallBack callback) const {
	std::shared_lock<std::shared_mutex> lock(mimpl_->user_con_sm_);
	const auto itr = mimpl_->user_con_.find(sn);
	if (itr != mimpl_->user_con_.end()) {
		callback(itr->second);
		return true;
	}
	return false;
}

size_t
UserManager::UserCount() {
	std::shared_lock<std::shared_mutex> lock(mimpl_->user_con_sm_);
	return mimpl_->user_con_.size();
}

uint64_t 
UserManager::IssueUserSn() {
	return mimpl_->user_sn_indexer.fetch_add(1);
}


	
}} /// examchatserver::logic