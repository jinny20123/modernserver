﻿#pragma once

#include <boost/serialization/singleton.hpp>
#include <memory>

namespace common { namespace logic { namespace configure { 
	class IConfigure;

	namespace object
	{
		class InIConfigure;
	}

}}}

namespace  examchatserver { namespace logic { namespace configure { 


/** 
	@class : ConfigureManager 
	@date : 2020-04-21
	@brief : Poker서버에서 사용할 설정 파일을 등록합니다.
	@author : jinny20123(안 정웅)
*/
class ConfigureManager 
	: public ::boost::serialization::singleton<ConfigureManager>
{
public: 
	typedef ::common::logic::configure::IConfigure
			IConfigure;
	typedef ::common::logic::configure::object::InIConfigure
			InIConfigure;

	ConfigureManager();
	virtual ~ConfigureManager();

	enum ConfigureType
	{
		TCPSERVERINFO,			
		DATABASEINFO,
	};

	void SetUp();			///< ConfigureManager를 설정하는 작업 실행 - 파일 연결하기
	IConfigure * Get(ConfigureType type);	///< 해당 타입의 Configure를 리턴합니다. 
private :
	struct Impl;
	std::unique_ptr<Impl> impl_;
};

inline ConfigureManager & GET_CONFIGUREMANAGER()
{
	return ConfigureManager::get_mutable_instance();
}
	
}}} ///  pokerserver::logic::configure