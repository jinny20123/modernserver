﻿
#include "ConfigureManager.h"
#include "Utility/src/macro.h"
#include "Common/src/Logic/Configure/Object/InIConfigure.h"
#include <boost/filesystem.hpp>
#include <unordered_map>

namespace  examchatserver { namespace logic { namespace configure {


#define INSERT_CONFIGURE(type, Object, filename) \
	impl_->container_.insert(Impl::ConfigureContainer::value_type(type, std::make_shared<Object>( ( config_path + filename).c_str() ) ) );	

/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct ConfigureManager::Impl
{
	typedef std::unordered_map<ConfigureType, std::shared_ptr<IConfigure>>
			ConfigureContainer;
	Impl() {}

	ConfigureContainer container_;
	std::shared_ptr<IConfigure> base_ini_;
}; 

ConfigureManager::ConfigureManager()
{
	MAKE_UNIQUE(impl_, Impl);
}
ConfigureManager::~ConfigureManager()
{
	
}

void
ConfigureManager::SetUp()
{
	boost::filesystem::path current_path = boost::filesystem::current_path();

	/// 해당 경로에 폴더 만들기 
	std::string config_path = current_path.string();
	config_path.append("\\config\\");

	if (!boost::filesystem::exists(boost::filesystem::path(config_path)))
	{
		boost::filesystem::create_directory(config_path);
	}

	// Configure 파일을 읽습니다. 
	INSERT_CONFIGURE(TCPSERVERINFO, InIConfigure, "TCPServer.ini");
	INSERT_CONFIGURE(DATABASEINFO, InIConfigure, "DataBase.ini");
}

ConfigureManager::IConfigure *
ConfigureManager::Get(ConfigureType type)
{
	Impl::ConfigureContainer::iterator itr = impl_->container_.find(type);
	if (impl_->container_.end() == itr)
	{
		return nullptr;
	}

	return itr->second.get();
}
	
}}} ///  pokerserver::logic::configure