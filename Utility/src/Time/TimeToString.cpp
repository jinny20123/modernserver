﻿#include "TimeToString.h"
#include <Windows.h>

namespace utility {
	namespace time {
		TimeToString::TimeToString()
		{
		}
		TimeToString::~TimeToString()
		{
		}

		void
			TimeToString::GetCurrentTimeToString(std::string& strtime)
		{
			const size_t kdatelength = 64;
			char date[kdatelength];
			GetCurrentTimeToString(date, kdatelength);
			strtime.append(date);
		}

		void
			TimeToString::GetCurrentTimeToString(char* strtime, size_t size)
		{
#ifdef _WIN32
			SYSTEMTIME systemtime;
			GetLocalTime(&systemtime);

			sprintf_s(strtime, size, "[%04d-%02d-%02d %02d:%02d:%02d:%03d] ",
				systemtime.wYear,
				systemtime.wMonth,
				systemtime.wDay,
				systemtime.wHour,
				systemtime.wMinute,
				systemtime.wSecond,
				systemtime.wMilliseconds
			);
#else
			timeval tv;
			gettimeofday(&tv, NULL);

			struct tm* tm = localtime((time_t*)&tv.tv_sec);

			snprintf(strtime, size, "[%04d-%02d-%02d %02d:%02d:%02d:%03d] ",
				tm->tm_year + 1900,
				tm->tm_mon + 1,
				tm->tm_mday,
				tm->tm_hour,
				tm->tm_min,
				tm->tm_sec,
				(int32_t)(tv.tv_usec / 1000)
			);

#endif
		}
	}
} /// utility::time