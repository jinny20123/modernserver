﻿#pragma once
#include <cstdio>
#include <cstdarg>
#include <xstring>

namespace utility {
	namespace time {
		class TimeToString
		{
		public:
			TimeToString();
			virtual ~TimeToString();

			static void GetCurrentTimeToString(std::string& strtime);
			static void GetCurrentTimeToString(char* strtime, size_t size);
		private:
		};
	}
} /// utility::time