﻿#ifndef MODERN_ENGINE_PROCESSOR_PRODUCER_H_
#define MODERN_ENGINE_PROCESSOR_PRODUCER_H_

#include <cstdint>
#include <boost/bind.hpp>
#include <memory>
#include "../../Object/message.h"
#include "../../Object/queue.h"

namespace utility {
	namespace engine {
		namespace processor {
			/**
				@class : Producer<T>
				@date : 2015-08-21
				@brief : Producer는 Queue 보유합니다.
						Queue는 템플릿타입의 정보를 저장하고 있습니다.
						Producer를 Consumer에 등록하면 Consumer에서는 Producer의 Queue를 감시하여 데이터가 들어오면 처리합니다.
				@author : jinny20123(안 정웅)
			*/
			template<typename object>
			class Producer
			{
			public:
				typedef object
					obj;
				typedef utility::object::Queue<object>
					Queue;
				typedef typename utility::object::Queue<object>::size_type
					size_type;

				Producer();
				virtual ~Producer();

				object							pop();
				void							push(object& obj_value);
				void							push(int32_t nhandle, void* pvalue);
				size_type						size();
			private:
				std::shared_ptr<utility::object::Queue<object>> pqueue_;
			};

			template<typename object>
			Producer<object>::Producer()
			{
				pqueue_.reset(new utility::object::Queue<object>);
			}
			template<typename object>
			Producer<object>::~Producer()
			{
			}

			template<typename object>
			object
				Producer<object>::pop()
			{
				return pqueue_->Pop();
			}
			template<typename object>
			void
				Producer<object>::push(object& obj_value)
			{
				return pqueue_->Push(obj_value);
			}
			template<typename object>
			void
				Producer<object>::push(int32_t nhandle, void* pvalue)
			{
				if (nullptr == pvalue) return;

				object obj;
				obj.Handle_ = nhandle;
				obj.pValue_ = pvalue;
				push(obj);
			}

			template<typename object>
			typename Producer<object>::size_type
				Producer<object>::size()
			{
				return pqueue_->Size();
			}
		}
	}
} /// utility::engine::processor

#define ENGINE_PRODUCER(obj) \
::utility::engine::processor::Producer<obj>
#endif /// MODERN_ENGINE_PROCESSOR_PRODUCER_H_