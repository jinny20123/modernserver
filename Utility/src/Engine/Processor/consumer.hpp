﻿#ifndef MODERN_ENGINE_PROCESSOR_CONSUMER_H_
#define MODERN_ENGINE_PROCESSOR_CONSUMER_H_

#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <memory>
#include "./producer.hpp"
#include "../../Object/message.h"

namespace utility {
	namespace engine {
		namespace processor {
			/**
				@class : Consumer<T>
				@date : 2015-08-21
				@brief : 등록된 Producer의 Queue를 실시간으로 감지하여 데이터가 들어오면
						Handle함수로 보내집니다.
						Handle함수는 사용자가 정의합니다.
				@author : jinny20123(안 정웅)
			*/
			template<typename Producer>
			class Consumer
			{
			public:
				typedef Producer
					ProducerObj;
				typedef typename Producer::obj
					object;

				explicit Consumer(Producer* producer, size_t thread_count);
				virtual ~Consumer();

			protected:
				virtual void Prepare();									///  프로세서를 실행하기 전에 해야 할 작업을 등록합니다.
				virtual void Execute();									///  생산자 큐에서 오브젝트를 받아와 Handler에 넘깁니다.
				virtual void Handle(const object& obj) = 0;			///  생산자의 object를 처리합니다.
				virtual void ThreadRun();

			private:
				struct Impl
				{
					std::shared_ptr<Producer>	pProducer_;
					boost::thread_group			thread_group_;
					size_t						nRuncount_;
				};

				void Run();
				Impl* mImpl;
			};

			template<typename Producer>
			Consumer<Producer>::Consumer(Producer* producer, size_t thread_count)
			{
				mImpl = new Impl;
				mImpl->pProducer_.reset(producer);
				mImpl->nRuncount_ = thread_count;
			}

			template<typename Producer>
			Consumer<Producer>::~Consumer()
			{
			}

			template<typename Producer>
			void
				Consumer<Producer>::Prepare()
			{
			}

			template<typename Producer>
			void
				Consumer<Producer>::ThreadRun()
			{
				for (int i = 0; i < mImpl->nRuncount_; i++) Run();
			}

			template<typename Producer>
			void
				Consumer<Producer>::Execute()
			{
				Prepare();
				for (;;)
				{
					object obj = mImpl->pProducer_->pop();
					Handle(obj);
				}
			}

			template<typename Producer>
			void
				Consumer<Producer>::Run()
			{
				mImpl->thread_group_.add_thread(new boost::thread(boost::bind(&Consumer::Execute, this)));
			}
		}
	}
} /// utility::engine::processor

#define CONSUMER(obj) \
::utility::engine::processor::Consumer<utility::engine::processor::Producer<obj> >
#endif /// MODERN_ENGINE_PROCESSOR_CONSUMER_H_