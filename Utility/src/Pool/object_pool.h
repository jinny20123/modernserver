﻿#pragma once

/*
* [정보]
*  작성자	: 안 정웅
*  히스토리
*   1. 2014-03-26 : Pool클래스 리뉴얼 작업, 가독성이 편하도록 변경, 클래스 정보 추가 작업
*
* [설명]
*  ObjectPool는 객체를 동적 할당하고 Pool 컨테이너에서 관리합니다.
*  생성시나 Create를 통해서 컨테이너에 객체를 생성합니다.
*  필요시에 Acquire를 통해서 객체를 얻어옵니다.
*
* [예시]
* TODO 작성요망
*/

#include "../Type/type.h"
#include <boost/noncopyable.hpp>
#include <functional>

namespace utility {
	namespace pool {
		/**
			@class : ObjectPool
			@date : 2015/05/06
			@brief : ObjectPool는 객체를 동적 할당하고 Pool 컨테이너에서 관리합니다.
					생성시나 Create를 통해서 컨테이너에 객체를 생성합니다.
					필요시에 Acquire를 통해서 객체를 얻어옵니다.
			@author : jinny20123(안 정웅)
		*/
		template <typename object, template<typename T> class ThreadType>
		class ObjectPool : private boost::noncopyable
		{
		public:
			using ArgT = typename type::TypeOp<object>::ArgT;
			using PtrT = typename type::TypeOp<object>::PtrT;

			using Queue = typename ThreadType<object*>::Queue;

			ObjectPool();
			~ObjectPool();

			/// Pool에서 객체를 얻어옵니다.
			bool Acquire(object*& ptr);
			/// 사용이 끝난 객체를 Pool에 반환합니다.
			bool Restore(object*& ptr);

		private:
			/// 사용대기중이 객체를 관리하기 위한 컨테이너
			Queue										queue_;
		};

		template <typename object, template<typename T> class ThreadType>
		ObjectPool<object, ThreadType>::ObjectPool()
		{
		}

		template <typename object, template<typename T> class ThreadType>
		ObjectPool<object, ThreadType>::~ObjectPool()
		{
		}

		template <typename object, template<typename T> class ThreadType>
		bool
		ObjectPool<object, ThreadType>::Acquire(object*& ptr)
		{
			bool ret = false;

			do
			{
				// 현재 컨테이너가 비어 있는지 확인
				if (true == queue_.Empty())
				{
					break;
				}

				if (true == queue_.Pop(ptr))
				{
					ret = true;
					break;
				}
			} while (false);

			return ret;
		}

		template <typename object, template<typename T> class ThreadType>
		bool
			ObjectPool<object, ThreadType>::Restore(object*& ptr)
		{
			queue_.Push(ptr);
			ptr = nullptr;
			return true;
		}
	}
} /// utility::pool
