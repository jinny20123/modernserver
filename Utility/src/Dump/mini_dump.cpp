﻿#include "mini_dump.h"

#ifdef _WIN32
#include <tchar.h>
#include <cstdio>
#include <WinNT.h>
#include <process.h>
#include <functional>
#pragma comment(lib, "Dbghelp.lib")

namespace utility {
	namespace dump {
		namespace minidump {
			typedef BOOL(WINAPI* MINIDUMPWRITEDUMP)( // Callback 함수의 원형
				HANDLE hProcess,
				DWORD dwPid,
				HANDLE hFile,
				MINIDUMP_TYPE DumpType,
				CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
				CONST PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
				CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam);

			LPTOP_LEVEL_EXCEPTION_FILTER PreviousExceptionFilter = NULL;

			std::string MiniDump::name_key;

			typedef struct _MiniDumpInfo
			{
				DWORD threadId;
				EXCEPTION_POINTERS* pexception;
			}MiniDumpInfo, * PMiniDumpInfo;

			static const size_t TRHEAD_STACK_SIZE = 1024 * 1024 * 2;

			MiniDump::MiniDump()
			{
			}
			MiniDump::~MiniDump()
			{
			}

			unsigned __stdcall
				MiniDump::CallCreateDumpFile(void* arg)
			{
				struct _EXCEPTION_POINTERS* exceptionInfo = static_cast<struct _EXCEPTION_POINTERS*>(arg);
				return CreateDumpFile(exceptionInfo);
			}

			BOOL
				MiniDump::Begin()
			{
				SetErrorMode(SEM_FAILCRITICALERRORS);

				PreviousExceptionFilter = SetUnhandledExceptionFilter(UnHandledExceptionFilter);

				return true;
			}

			VOID
				MiniDump::SetName(std::string name)
			{
				name_key = name;
			}

			BOOL
				MiniDump::End(VOID)
			{
				SetUnhandledExceptionFilter(PreviousExceptionFilter);

				return true;
			}

			LONG WINAPI
				MiniDump::UnHandledExceptionFilter(struct _EXCEPTION_POINTERS* exceptionInfo)
			{
				// 스택 오버 플로우 일 경우
				if (exceptionInfo->ExceptionRecord->ExceptionCode == EXCEPTION_STACK_OVERFLOW)
				{
					// 덤프를 찍기 위한 쓰레드 생성
					HANDLE hThread = (HANDLE)_beginthreadex(0, TRHEAD_STACK_SIZE, MiniDump::CallCreateDumpFile, (void*)exceptionInfo, 0, nullptr);

					WaitForSingleObject(hThread, INFINITE);
					CloseHandle(hThread);

					return EXCEPTION_EXECUTE_HANDLER;
				}

				LONG Result = CreateDumpFile(exceptionInfo);

				return Result;
			}

			LONG WINAPI
				MiniDump::CreateDumpFile(struct _EXCEPTION_POINTERS* exceptionInfo)
			{
				// Windows 2000 이전에는 따로 DBGHELP를 배포해서 설정해 주어야 한다.
				HMODULE DllHandle = LoadLibrary(_T("DBGHELP.DLL"));

				if (DllHandle)
				{
					// DBGHELP.DLL에서 MiniDumpWriteDump 함수를 추출합니다.
					MINIDUMPWRITEDUMP Dump = (MINIDUMPWRITEDUMP)GetProcAddress(DllHandle, "MiniDumpWriteDump");

					if (Dump)
					{
						char        DumpPath[MAX_PATH] = { 0, };
						SYSTEMTIME    SystemTime;

						GetLocalTime(&SystemTime);

						sprintf_s(DumpPath, MAX_PATH, "%s_%d-%d-%d %d_%d_%d.dmp",
							name_key.c_str(),
							SystemTime.wYear,
							SystemTime.wMonth,
							SystemTime.wDay,
							SystemTime.wHour,
							SystemTime.wMinute,
							SystemTime.wSecond);

						HANDLE FileHandle = CreateFileA(
							DumpPath,
							GENERIC_WRITE,
							FILE_SHARE_WRITE,
							NULL, CREATE_ALWAYS,
							FILE_ATTRIBUTE_NORMAL,
							NULL);

						if (FileHandle != INVALID_HANDLE_VALUE)
						{
							_MINIDUMP_EXCEPTION_INFORMATION MiniDumpExceptionInfo;

							MiniDumpExceptionInfo.ThreadId = GetCurrentThreadId();
							MiniDumpExceptionInfo.ExceptionPointers = exceptionInfo;
							MiniDumpExceptionInfo.ClientPointers = NULL;

							BOOL Success = Dump(
								GetCurrentProcess(),
								GetCurrentProcessId(),
								FileHandle,
								MiniDumpNormal,
								&MiniDumpExceptionInfo,
								NULL,
								NULL);

							if (Success)
							{
								CloseHandle(FileHandle);

								return EXCEPTION_EXECUTE_HANDLER;
							}
						}

						CloseHandle(FileHandle);
					}
				}

				return EXCEPTION_CONTINUE_SEARCH;
			}
		}
	}
} /// utility::dump::minidump

#endif