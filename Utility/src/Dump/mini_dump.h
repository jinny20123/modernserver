﻿#ifndef MODERN_UTILITY_DUMP_MINIDUMP_MINIDUMP_H_
#define MODERN_UTILITY_DUMP_MINIDUMP_MINIDUMP_H_

#ifdef _WIN32

#include <Windows.h>
#include <DbgHelp.h>
#include <fstream>
#include <cstring>

namespace utility {
	namespace dump {
		namespace minidump {
			class MiniDump
			{
			public:
				MiniDump();
				virtual ~MiniDump();

				static BOOL	Begin();
				static VOID SetName(std::string name);
				static BOOL End(VOID);

			private:
				static void writeStackInfo(PCONTEXT pContext, std::fstream& flog_stream);
				static BOOL getLogicalAddress(LPVOID pAddr, LPTSTR sModule, DWORD len, DWORD& dwSection, DWORD& dwOffset);
				static LONG WINAPI UnHandledExceptionFilter(struct _EXCEPTION_POINTERS* exceptionInfo);
				static LONG WINAPI CreateDumpFile(struct _EXCEPTION_POINTERS* exceptionInfo);
				static unsigned __stdcall CallCreateDumpFile(void* arg);

				static std::string name_key;
			};

#else
class MiniDump
{
public:
	MiniDump();
	virtual ~MiniDump();

	static BOOL	Begin(VOID) { return TRUE; }
	static BOOL End(VOID) { return TRUE; }
};
#endif
		}
	}
} /// utility::dump::minidump
#endif /// MODERN_UTILITY_DUMP_MINIDUMP_MINIDUMP_H_
