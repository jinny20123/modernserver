﻿#ifndef MODERN_UTILITY_POLICY_CREATE_PROTOTYPECREATEPOLICY_H_
#define MODERN_UTILITY_POLICY_CREATE_PROTOTYPECREATEPOLICY_H_


namespace utility { namespace policy { namespace create { 

template<typename T1>
class PrototypeCreatePolicy 
{
public: 
	PrototypeCreatePolicy(T1 * ptr = nullptr) : pprototype(ptr) {};
	T1 * Create();
	void SetPrototype(T1 ptr) { pprototype = ptr; }
	T1 * GetPrototype() { return pprototype; }
protected:
	~PrototypeCreatePolicy() {};
private :
	T1 * pprototype;
};

template<typename T1>
T1 *
PrototypeCreatePolicy<T1>::Create()
{
	return pprototype ? pprototype->Clone() : nullptr;
}
		
}}} /// utility::policy::Create
#endif /// MODERN_UTILITY_POLICY_CREATE_PROTOTYPECREATEPOLICY_H_
