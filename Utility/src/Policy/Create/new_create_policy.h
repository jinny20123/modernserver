﻿#pragma once
#include <vector>
#include <mutex>

namespace utility { namespace policy { namespace create { 

/// NoneType Creator를 정의합니다. 
template<typename OBJECT>
class NoneCreatePolicy 
{
public: 
	NoneCreatePolicy() {};

	OBJECT * Create();

protected:
	~NoneCreatePolicy() {};
};

template<typename OBJECT>
OBJECT *
NoneCreatePolicy<OBJECT>::Create()
{
	return nullptr;
}


/// NewType Creator를 정의합니다. 
/// new 할당자를 사용하여 객체를 할당받습니다.
template<typename OBJECT>
class NewCreatePolicy 
{
public: 
	NewCreatePolicy() 
	{
		allocate_size_ = 256;
		ptr_ = nullptr;
		alloc();
		
	};
	OBJECT * Create();
protected:
	~NewCreatePolicy() {};
private :
	static const size_t max_allocate_bytes = 1024 * 1024 * 128; // 128M
	void alloc()
	{
		if (nullptr != ptr_)
		{
			ptr_container_.push_back(ptr_);

			if( max_allocate_bytes < ( allocate_size_ * sizeof(OBJECT) ) )
			{ allocate_size_ = static_cast<size_t>(static_cast<double>(allocate_size_) * 1.5); }
		}

		ptr_ = reinterpret_cast<OBJECT *>( ::operator new( allocate_size_ * sizeof(OBJECT) ) );
		cur_pos_ = 0;
	}

	size_t cur_pos_;
	size_t allocate_size_;
	OBJECT * ptr_;
	std::vector<void *> ptr_container_;
	std::mutex mutex_;
};

template<typename OBJECT>
OBJECT *
NewCreatePolicy<OBJECT>::Create()
{
	OBJECT * ptr = nullptr;
	{
		std::lock_guard<std::mutex> lock(mutex_);
	
		if ( cur_pos_ >= allocate_size_ )
		{ 
			alloc(); 
		}

		ptr = new(ptr_ + cur_pos_) OBJECT;
		cur_pos_++;
	}
	return ptr;
}

/// ProtoType Creator를 정의합니다. 
/// ProtoType을 통해서 할당 받습니다. 
template<typename OBJECT>
class PrototypeCreatePolicy 
{
public: 
	PrototypeCreatePolicy(OBJECT * pObj = nullptr)
	{
		SetProtoType(pObj);
	}
	~PrototypeCreatePolicy() {}

	OBJECT * Create();
	OBJECT * GetProtoType() { return pPrototype_; }
	void SetProtoType(OBJECT * pObj) { pPrototype_ =  pObj; }
private :
	OBJECT * pPrototype_;
};

template<typename OBJECT>
OBJECT *
PrototypeCreatePolicy<OBJECT>::Create()
{
	return pPrototype_ != nullptr ? static_cast<OBJECT*>(pPrototype_->Clone()) : nullptr;
}
		
}}} /// utility::policy::create

