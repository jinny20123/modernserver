﻿#ifndef MODERN_UTILITY_POLICY_FACTORY_POOLFACTORYPOLICY_H_
#define MODERN_UTILITY_POLICY_FACTORY_POOLFACTORYPOLICY_H_

#include <mutex>
#include <shared_mutex>
#include <fstream>
#include "../Create/new_create_policy.h"
#include "../../Time/TimeToString.h"
#include "../../Pool/object_pool.h"

#define _MEMORY_TRACE


namespace utility { namespace policy { namespace factory { 

/**
	@class : PoolFactoryPolicy
	@date : 2015-09-20
	@brief : PoolFactoryPolicy는 Creator를 상속받아서 해당 방식으로 객체를 생성합니다. 
			 Creator 객체 생성 담당
			 PoolFactoryPolicy 객체를 Pool을 통해서 관리되도록 관리합니다. 
	@author : jinny20123(안 정웅)
*/
template<typename Obj, typename Creator, template<typename T> class ThreadType >
class PoolFactoryPolicy : 
	public Creator
{
public: 
	PoolFactoryPolicy() { count_ = 0; next_size_ = 200; }; 
	~PoolFactoryPolicy() {};

	Obj * Create();
	void Destroy(Obj * ptr);
private :
	typedef utility::pool::ObjectPool<Obj, ThreadType>
			Pool;
	Pool				pool_;
	size_t				next_size_;
	size_t				count_;
	std::shared_mutex	mutex_;
};

template<typename Obj, typename Creator, template<typename T> class ThreadType >
Obj *
PoolFactoryPolicy<Obj, Creator, ThreadType>::Create()
{
	Obj * ptr = nullptr;
	pool_.Acquire(ptr);
	if (ptr == nullptr)
	{
		ptr = Creator::Create();	

		/// 메모리 추적을 위한 작업
#ifdef _MEMORY_TRACE
		{
//			std::unique_lock lock(mutex_);
			count_++;
			if(count_ > next_size_)
			{
				std::string strtime;
				time::TimeToString::GetCurrentTimeToString(strtime);
				/// 파일에 찍는다.
				std::fstream fs;
				fs.open("MemoryTrace.log", std::fstream::in | std::fstream::out | std::fstream::app);
				fs << strtime.c_str() << " " << typeid(this).name() << " count : " << count_ << std::endl;
				fs.close();
				next_size_ = static_cast<size_t>(static_cast<double>(next_size_) * 1.5f);
			}
		}
#endif // _MEMORY_TRACE
	}

	return ptr;
}

template<typename Obj, typename Creator, template<typename T> class ThreadType >
void 
PoolFactoryPolicy<Obj, Creator, ThreadType>::Destroy(Obj * ptr)
{
	pool_.Restore(ptr);
}
		
}}} /// utility::policy::factory
#endif // MODERN_UTILITY_POLICY_FACTORY_POOLFACTORYPOLICY_H_
