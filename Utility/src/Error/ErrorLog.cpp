﻿#include "ErrorLog.h"
#include <memory>
namespace utility {
	namespace error {
		/// 내부에서 사용할 변수를 정의합니다.
		/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
		struct ErrorLog::Private
		{
			std::fstream f;
		};

		ErrorLog::ErrorLog()
		{
			This = std::make_shared<Private>();
			This->f.open("error.log", std::fstream::app | std::fstream::out);
		}
		ErrorLog::~ErrorLog()
		{
			This->f.close();
		}

		std::fstream&
			ErrorLog::Get()
		{
			return This->f;
		}

		void
			ErrorLog::Flush()
		{
			This->f.flush();
		}
	}
} /// utility::error