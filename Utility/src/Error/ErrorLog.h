﻿#pragma once
#include <memory>
#include <fstream>

namespace utility {
	namespace error {
		class ErrorLog
		{
		public:
			ErrorLog();
			virtual ~ErrorLog();
			std::fstream& Get();
			void Flush();
		private:
			struct Private;
			std::shared_ptr<Private> This;
		};
	}
} /// utility::error