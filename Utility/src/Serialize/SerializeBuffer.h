﻿#pragma once

#include <memory>
#include "../macro.h"

namespace utility { namespace object {
struct Argument;
}}

namespace utility { namespace serialize { 


/** 
	@class : SerializeBuffer 
	@date : 2016-04-15
	@brief : 
	@author : jinny20123(안 정웅)
*/
class SerializeBuffer 
{
public: 
	typedef ::utility::object::Argument
			Argument;

	SerializeBuffer();
	SerializeBuffer(char * buffer);
	virtual ~SerializeBuffer();

	virtual bool Capture(Argument && arg);
	virtual void Release();

	template<typename T>
	size_t Input(T t)
	{

	}

private :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};

	
}} /// utility::serialize