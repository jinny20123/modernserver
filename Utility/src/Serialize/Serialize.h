﻿#pragma once

#include <memory>
namespace utility {
	namespace serialize {
		/**
			@class : Serialize
			@date : 2016-04-15
			@brief :
			@author : jinny20123(안 정웅)
		*/
		class Serialize
		{
		public:
			Serialize();
			virtual ~Serialize();
		private:
			struct Impl;
			std::unique_ptr<Impl> mImpl;
		};
	}
} /// utility::serialize