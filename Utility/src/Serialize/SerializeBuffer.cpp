﻿#include "SerializeBuffer.h"
#include <cstdint>

namespace utility {
	namespace serialize {
		/// 내부에서 사용할 변수를 정의합니다.
		/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
		struct SerializeBuffer::Impl
		{
			Impl() {}
			char* buffer;
			char* allocate;
			size_t pos;

			void Capture(char* buffer = nullptr);
			void Release();
		};

		SerializeBuffer::SerializeBuffer()
		{
			MAKE_UNIQUE(mImpl, Impl);
		}

		SerializeBuffer::SerializeBuffer(char* buffer)
		{
			MAKE_UNIQUE(mImpl, Impl);
		}

		SerializeBuffer::~SerializeBuffer()
		{}

		bool
			SerializeBuffer::Capture(Argument&& arg)
		{
			mImpl->Capture();
			return true;
		}

		void
			SerializeBuffer::Release()
		{
			mImpl->Release();
		}

		void
			SerializeBuffer::Impl::Capture(char* buffer)
		{
			if (nullptr == buffer)
			{
				Release();
				buffer = new char[512];
				allocate = buffer;
			}
			else
			{
				buffer = buffer;
				allocate = nullptr;
				pos = 0;
			}
			pos = 0;
		}

		void
			SerializeBuffer::Impl::Release()
		{
			if (nullptr != allocate)
			{
				delete[] allocate;
			}
		}
	}
} /// utility::serialize