﻿#include "Serialize.h"
#include "../macro.h"
namespace utility {
	namespace serialize {
		/// 내부에서 사용할 변수를 정의합니다.
		/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
		struct Serialize::Impl
		{
			Impl() {}
		};

		Serialize::Serialize()
		{
			MAKE_UNIQUE(mImpl, Impl);
		}
		Serialize::~Serialize()
		{
		}
	}
} /// utility::serialize