﻿#ifndef MODERN_UTILITY_TYPE_TYPEOP_H_
#define MODERN_UTILITY_TYPE_TYPEOP_H_

namespace utility {
	namespace type {
		template<typename T>
		class TypeOp
		{
		public:
			typedef T						ArgT;
			typedef T						BareT;
			typedef T const					ConstT;
			typedef T& RefT;
			typedef T& RefTBareT;
			typedef T const& RefConstT;
			typedef T* PtrT;
		};

		/// 참조자 타입을 위한 템플릿 특수화
		template<typename T>
		class TypeOp<T&>
		{
		public:
			typedef T& ArgT;
			typedef typename TypeOp<T>::BareT	BareT;
			typedef T const						ConstT;
			typedef T& RefT;
			typedef typename TypeOp<T>::BareT& RefBareT;
			typedef T const& RefConstT;
			typedef T* PtrT;
		};

		/// 포인터 타입을 위한 템플릿 특수화
		template<typename T>
		class TypeOp<T*>
		{
		public:
			typedef T* ArgT;
			typedef typename TypeOp<T>::BareT	BareT;
			typedef T const						ConstT;
			typedef T*& RefT;
			typedef typename TypeOp<T>::BareT& RefBareT;
			typedef T const& RefConstT;
			typedef T* PtrT;
		};
	}
} /// utility::type
#endif /// MODERN_UTILITY_TYPE_TYPEOP_H_
