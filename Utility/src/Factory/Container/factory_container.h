﻿#pragma once
#include <unordered_map>
#include <string>
#include "../factory.h"

namespace utility {
	namespace factory {
		namespace container {
			template<typename Interface, typename Key = std::string>
			class FactoryContainer
			{
			public:
				using FactoryInterface = ::utility::factory::FactoryInterface<Interface>;
				using InterFaceContainer = std::unordered_map<Key, FactoryInterface*>;

				FactoryContainer() {};
				virtual ~FactoryContainer() {
					for (auto& value : container_)
					{
						if (nullptr != value.second)
						{
							if (nullptr != value.second)
							{
								delete(value.second);
								value.second = nullptr;
							}
						}
					}
				};

				FactoryInterface* Get(const Key& key) {
					auto iter = container_.find(key);
					if (iter != container_.end())
					{
						return iter->second;
					}

					return nullptr;
				}
				bool Set(const Key& key, FactoryInterface* pObj)
				{
					auto ret = container_.insert(InterFaceContainer::value_type(key, pObj));
					return ret.second;
				}
			private:
				InterFaceContainer	container_;
			};
		}
	}
} /// utility::factory::container

#define SKFC_TYPEDEF(Interface_name)													\
typedef Interface_name																	\
		Interface;																		\
typedef std::string																		\
		key_type;																		\
typedef ::utility::factory::container::FactoryContainer<Interface>			\
		FactoryContainer

#define FC_TYPEDEF(Interface_name, key)													\
typedef Interface_name																	\
		Interface;																		\
typedef key																				\
		key_type;																		\
typedef ::utility::factory::container::FactoryContainer<Interface, key>	\
		FactoryContainer

#define KEY_FACTORY_SET(fac, key, object)												\
fac->Set(key, new ::utility::factory::Factory< Interface, object> )

#define KEY_FACTORY_SET_4(fac, key, Interface, object)										\
fac->Set(key, new ::utility::factory::Factory< Interface, object> )

#define STATIC_FACTORY(classname, variable)																\
template<typename T>																					\
::utility::factory::Factory<typename classname<T>::Interface, T> classname<T>::variable
