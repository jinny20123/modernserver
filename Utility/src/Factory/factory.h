﻿#pragma once

#include <memory>
#include "../Policy/Create/new_create_policy.h"
#include "../Policy/Factory/pool_factory_policy.h"
#include "../Type/type.h"
#include "../Object/SingleThread.h"
#include "../object/MultiThread.h"

namespace utility {
	namespace factory {
		/// factory를 인터페이스별로 구분하기 위한 부모 클래스
		template <typename TInterface>
		class FactoryInterface
		{
		public:
			virtual TInterface* Create() = 0;
			virtual void Destroy(TInterface* pobj) = 0;
		};

		/**
			@class : FactoryImp
			@date : 2015-09-20
			@brief : 실제 팩토리 구현부입니다.
					 FactoryPolicy를 상속받아서 처리됩니다.
					 FactoryPolicy는 팩토리의 운영정책이 등록됩니다.
					 CPolicy는 객체의 생성 방식을 결정합니다.
					 ThreadType은 멀티쓰레드인지 여부를 판단하여 동작합니다.(싱글쓰레드로직에 락비용 제거하기 위한 작업)
			@author : jinny20123(안 정웅)
		*/
		template<typename INTERFACE, typename OBJECT,
			template<typename T> class ThreadType,
			template<typename Obj, typename Creator, template<typename T> class ThreadType> class FactoryPolicy,
			template<typename Obj> class CPolicy = ::utility::policy::create::NewCreatePolicy
		>
			class FactoryImp
			: public FactoryPolicy<INTERFACE, CPolicy<OBJECT>, ThreadType >
		{
		public:
			typedef utility::type::TypeOp<OBJECT>
				ObjType;
		};

		/**
			@class : Factory
			@date : 2015-09-20
			@brief : 외부에서는 팩토리클래스를 통해서 접근합니다.
			@author : jinny20123(안 정웅)
		*/
		template<typename INTERFACE, typename OBJECT,
			template<typename T> class ThreadType = ::utility::object::MultiThread,
			template<typename Obj> class CPolicy = ::utility::policy::create::NewCreatePolicy >
		class Factory
			: public FactoryInterface<INTERFACE>
		{
		public:
			Factory() { pimp_ = std::make_shared<FacImp>(); }
			virtual ~Factory() {}

			virtual INTERFACE* Create() { return pimp_->Create(); }
			virtual void Destroy(INTERFACE* p) { pimp_->Destroy(p); }
		private:
			typedef FactoryImp<INTERFACE, OBJECT, ThreadType, utility::policy::factory::PoolFactoryPolicy, CPolicy>
				FacImp;

			std::shared_ptr<FacImp> pimp_;
		};

		/**
		@class : Factory
		@date : 2015-09-20
		@brief : 외부에서는 팩토리클래스를 통해서 접근합니다.
				 템플릿 특수화를 통해서 PrototypeCreatePolicy일때의 특수한 처리를 담당합니다.
		@author : jinny20123(안 정웅)
		*/
		template<typename INTERFACE, typename OBJECT,
			template<typename T> class ThreadType>
		class Factory<INTERFACE, OBJECT, ThreadType, ::utility::policy::create::PrototypeCreatePolicy >
			: public FactoryInterface<INTERFACE>
		{
		public:
			typedef FactoryImp<INTERFACE, OBJECT, ThreadType, utility::policy::factory::PoolFactoryPolicy, ::utility::policy::create::PrototypeCreatePolicy>
				FacImp;

			Factory() { pimp_ = std::make_shared<FacImp>(); }
			virtual ~Factory() {}

			virtual INTERFACE* Create() { return pimp_->Create(); }
			virtual void Destroy(INTERFACE* p) { pimp_->Destroy(p); }
			virtual FacImp* GetCreator() { return pimp_.get(); }
		private:

			std::shared_ptr<FacImp> pimp_;
		};
	}
} /// :utility::factory
