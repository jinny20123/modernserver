﻿#ifndef MODERN_UTILITY_OBJECT_MESSAGE_H_
#define MODERN_UTILITY_OBJECT_MESSAGE_H_

#include <cstdint>

namespace utility {
	namespace object {
		struct message
		{
		public:
			message() { }
			message(int32_t h, void* p) : Handle_(h), pValue_(p)
			{}

			uint32_t Handle_;
			void* pValue_;
		};
	}
} /// utility::object
#endif /// MODERN_UTILITY_OBJECT_MESSAGE_H_
