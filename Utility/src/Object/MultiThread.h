﻿#pragma once

#include "queue.h"

namespace  utility {
	namespace object {
		template<typename T>
		class MultiThread
		{
		public:
			MultiThread() {};
			virtual ~MultiThread() {};
			using Queue = utility::object::Queue<T>;
		};
	}
} ///  utility::object