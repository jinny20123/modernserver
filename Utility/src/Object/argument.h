﻿#pragma once
#include <boost/assert.hpp>
#include <string>
#include <cstdint>

/// 전달되는 인수에 대한 상위 인터페이스를 제공합니다.
namespace utility {
	namespace object {
#define TRUE_IS_ENABLE()		\
	virtual bool IsEnable()		\
	{ return true; }			\

		struct Argument
		{
			virtual ~Argument() {
			}
			TRUE_IS_ENABLE()
		};

		/**
			@class : ArgumentUtil
			@date : 2015/04/30
			@brief : Argument를 사용할때 공통되는 부분을 묶습니다.
			@author : jinny20123(안 정웅)
		*/
		class ArgumentUtil
		{
		public:
			template<typename TArg, typename TOut>
			static void
			CastArgument(TArg* arg, TOut*& pout)
			{
				if (nullptr == arg) { pout = nullptr; return; }
				pout = dynamic_cast<TOut*>(arg);
			}
		};
	}
} /// utility::object
