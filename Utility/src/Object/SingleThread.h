﻿#pragma once
#include "queue.h"

namespace  utility { namespace object { 

template<typename T>
class SingleThread 
{
public: 
	SingleThread() {};
	virtual ~SingleThread() {};
	using Queue = utility::object::Queue<T>;

};
	
}} ///  utility::object