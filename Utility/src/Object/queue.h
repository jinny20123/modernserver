﻿#ifndef MODERN_UTILITY_OBJECT_QUEUE_H_
#define MODERN_UTILITY_OBJECT_QUEUE_H_

#include "../Type/type.h"
#include <tbb/concurrent_queue.h>
#include <queue>
#include <mutex>
#include <shared_mutex>
#include <condition_variable>
#include <boost/serialization/singleton.hpp>

namespace utility {
	namespace object {
		template<typename T1, typename T2>
		class StandardPopPolicy
		{
		public:
			static bool PopProcess(T1& object, T2& con)
			{
				con.pop(object);
				return true;
			}
		};

		template<typename OBJ, typename CON>
		class StdPopPolicy
		{
		public:
			static bool PopProcess(OBJ& object, CON& con)
			{
				object = con.front();
				con.pop();
				return true;
			}
		};


		template<typename T1, typename T2>
		class TryPopPolicy
		{
		public:
			static bool PopProcess(T1& object, T2& con)
			{
				return con.try_pop(object);
			}
		};

		template <
			typename object,
			class Con = std::queue<object>,
			class PopPolicy = StdPopPolicy<object, Con>
		>
		class Queue
		{
		public:
			Queue() {};
			virtual ~Queue() {};
			typedef int64_t
				size_type;

			typename type::TypeOp<object>::ArgT Pop();
			bool Pop(typename type::TypeOp<object>::RefT obj);
			void Push(typename type::TypeOp<object>::RefT obj);
			bool Empty();
			size_type Size();
		private:
			std::condition_variable_any					cv_;
			std::shared_mutex							shared_mutex_;
			Con											container_;
			size_type									container_count_;
		};

		template< typename object, class Con, class PopPolicy>
		typename type::TypeOp<object>::ArgT
		Queue<object, Con, PopPolicy>::Pop()
		{
			object obj;
			{
				std::unique_lock<std::shared_mutex> lk(shared_mutex_);
				
				cv_.wait(lk, [this] {return !container_.empty(); });
				PopPolicy::PopProcess(obj, container_);
			}
			return obj;
		}

		template< typename object, class Con, class PopPolicy>
		bool
		Queue<object, Con, PopPolicy>::Pop(typename type::TypeOp<object>::RefT obj)
		{
			std::unique_lock<std::shared_mutex> lk(shared_mutex_);
			cv_.wait(lk, [this] {return !container_.empty(); });
			return PopPolicy::PopProcess(obj, container_);
		}

		template< typename object, class Con, class PopPolicy>
		void
		Queue<object, Con, PopPolicy>::Push(typename type::TypeOp<object>::RefT obj)
		{
			{
				std::unique_lock<std::shared_mutex> lk(shared_mutex_);
				container_.push(obj);
			}
			cv_.notify_one();
		}
		template< typename object, class Con, class PopPolicy>
		bool
		Queue<object, Con, PopPolicy>::Empty()
		{
			std::shared_lock<std::shared_mutex> lk(shared_mutex_);
			return container_.empty();
		}

		template< typename object, class Con, class PopPolicy>
		typename Queue<object, Con, PopPolicy>::size_type
		Queue<object, Con, PopPolicy>::Size()
		{
			std::shared_lock<std::shared_mutex> lk(shared_mutex_);
			return container_.size();
		}

#define MakeQueue(obj, con, policy) \
utility::object::Queue<obj, con<obj>, utility::object::policy<obj, con<obj> > >
	}
} /// utility::object
#endif /// MODERN_UTILITY_OBJECT_QUEUE_H_
