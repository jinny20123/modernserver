﻿#pragma once

#include "../../../Utility/src/Factory/Container/factory_container.h"
namespace utility {
	namespace object {
		struct Argument;
	}
}
namespace utility {
	namespace object {
		/**
			@class : ArgumentFactory
			@date : 2015/04/30
			@brief :
			@author :
		*/
		template<typename Type>
		class ArgumentFactory
		{
		public:
			typedef ::utility::object::Argument
				Argument;
			typedef Argument
				Interface;

			ArgumentFactory();
			virtual ~ArgumentFactory();

			static Interface* Create()
			{
				auto pobj = factory_.Create();
				return pobj;
			}

			static Type* CreateT()
			{
				auto pobj = factory_.Create();
				return static_cast<Type*>(pobj);
			}

			static void Destroy(Interface* pobj)
			{
				factory_.Destroy(pobj);
			}
		private:
			static ::utility::factory::Factory<Interface, Type > factory_;
		};

		STATIC_FACTORY(ArgumentFactory, factory_);
	}
} /// utility::object