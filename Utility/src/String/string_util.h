﻿#ifndef MODERN_UTILITY_STRING_STRINGUTIL_H_
#define MODERN_UTILITY_STRING_STRINGUTIL_H_

#include <memory>
#include <vector>
#include <string>
namespace utility {
	namespace string {
		class StringUtil
		{
		public:
			typedef std::vector < std::string >
				TokenContainer;
			StringUtil();
			virtual ~StringUtil();

			static TokenContainer SplitString(std::string input, std::string delimiter);
		};
	}
} /// utility::string
#endif /// MODERN_UTILITY_STRING_STRINGUTIL_H_
