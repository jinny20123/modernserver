﻿#include "string_util.h"
#include <sstream>
namespace utility {
	namespace string {
		/// 내부에서 사용할 변수를 정의합니다.
		/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
		StringUtil::StringUtil()
		{
		}
		StringUtil::~StringUtil()
		{
		}

		StringUtil::TokenContainer
			StringUtil::SplitString(std::string input, std::string delimiter)
		{
			std::vector<std::string> output;
			size_t pos_index = 0;
			size_t maker_index = 0;

			while (maker_index != std::string::npos)
			{
				maker_index = input.find_first_of(delimiter, pos_index);

				if (maker_index != std::string::npos)
				{
					output.push_back(input.substr(pos_index, maker_index - pos_index));
					pos_index = maker_index + 1;
				}
				else
				{
					if (input.size() != pos_index)
					{
						output.push_back(input.substr(pos_index, input.size() - pos_index));
					}
				}
			}

			return output;
		}
	}
} /// utility::string