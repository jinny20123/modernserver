﻿
#if (_MSC_VER >= 1800)	// visual studio 2013부터 지원됩니다.  사용해야할 최종버전 - 아직 컴파일러가 지원하지 않습니다.
#define MAKE_UNIQUE(pointer,type, ...) pointer = std::make_unique<type>(__VA_ARGS__)	
#define OVERRIDE override 
#else
#define MAKE_UNIQUE(pointer,type, ...) pointer.reset(new type(__VA_ARGS__) )
#define OVERRIDE
#endif

#define SINGLETON_CALLFUNC(Type, CallName)	\
inline Type & CallName() { return Type::GetInstance(); }

#define TEMPLATE_SINGLETON_CALLFUNC(Type, CallName)	\
template<typename T>							\
Type<T> & CallName() { return Type<T>::GetInstance(); }
