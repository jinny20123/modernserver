﻿#include "DataBaseStatic.h"
#include "../../../Utility/src/macro.h"

namespace database {
	namespace static_ {
		/// 내부에서 사용할 변수를 정의합니다.
		/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
		struct DataBaseStatic::Impl
		{
			Impl() {}
			std::shared_ptr<Producer>						pproducer;
		};

		DataBaseStatic::DataBaseStatic()
		{
			MAKE_UNIQUE(mImpl, Impl);
			mImpl->pproducer = std::make_shared<Producer>();
		}
		DataBaseStatic::~DataBaseStatic()
		{
		}

#define GetFunction(classname, variable)	\
DataBaseStatic::classname				*	\
DataBaseStatic::Get##classname()			\
{											\
	return variable.get();					\
}

		GetFunction(Producer, mImpl->pproducer);
	}
} /// database::static_