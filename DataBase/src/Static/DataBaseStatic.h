﻿#pragma once
#include <boost/serialization/singleton.hpp>
#include <memory>
#include "../../../Utility/src/Engine/Processor/producer.hpp"

namespace database {
	namespace static_ {
		/**
			@class : DataBaseStatic
			@date : 2016-10-04
			@brief : 데이터베이스 라이브러리에서 싱글톤으로 접근할 객체들의 모음
			@author : jinny20123(안 정웅)
		*/
		class DataBaseStatic
			: public boost::serialization::singleton<DataBaseStatic>
		{
		public:
			typedef ENGINE_PRODUCER(::utility::object::message)
				Producer;

			DataBaseStatic();
			virtual ~DataBaseStatic();

			Producer* GetProducer();
		private:
			struct Impl;
			std::unique_ptr<Impl> mImpl;
		};

		inline DataBaseStatic& DATABASE_STATIC()
		{
			return DataBaseStatic::get_mutable_instance();
		}
	}
} /// database::static_