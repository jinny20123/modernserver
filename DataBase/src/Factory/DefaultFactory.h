﻿#pragma once
#include "../../../Utility/src/macro.h"
#include "../../../Utility/src/Factory/Container/factory_container.h"
#include <memory>
#include <boost/serialization/singleton.hpp>

namespace utility { namespace object {
	struct Argument;
}}
namespace database { namespace factory { 


/** 
	@class : DefaultFactory 
	@date : 2016-09-21
	@brief : 일반적인 방법은 생성되는 객체들은 해당 Factory객체에서 처리 할 수 있도록 합니다. 
	@author : jinny20123(안 정웅)
*/
template<typename Type, typename Interface = Type>
class DefaultFactory 
	: private boost::serialization::singleton< DefaultFactory<Type, Interface> >
{
public:
	typedef ::utility::object::Argument
			Argument; 

	DefaultFactory() {};
	virtual ~DefaultFactory() {};

	Interface * Create(Argument * arg = nullptr)
	{
		auto pobj = factory_.Create();
		pobj->Capture(arg);
		return pobj;
	}

	void Destroy( Interface * pobj)
	{
		pobj->Release();
		factory_.Destroy(pobj);
	}
	
	static DefaultFactory<Type, Interface> & GetInstance()
	{
		return get_mutable_instance();
	}
private :
	::utility::factory::Factory<Interface, Type > factory_;
};

	
}} /// database::factory