﻿#include "QueryProcessor.h"
#include "../../../../Utility/src/macro.h"
#include "Handler/IHandler.h"
#include "../../Session/ISession.h"
#include "../Query/QueryArgument.h"
#include <thread>
#include <unordered_map>

namespace database {
	namespace logic {
		namespace processor {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct QueryProcessor::Impl
			{
				Impl() {}

				///< thread id 별로 Connection Session을 생성합니다.

				
				typedef std::unordered_map<size_t, ::database::session::ISession*>
					SessionContainer;

				std::shared_ptr<IHandler> phandler_;
				SessionContainer session_con_;
				MakeSessionFun make_fun_;

				void QueryRequestSessionSet(Argument* arg)		///< 해당 스레드에 해당하는 세션을 정해줍니다.
				{
					typedef ::database::logic::query::QueryRequestArgument
						QueryRequestArgument;
					QueryRequestArgument* mqr_arg = nullptr;
					::utility::object::ArgumentUtil::CastArgument(arg, mqr_arg);
					auto itr = session_con_.find(std::hash<std::thread::id>()(std::this_thread::get_id()));
					if (itr != session_con_.end())
					{
						mqr_arg->session_ = itr->second;
					}
				}
			};

			QueryProcessor::QueryProcessor(ProducerObj* pproducer, size_t thread_count, std::shared_ptr<IHandler> phandler, MakeSessionFun make_fun)
				: Consumer(pproducer, thread_count)
			{
				MAKE_UNIQUE(mImpl, Impl);
				mImpl->phandler_ = phandler;
				mImpl->make_fun_ = make_fun;

				ThreadRun();
			}
			QueryProcessor::~QueryProcessor()
			{
			}
			void
				QueryProcessor::Prepare()
			{
				///< 쓰레드별로 DateBase Session을 만들어 넣습니다.
				size_t tid = std::hash<std::thread::id>()(std::this_thread::get_id());
				mImpl->session_con_.insert(Impl::SessionContainer::value_type(tid, mImpl->make_fun_()));
			}

			void
				QueryProcessor::Handle(const object& obj)
			{
				if (nullptr == obj.pValue_)
				{
					// 로그 삽입
				}

				Argument* arg = static_cast<Argument*>(obj.pValue_);

				switch (obj.Handle_)
				{
				case IHandler::HANDLE_REQUESTQUERY:
					mImpl->QueryRequestSessionSet(arg);
					mImpl->phandler_->OnQuery(arg);
					break;
				}
			}
		}
	}
} /// database::logic::processor