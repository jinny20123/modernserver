﻿
#include "MysqlHandler.h"
#include "../../../Query/QueryArgument.h"
#include "../../../Query/IQuery.h"

namespace database { namespace logic { namespace processor { namespace handler { namespace object { 


/// 내부에서 사용할 변수를 정의합니다. 
/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
struct MysqlHandler::Impl
{
	Impl() {}
}; 

MysqlHandler::MysqlHandler()
{
	MAKE_UNIQUE(mImpl, Impl);
}
MysqlHandler::~MysqlHandler()
{
	
}

void 
MysqlHandler::OnQuery(Argument * arg)
{
	typedef ::database::logic::query::QueryRequestArgument
			MysqlQueryRequestArgument;
	MysqlQueryRequestArgument * mqr_arg = nullptr;

	::utility::object::ArgumentUtil::CastArgument(arg, mqr_arg);

	// 파라미터가 비정상적이면 체크 로그를 남기자!
	if (!mqr_arg) { return; }
	if (!mqr_arg->session_ || !mqr_arg->pquery_) { return; }

	mqr_arg->pquery_->Request(mqr_arg->session_);

	// 세션작업을 완료한 세션포인터를 해제 합니다. 
	mqr_arg->session_ = nullptr;


}
	
}}}}} /// database::logic::processor::handler::object