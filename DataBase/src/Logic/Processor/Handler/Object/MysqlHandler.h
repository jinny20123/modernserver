﻿#pragma once

#include <memory>
#include "../IHandler.h"
namespace database {
	namespace logic {
		namespace processor {
			namespace handler {
				namespace object {
					/**
						@class : MysqlHandler
						@date : 2016-09-22
						@brief :
						@author : jinny20123(안 정웅)
					*/
					class MysqlHandler
						: public database::logic::processor::handler::IHandler
					{
					public:
						MysqlHandler();
						virtual ~MysqlHandler();

						virtual void OnQuery(Argument* arg) OVERRIDE;
					private:
						struct Impl;
						std::unique_ptr<Impl> mImpl;
					};
				}
			}
		}
	}
} /// database::logic::processor::handler::object