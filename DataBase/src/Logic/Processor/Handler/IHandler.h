﻿#pragma once

#include <memory>
#include "../../../../../Utility/src/macro.h"
#include "../../../../../Utility/src/Object/argument_fwd.h"

namespace database {
	namespace logic {
		namespace processor {
			namespace handler {
				class IHandler
				{
				public:
					typedef ::utility::object::Argument
						Argument;

					virtual ~IHandler() {};

					/// IHandler 인터페이스를 정의합니다.
					virtual void OnQuery(Argument* arg) = 0;		///< 쿼리를 실행합니다.

					enum HANDEL_TYPE
					{
						HANDLE_START = 0,
						HANDLE_REQUESTQUERY = HANDLE_START,
						HANDLE_CLOSE,
					};
				};
			}
		}
	}
} /// database::logic::processor::handler
