﻿#pragma once

#include "../../../../Utility/src/macro.h"
#include "../../../../Utility/src/Engine/Processor/consumer.hpp"
#include "../../../../Utility/src/Object/argument_fwd.h"
#include <memory>
#include <functional>

namespace database {
	namespace logic {
		namespace processor {
			namespace handler {
				class IHandler;
			}
		}
	}
}

namespace database {
	namespace session {
		class ISession;
	}
}

namespace database {
	namespace logic {
		namespace processor {
			/**
				@class : QueryProcessor
				@date : 2016-09-22
				@brief : 쿼리를 전달받아서 처리하는 프로세서를 정의합니다.
						thread_count를 통해서 쓰레드 갯수를 정의합니다.
				@author : jinny20123(안 정웅)
			*/
			class QueryProcessor
				: public CONSUMER(utility::object::message)
			{
			public:
				typedef ::utility::object::Argument
					Argument;
				typedef ::database::logic::processor::handler::IHandler
					IHandler;
				typedef std::function<::database::session::ISession * ()>
					MakeSessionFun;

				QueryProcessor(ProducerObj * producer, size_t thread_count, std::shared_ptr<IHandler> pHandler, MakeSessionFun make_fun);
				virtual ~QueryProcessor();

				virtual void Prepare() OVERRIDE;
				virtual void Handle(const object & obj) OVERRIDE;
			protected:
			private:
				struct Impl;
				std::unique_ptr<Impl> mImpl;
			};
		}
	}
} /// database::logic::processor