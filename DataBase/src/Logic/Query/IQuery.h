﻿#pragma once

#include <memory>
#include "../../../../Utility/src/macro.h"

namespace database { namespace session { 
class ISession;
}}

namespace database { namespace logic { namespace query { 
/** 
	@class : IQuery 
	@date : 2016-09-22
	@brief : 사용자가 요청할 Query 디자인을 제공합니다. 
	@author : jinny20123(안 정웅)
*/
class IQuery 
{
public: 
	IQuery() {};
	virtual ~IQuery() {};

	virtual bool Request(database::session::ISession * psession) = 0;
	virtual bool Response() = 0;
};
	
}}} /// database::logic::query