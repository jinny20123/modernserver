﻿#pragma once

#include <memory>
#include "../../../Utility/src/Object/argument.h"

namespace database {
	namespace session {
		class ISession;
	}
}

namespace database {
	namespace logic {
		namespace query {
			class IQuery;

			typedef ::utility::object::Argument
				Argument;
			typedef ::database::session::ISession
				ISession;
			/**
				@class : MysqlQueryRequestArgument
				@date : 2016-09-22
				@brief : Mysql 쿼리 요청을 위한 파라미터를 넘깁니다.
				@author : jinny20123(안 정웅)
			*/
			struct QueryRequestArgument
				: public Argument
			{
				QueryRequestArgument() {}
				QueryRequestArgument(ISession* session, IQuery* pquery)
					: session_(session), pquery_(pquery)
				{}

				ISession* session_;
				IQuery* pquery_;
			};
		}
	}
} /// database::logic::query