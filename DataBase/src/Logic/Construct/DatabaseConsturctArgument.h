﻿#pragma once
#include <memory>
#include "../../../Utility/src/Object/argument.h"

namespace database {
	namespace logic {
		namespace construct {
			typedef ::utility::object::Argument
				Argument;

			struct MysqlConsturctCreateArgument
				: public Argument
			{
				MysqlConsturctCreateArgument() {}
				MysqlConsturctCreateArgument(size_t thread_count, const char* ip, const char* user, const char* pw)
					: thread_count_(thread_count), ip_(ip), user_(user), pw_(pw)
				{}

				size_t thread_count_;		///< 동작할 쓰레드 카운트 수
				std::string ip_;			///< 접속할 데이터베이스 정보
				std::string user_;			///< 유저명
				std::string pw_;			///< 패스워드
			};
		}
	}
} /// database::logic::construct
