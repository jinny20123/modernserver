﻿#include "MysqlDatabaseConstruct.h"
#include "DatabaseConsturctArgument.h"
#include "../Processor/QueryProcessor.h"
#include "../Processor/Handler/Object/MysqlHandler.h"
#include "../../Static/DataBaseStatic.h"
#include "../../Factory/DefaultFactory.h"
#include "../../Session/SessionArgument.h"
#include "../../Session/Object/MysqlSession.h"
#include <jdbc/mysql_driver.h>

namespace database {
	namespace logic {
		namespace construct {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct MysqlDatabaseConstruct::Impl
			{
				Impl() {}

				typedef ::database::logic::processor::QueryProcessor
					QueryProcessor;
				typedef ::database::logic::processor::handler::object::MysqlHandler
					Handler;

				size_t thread_count_;
				std::string ip_;			///< 접속할 데이터베이스 정보
				std::string user_;			///< 유저명
				std::string pw_;			///< 패스워드
				std::unique_ptr<QueryProcessor> query_processor_;
				std::unique_ptr<sql::mysql::MySQL_Driver> driver_;
				std::shared_ptr<Handler>	phandler_;
			};

			MysqlDatabaseConstruct::MysqlDatabaseConstruct(Argument* arg)
			{
				MAKE_UNIQUE(mImpl, Impl);
				MysqlConsturctCreateArgument* mysql_arg = nullptr;
				::utility::object::ArgumentUtil::CastArgument(arg, mysql_arg);
				BOOST_ASSERT(mysql_arg);

				mImpl->phandler_ = std::make_shared<Impl::Handler>();

				mImpl->driver_.reset(sql::mysql::get_mysql_driver_instance());

				mImpl->thread_count_ = mysql_arg->thread_count_;
				mImpl->ip_ = mysql_arg->ip_;
				mImpl->pw_ = mysql_arg->pw_;
				mImpl->user_ = mysql_arg->user_;
			}
			MysqlDatabaseConstruct::~MysqlDatabaseConstruct()
			{
			}

			void
				MysqlDatabaseConstruct::SetUp()
			{
				PrepareProcessor();
			}

			void
				MysqlDatabaseConstruct::Run()
			{
			}

			void
				MysqlDatabaseConstruct::PrepareProcessor()
			{
				MAKE_UNIQUE(mImpl->query_processor_, Impl::QueryProcessor, ::database::static_::DATABASE_STATIC().GetProducer(),
					mImpl->thread_count_, mImpl->phandler_, std::bind(&MysqlDatabaseConstruct::MakeSession, this));
			}

			MysqlDatabaseConstruct::ISession*
				MysqlDatabaseConstruct::MakeSession()
			{
				return ::database::factory::DefaultFactory<::database::session::object::MysqlSession, ::database::session::ISession>::
					GetInstance().Create(&::database::session::MysqlSessionCreateArgument(mImpl->driver_.get(), mImpl->ip_.c_str(), mImpl->user_.c_str(), mImpl->pw_.c_str()));
			}
		}
	}
} /// database::logic::construct