﻿#pragma once

#include <memory>
#include "../../../../Utility/src/macro.h"
#include "../../../../Common/src/Logic/Construct/IConstruct.h"

namespace database {
	namespace session {
		class ISession;
	}
}

namespace database {
	namespace logic {
		namespace construct {
			/**
				@class : DatabaseConstruct
				@date : 2016-09-22
				@brief : DataBase를 구동하는 작업을 등록합니다.
				@author : jinny20123(안 정웅)
			*/
			class MysqlDatabaseConstruct
				: public ::common::logic::construct::IConstruct
			{
			public:
				typedef ::database::session::ISession
					ISession;

				MysqlDatabaseConstruct(Argument* arg);
				virtual ~MysqlDatabaseConstruct();
			private:
				void PrepareProcessor();

				ISession* MakeSession();

				struct Impl;
				std::unique_ptr<Impl> mImpl;

				virtual void SetUp() OVERRIDE;
				virtual void Run() OVERRIDE;
			};
		}
	}
} /// database::logic::construct