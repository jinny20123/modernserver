﻿#pragma once

#include <memory>
#include "../../../Utility/src/macro.h"
#include "../../../Utility/src/Object/argument_fwd.h"

namespace database { namespace session { 

class ISession 
{
public: 
	typedef ::utility::object::Argument
			Argument;

	enum Type
	{
		MYSQL,
	};

	virtual ~ISession(){};
	/// ISession 인터페이스를 정의합니다. 
	virtual bool Capture(Argument * arg) = 0;
	virtual void Release() = 0;
	virtual Type GetType() = 0;
};

}} /// database::session
