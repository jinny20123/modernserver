﻿#include "MysqlSession.h"
#include "../SessionArgument.h"
#include "../../../../Log/src/Common/CommonLogger.h"
#include <jdbc/mysql_driver.h>
#include <jdbc/mysql_connection.h>
#include <jdbc/cppconn/connection.h>
#include <jdbc/cppconn/statement.h>

namespace database {
	namespace session {
		namespace object {
			/// 내부에서 사용할 변수를 정의합니다.
			/// 생성자를 통해서 객체를 할당하고 소멸자를 통해서 해제합니다.
			struct MysqlSession::Impl
			{
				Impl() {}

				std::shared_ptr<::sql::Driver>	driver_;
				std::shared_ptr<::sql::Connection> conn_;
			};

			MysqlSession::MysqlSession()
			{
				MAKE_UNIQUE(mImpl, Impl);
			}
			MysqlSession::~MysqlSession()
			{}

			bool
				MysqlSession::Capture(Argument* arg)
			{
				MysqlSessionCreateArgument* pmsc_arg = nullptr;
				::utility::object::ArgumentUtil::CastArgument(arg, pmsc_arg);

				if (!pmsc_arg)
				{
					return false;
				}

				try
				{
					mImpl->driver_ = pmsc_arg->pdriver_;
					Connect(pmsc_arg->ip_.c_str(), pmsc_arg->user_.c_str(), pmsc_arg->pw_.c_str());

					if (mImpl->conn_.get() == nullptr || mImpl->conn_->isClosed()) ///< 연결이 유지되고 있는지 판단합니다.
					{
						COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "[MysqlSession] Capture Session Close");
						return false;
					}
				}
				catch (sql::SQLException & e)
				{
					COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "[MysqlSession] %s", e.what());
				}

				return true;
			}

			void
				MysqlSession::Release()
			{
				// 세션을 종료합니다.
				Close();
			}

			MysqlSession::Type
				MysqlSession::GetType()
			{
				return Type::MYSQL;
			}

			void
				MysqlSession::Connect(const char* ip, const char* user, const char* pw)
			{
				Close();
				try
				{
					mImpl->conn_.reset(mImpl->driver_->connect(ip, user, pw));
					COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "[MysqlSession] Connect %s", ip);
					COMMONLOG().ToDebug(NS_COMMONLOG::CONSOLE, "[MysqlSession] Connect %s", ip);
				}
				catch (sql::SQLException & e)
				{
					COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "[MysqlSession] %s", e.what());
					COMMONLOG().ToInfo(NS_COMMONLOG::CONSOLE, "[MysqlSession] %s", e.what());
				}
			}

			void
				MysqlSession::Close()
			{
				if (mImpl->conn_.get() && !mImpl->conn_->isClosed())
				{
					mImpl->conn_->close();
				}
			}

			bool
				MysqlSession::SimpleQuery(const char* query)
			{
				bool bRet = false;
				if (!query) { return bRet; }

				sql::Statement* stmt = nullptr;
				try
				{
					stmt = mImpl->conn_->createStatement();
					stmt->execute(query);

					delete stmt; stmt = nullptr;

					bRet = true;
				}
				catch (sql::SQLException & e)
				{
					COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "[MysqlSession] SimpleQuery %s", e.what());
					if (stmt) { delete stmt; }
				}

				return bRet;
			}

			sql::Statement*
				MysqlSession::GetStatement()
			{
				sql::Statement* stmt = nullptr;
				try
				{
					stmt = mImpl->conn_->createStatement();
				}
				catch (sql::SQLException & e)
				{
					COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "[MysqlSession] GetStatement %s", e.what());
				}
				return stmt;
			}

			sql::PreparedStatement*
				MysqlSession::Prepare(const char* query)
			{
				if (!query) { return nullptr; }
				sql::PreparedStatement* pstmt = nullptr;
				try
				{
					pstmt = mImpl->conn_->prepareStatement(query);
				}
				catch (sql::SQLException & e)
				{
					COMMONLOG().ToInfo(NS_COMMONLOG::SYSTEM, "[MysqlSession] Prepare %s", e.what());
				}

				return pstmt;
			}
		}
	}
} /// database::session::object