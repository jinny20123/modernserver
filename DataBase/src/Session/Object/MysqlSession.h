﻿#pragma once

#include <memory>
#include "../ISession.h"
#include "../../../../Utility/src/macro.h"

namespace sql
{
	class Statement;
	class PreparedStatement;
}

namespace database { namespace session { namespace object { 


/** 
	@class : MysqlSession 
	@date : 2016-09-21
	@brief : Mysql에 연결하는 Session 객체를 정의합니다. 
	@author : jinny20123(안 정웅)
*/
class MysqlSession 	
	: public ISession
{
public: 
	MysqlSession();
	virtual ~MysqlSession();
	virtual bool Capture(Argument * arg) OVERRIDE;
	virtual void Release() OVERRIDE;
	virtual Type GetType() OVERRIDE;

	void Connect(const char * ip, const char * user, const char * pw);
	void Close();
	bool SimpleQuery(const char * query);
	sql::Statement * GetStatement();
	sql::PreparedStatement * Prepare(const char * query);


private :
	struct Impl;
	std::unique_ptr<Impl> mImpl;
};
	
}}} /// database::session::object