﻿#pragma once

#include "../../../Utility/src/Object/argument.h"
namespace sql {
	namespace mysql {
		class MySQL_Driver;
	}
}

namespace database { namespace session { 
	typedef ::utility::object::Argument
			Argument;
	struct MysqlSessionCreateArgument
		: public Argument
	{
		MysqlSessionCreateArgument() {}
		MysqlSessionCreateArgument(::sql::mysql::MySQL_Driver * pdriver, const char * ip, const char * user, const char * pw) :
			pdriver_(pdriver), ip_(ip), user_(user), pw_(pw)
		{}

		std::shared_ptr<::sql::mysql::MySQL_Driver> pdriver_;
		std::string ip_;
		std::string user_;
		std::string pw_;

	};
}} /// database::session